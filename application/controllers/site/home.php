<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('format');
		$this->load->model(array(
			'time_log_model'
		));
	}

	public function index()
	{
		$this->template->show();
	}

	public function login()
	{
		$post = $this->input->post();

		if($post) {
			$account = $this->account_model->authenticate($post['username'], $post['password']);
			if($account !== false) {
				$notif = (date('A') == 'AM');

				$hour = floor($account->acc_hours_to_work);
				$minutes = $account->acc_hours_to_work - $hour;
				// convert decimal to minutes in minutes
				$minutes = 60 * $minutes;

				$curTimestamp = format_mysql_datetime();
				$tml_set_time_in = format_mysql_date()." ".$account->acc_time_in;
				$tml_set_time_out = format_mysql_datetime($tml_set_time_in." + {$hour} hours {$minutes} minutes");
				$time_log = array(
					'acc_id' => $account->acc_id,
					'tml_in' => $curTimestamp,
					'tml_set_time_in' => $tml_set_time_in,
					'tml_set_time_out' => $tml_set_time_out
				);
				
				$this->time_log_model->create($time_log);
				$this->template->notification('Login successful.', 'success', false);
				redirect(current_url(), 'refresh');
			}
			else {
				$this->template->notification('Invalid employee login.', 'danger', false);
			}
		}

		$this->template->content('employee/login');
		$this->template->show('site/templates', 'plain');
	}
}

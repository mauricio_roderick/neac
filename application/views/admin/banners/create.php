<div class="row">
	<div class="col-md-9">
		<form method="post" enctype="multipart/form-data" data-submit="submitForm">
			<div class="box">
				<div class="box-body">
					<div class="form-group">
						<label for="ban_title" class="control-label">Title</label>
						<input type="text" name="ban_title" class="form-control" maxlength="100" />
					</div>
					<div class="form-group">
						<label for="ban_description" class="control-label">Caption / Description</label>
						<input type="text" name="ban_description" class="form-control" maxlength="255" />
					</div>
					<div class="form-group">
						<div class="btn btn-default btn-file">
	                    	<i class="fa fa-file-image-o"></i> Image
	                    	<input type="file" name="ban_image">
	                    </div>
					</div>
					<div class="form-group">
						<label for="ban_link" class="control-label">Link</label>
						<input type="text" name="ban_link" class="form-control" size="80" maxlength="500" value="" />
					</div>
					<div class="form-group">
						<label for="ban_published" class="control-label">Published</label>
						<select name="ban_published" class="form-control">
							<option value="published">Yes</option>
							<option value="draft">No</option>
						</select>
					</div>
				</div>
				<div class="box-footer">
					<div class="pull-right">
						<button type="submit" name="form_submit" value='submit' class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
					</div>
					<a class="btn btn-default" href="<?php echo back_href(); ?>"><i class="fa fa-times"></i> Cancel</a>
				</div>
			</div>
		</form>
	</div>
</div>
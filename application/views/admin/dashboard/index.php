<!-- Add Site Modules here... -->
<div class="row">

	<div class="col-md-6 col-sm-12 col-xs-12">
		<div class="small-box bg-primary">
			<div class="inner">
				<h3>Banners</h3>
				<p>...</p>
			</div>
			<div class="icon">
				<i class="fa fa-picture-o"></i>
			</div>
			<a  href="<?php echo admin_url('banners') ?>" class="small-box-footer">
				Manage Banners <i class="fa fa-arrow-circle-right"></i>
			</a>
		</div>
	</div>

	<div class="col-md-6 col-sm-12 col-xs-12">
		<div class="small-box bg-primary">
			<div class="inner">
				<h3>Articles</h3>
				<p>...</p>
			</div>
			<div class="icon">
				<i class="fa fa-file"></i>
			</div>
			<a  href="<?php echo admin_url('articles') ?>" class="small-box-footer">
				Manage Articles <i class="fa fa-arrow-circle-right"></i>
			</a>
		</div>
	</div>

	<div class="col-md-6 col-sm-12 col-xs-12">
		<div class="small-box bg-primary">
			<div class="inner">
				<h3>Pages</h3>
				<p>...</p>
			</div>
			<div class="icon">
				<i class="fa fa-book"></i>
			</div>
			<a  href="<?php echo admin_url('pages') ?>" class="small-box-footer">
				Manage Pages <i class="fa fa-arrow-circle-right"></i>
			</a>
		</div>
	</div>

	<div class="col-md-6 col-sm-12 col-xs-12">
		<div class="small-box bg-primary">
			<div class="inner">
				<h3>Users</h3>
				<p>...</p>
			</div>
			<div class="icon">
				<i class="fa fa-user"></i>
			</div>
			<a  href="<?php echo admin_url('accounts') ?>" class="small-box-footer">
				Manage Users <i class="fa fa-arrow-circle-right"></i>
			</a>
		</div>
	</div>
</div>
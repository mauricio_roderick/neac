<div class="row">
	<div class="col-md-9">
		<form method="post" enctype="multipart/form-data" data-submit="submitForm">
			<div class="box">
				<div class="box-body">
					<div class="form-group">
						<label for="art_title" class="control-label">Title</label>
						<input type="text" name="art_title" class="form-control" maxlength="100" />
					</div>
					<div class="form-group">
						<label for="art_blurb" class="control-label">Blurb</label>
						<input type="text" name="art_blurb" class="form-control" maxlength="200" />
					</div>
					<div class="form-group">
						<label for="art_slug" class="control-label">Slug</label>
						<input type="text" name="art_slug" class="form-control" maxlength="100" />
					</div>
					<div class="form-group">
						<label for="art_content" class="control-label">Content</label>
						<textarea name="art_content" class="redactor" style="width: 100%; height: 400px;"><?php echo $article->art_content ?></textarea>
					</div>
					<div class="form-group">
						<div class="btn btn-default btn-file">
	                    	<i class="fa fa-file-image-o"></i> Image
	                    	<input type="file" name="art_image">
	                    </div>

						<br>
						<br>
						<div>
	                    	<?php if($article->art_image!=""){?>
							<div id="thumb">
								<img src="<?php echo base_url($article->art_image); ?>" />
							</div>
							<br />

							<a class="btn-btn-link" href="<?php echo base_url($article->art_image); ?>" target="_blank">Download Image</a>
							<?php } ?>
						</div>
					</div>
					<div class="form-group">
						<label for="art_published" class="control-label">Published</label>
						<select name="art_published" class="form-control">
							<option value="published">published</option>
							<option value="draft">draft</option>
						</select>
					</div>
					<div class="form-group">
						<label for="art_featured" class="control-label">Featured</label>
						<select name="art_featured" class="form-control">
							<option value="yes">yes</option>
							<option value="no">no</option>
						</select>
					</div>
					<div class="form-group">
						<label for="art_date" class="control-label">Date</label>
						<div class="row">
							<div class='col-md-6 col-xs-8'>
								<input type="text" name="art_date" class="sumodate" value="<?php echo $article->art_date ?>" />
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="art_author" class="control-label">Author</label>
						<input type="text" name="art_author" class="form-control" maxlength="100" />
					</div>
				</div>
				<div class="box-footer">
					<div class="pull-right">
						<button type="submit" name="form_submit" value='submit' class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
					</div>
					<a class="btn btn-default" href="<?php echo back_href(); ?>"><i class="fa fa-times"></i> Cancel</a>
				</div>
			</div>
		</form>
	</div>
</div>
<script type="text/javascript">
$(function() {
	$('form').floodling('art_title', "<?php echo addslashes($article->art_title); ?>");
	$('form').floodling('art_blurb', "<?php echo addslashes($article->art_blurb); ?>");
	$('form').floodling('art_slug', "<?php echo addslashes($article->art_slug); ?>");
	$('form').floodling('art_published', "<?php echo addslashes($article->art_published); ?>");
	$('form').floodling('art_featured', "<?php echo addslashes($article->art_featured); ?>");
	$('form').floodling('art_author', "<?php echo addslashes($article->art_author); ?>");

	$('input[name="art_title"]').change(function(){
		$('input[name="art_slug"]').val($(this).val().toSlug());
	});
});
</script>

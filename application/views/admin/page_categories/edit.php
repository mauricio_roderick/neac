<?php
// Normal form here. Form validation are taken care of by the controller.
// Make sure to name your form elements properly and uniquely.
?>
<form method="post">
	<div class="col-sm-3 col-sm-push-9 block">
		<div class="box">
			<div class="box-header">
				<h3 class="box-title">Menu</h3>
			</div>
			<div class="box-body">
				<button class="btn btn-primary btn-flat" type="submit" value="submit" name="submit">
					<i class="fa fa-save"></i> Submit
				</button>
				<?php echo create_admin_back_btn($this->router->fetch_class(), 'Cancel'); ?>
			</div>
		</div>
	</div>

	<div class="col-sm-9 col-sm-pull-3">
		<div class="box">
			<div class="box-body">
				<div class="form-group">
					<label for="pct_name" class="control-label">Category Name</label>
					<input class="form-control" type="text" name="pct_name" value="<?php echo $page_category->pct_name; ?>" />
				</div>
				<div class="form-group">
					<button class="btn btn-primary btn-flat" type="submit" value="submit" name="submit">
						<i class="fa fa-save"></i> Submit
					</button>
				</div>
			</div>
		</div>
	</div>
</form>

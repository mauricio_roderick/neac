<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH.'/libraries/REST_Controller.php';

class Pages extends REST_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->api_access_control->validate();
		$this->load->model('page_api_model');
	}

	public function view_get()
	{
		$request = $this->input->get();
		$response = $this->page_api_model->get_one($request['pag_id']);
		if(isset($response['error']))
		{
			$this->response($response, 200);
		}
		else
		{
			$this->response($response, 200);
		}
	}

	public function list_get()	
	{
		$request = $this->extract->get();
		$response = $this->page_api_model->get_all($request);
		if(isset($response['error']))
		{
			$this->response($response, 200);
		}
		else
		{
			$this->response($response, 200);
		}
	}

	public function create_post()
	{
		$request = $this->extract->post();
		$response = $this->page_api_model->create($request);
		if(isset($response['error']))
		{
			$this->response($response, 200);	
		}
		else
		{
			$this->response($response, 200);
		}
	}

	public function update_post()
	{
		$request = $this->extract->post();
		$response = $this->page_api_model->update($request);
		if(isset($response['error']))
		{
			$this->response($response, 200);
		}
		else
		{
			$this->response($response, 200);
		}
	}

	public function delete_post()
	{
		$request = $this->extract->post();
		$response = $this->page_api_model->delete($request['pag_id']);
		if(isset($response['error']))
		{
			$this->response($response, 200);
		}
		else
		{
			$this->response($response, 200);
		}
	}
}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Articles extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->access_control->logged_in();
		$this->access_control->account_type('dev', 'admin');
		$this->access_control->validate();

		$this->load->helper('nav');
		$this->load->model('article_model');

	}

	public function index($art_published = 'all')
	{
		$this->template->title('Articles');

		if($this->input->post('form_mode'))
		{
			$form_mode = $this->input->post('form_mode');

			if($form_mode == 'delete')
			{
				$art_ids = $this->input->post('art_ids');
				if($art_ids !== false)
				{
					foreach($art_ids as $art_id)
					{
						$article = $this->article_model->get_one($art_id);
						if($article !== false)
						{
							$this->article_model->delete($art_id);
						}
					}
					$this->template->notification('Selected articles were deleted.', 'success');
				}
			}
		}

		if($art_published == 'all')
		{
			$params['art_published !='] = 'archived';
		}
		else
		{
			$params['art_published'] = $art_published;
		}

		$page = array();
		$page['articles'] = $this->article_model->pagination("admin/articles/index/$art_published/__PAGE__", 'get_all', $params);
		$page['articles_pagination'] = $this->article_model->pagination_links();
		$this->template->content('articles-index', $page);
		$this->template->content('articles-menu', null, 'admin', 'page-nav');
		$this->template->show();
	}

	public function create()
	{
		$this->template->title('Create Article');


		// Use the set_rules from the Form_validation class for form validation.
		// Already combined with jQuery. No extra coding required for JS validation.
		// We get both JS and PHP validation which makes it both secure and user friendly.
		// NOTE: Set the rules before you check if $_POST is set so that the jQuery validation will work.
		$this->form_validation->set_rules('art_title', 'Title', 'trim|required|max_length[100]|xxs_clean');
		$this->form_validation->set_rules('art_blurb', 'Blurb', 'trim|required|max_length[200]|xxs_clean');
		$this->form_validation->set_rules('art_slug', 'Slug', 'trim|max_length[100]|xxs_clean');
		$this->form_validation->set_rules('art_content', 'Content', 'trim|required');
		$this->form_validation->set_rules('art_published', 'Published', 'trim|required|xxs_clean');
		$this->form_validation->set_rules('art_featured', 'Featured', 'trim|required|xxs_clean');
		$this->form_validation->set_rules('art_date', 'Date', 'trim|required|date|xxs_clean');
		$this->form_validation->set_rules('art_author', 'Author', 'trim|required|max_length[100]|xxs_clean');

		if($this->input->post('form_submit'))
		{
			$article = $this->extract->post();
			// Call run method from Form_validation to check
			if($this->form_validation->run() !== false)
			{
				$id = $this->article_model->create($article);
				// Set a notification using notification method from Template.
				// It is okay to redirect after and the notification will be displayed on the redirect page.
				$this->template->notification('New article created.', 'success');
				$id = $id['result']['insert_id'];
				redirect("/admin/articles/edit/$id");
			}
			else
			{
				// To display validation errors caught by the Form_validation, you should have the code below.
				$this->template->notification(validation_errors(), 'danger');
			}

			$this->template->autofill($article);
		}

		$page = array();

		$this->template->content('articles-create', $page);
		$this->template->content('articles-menu', null, 'admin', 'page-nav');
		$this->template->show();
	}

	public function edit($art_id)
	{
		$this->template->title('Edit Article');


		$this->form_validation->set_rules('art_title', 'Title', 'trim|required|max_length[100]');
		$this->form_validation->set_rules('art_blurb', 'Blurb', 'trim|required|max_length[200]');
		$this->form_validation->set_rules('art_slug', 'Slug', 'trim|required|max_length[100]');
		$this->form_validation->set_rules('art_content', 'Content', 'trim|required');
		$this->form_validation->set_rules('art_published', 'Published', 'trim|required');
		$this->form_validation->set_rules('art_featured', 'Featured', 'trim|required');
		$this->form_validation->set_rules('art_date', 'Date', 'trim|required|date');
		$this->form_validation->set_rules('art_author', 'Author', 'trim|required|max_length[100]');


		if($this->input->post('form_submit'))
		{

			$article = $this->extract->post();

			if($this->form_validation->run() !== false)
			{
				$article['art_id'] = $art_id;
				$this->article_model->update($article);
				// Set a notification using notification method from Template.
				// It is okay to redirect after and the notification will be displayed on the redirect page.
				$this->template->notification('Article has been edited.', 'success');
				redirect("/admin/articles/edit/$art_id");
			}
			else
			{
				$this->template->notification(validation_errors());
			}
			$this->template->autofill($article);
		}

		$page = array();
		$page['article'] = $this->article_model->get_one($art_id);
		if($page['article'] === false)
		{
			$this->template->notification('Article was not found.', 'danger');
			redirect('admin/articles');
		}

		$this->template->content('articles-edit', $page);
		$this->template->content('articles-menu', null, 'admin', 'page-nav');
		$this->template->show();
	}

	public function view($article_id)
	{
		$this->template->title('View Article');

		$page = array();
		$page['article'] = $this->article_model->get_one($article_id);

		if($page['article'] === false)
		{
			$this->template->notification('Article was not found.', 'danger');
			redirect('admin/articles');
		}

		$this->template->content('articles-view', $page);
		$this->template->content('articles-menu', null, 'admin', 'page-nav');
		$this->template->show();
	}

	/***********
	handle upload
	***********/
	private function _handle_upload($form_element = null)
	{
		$image_upload = array();
		$form_element = ($form_element == null) ? 'art_image' : $form_element;
		$image = $this->image->upload($form_element, $this->upload_path);

		if(isset($image['error'])) {
			$this->image_upload_error = $image['error'];
			return false;
		}
		else {
			$image_upload['file_name'] = $image['file_name'];
			$image_upload['thumb'] = $image['raw_name'] . "_thumb" . $image['file_ext'];
			$thumbs = array(
				array('width'=>$this->img_width, 'height'=>$this->img_height, 'filename'=>$image_upload['thumb'])
			);
			$this->image->create_thumbs($image['full_path'], $thumbs);
			return $image_upload;
		}
	}

	private function _handle_image_crop($image, $coords = false)
	{
		$image = $this->image->crop($image, $coords);

		if(isset($image['error'])) {
			$this->image_upload_error = $image['error'];
			return false;
		}
		else {
			return $image;
		}
	}
}

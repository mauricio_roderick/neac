<div class="col-sm-3 col-sm-push-9 block">
	<div class="box">
		<div class="box-header">
			<h3 class="box-title">Menu</h3>
		</div>
		<div class="box-body">
			<div class="btn-group">
				<a href="<?php echo site_url('admin/site_options/edit/' . $site_options->opt_id); ?>" class="btn btn-primary btn-flat btn-sm">
					<i class="fa fa-edit"></i> Edit
				</a>
				<?php echo create_admin_back_btn($this->router->fetch_class()); ?>
			</div>
		</div>
	</div>
</div>
<div class="col-sm-9 col-sm-pull-3">
	<div class="box">
		<div class="box-body no-padding table-responsive">
			<table class="table table-bordered table-hover table-view">
				<tr>
					<th>Name</th>
					<td><?php echo $site_options->opt_name; ?></td>
				</tr>
				<tr>
					<th>Slug</th>
					<td><?php echo $site_options->opt_slug; ?></td>
				</tr>
				<tr>
					<th>Value</th>
					<td>
						<?php if($site_options->opt_type == "image" ): ?>
									
						<img src="<?php echo base_url($site_options->opt_value); ?>" alt="" class="lightbox" style="max-height:100px;max-width:100px"/>
						<?php else: ?>
							<?php echo $site_options->opt_value; ?>
						<?php endif; ?>
						
					</td>
				</tr>
			</table>
		</div>
	</div>
</div>
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Fb_registration extends CI_Controller 
{

	public function __construct() 
	{
		parent::__construct();
		$this->load->model('user_model');
		$this->load->model('user_api_ids_model');
		$this->load->helper('format');
	}

	public function login(){

    	$valid_account = "";

    	$result = array();
		$result['logged_in'] = false;

		$access_token = $this->input->post('uai_api_token');
		$user = $this->input->post(null, true);
		//DB SHIT HERE
		//Check if user exists
		$fb_account = $this->user_api_ids_model->get_one_based_on_id_type('facebook', $user['uai_api_id']);
		if($fb_account){
			//User already exists
			//Update access token just in case
			$usr = array();
			$usr['uai_id'] = $fb_account->aid_id;
			$usr['uai_api_token'] = $access_token;
			$this->user_api_ids_model->update($usr);
			
			$valid_account = $this->user_model->get_one($fb_account->usr_id);
		}
		else {
			//Create a new account
			$account = array();
			$account = $user;

			// $account['password'] = 'changeme';

			//verify user name is still valid
			$i = 2;
			$orig_username = $account['usr_username'];
			while($this->user_model->get_by_username($account['usr_username'])){
				$account['usr_username'] = $orig_username . $i++;
			}

			$account['usr_image'] = serialize(array(
				'image' => $account['usr_image'],
				'thumb' => $account['usr_thumb']
			));

			$usr_id = $this->user_model->create($account);
			
			$verified_account = $this->user_model->get_one($usr_id);
			//create account API
			$api_id = array();
			$api_id['uai_type'] = 'facebook';
			$api_id['usr_id'] = $usr_id;
			$api_id['uai_api_id'] = $user['uai_api_id'];
			$api_id['uai_api_token'] = $access_token;



			//create a temporary secret
			$api_id['uai_api_secret'] = substr(str_shuffle(str_repeat('ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789', 32)),0, 32);
			while($this->user_api_ids_model->get_one_based_on_secret('facebook', $api_id['uai_api_secret'])){
				$api_id['uai_api_secret'] = substr(str_shuffle(str_repeat('ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789', 32)),0, 32);
			}

			$uai_id = $this->user_api_ids_model->create($api_id);

			$valid_account = $this->user_model->get_one($usr_id);
		}



		if($valid_account !== 'false') {
			// set session variables
			$this->session->set_userdata('acc_username', $valid_account->usr_username);
			$this->session->set_userdata('acc_type', 'user');
			$this->session->set_userdata('account_id', $valid_account->usr_id);
			$this->session->set_userdata('acc_first_name', $valid_account->usr_fname);
			$this->session->set_userdata('acc_last_name', $valid_account->usr_lname);
			$this->session->set_userdata('logged_in', true);
			$result['logged_in'] = true;
	 	} else {
	 		$result['logged_in'] = false;
			$result['error'] = '<div id="login-error-message" class="alert alert-error">Failed to authenticate your account. Please try again.</div>';
	 	}

	 	echo json_encode($result);
	}
}
<?php
// Extend Base_model instead of CI_model
class User_model extends Base_model
{
	public function __construct()
	{
		// List all fields of the table.
		// Primary key must be auto-increment and must be listed here first.
		$fields = array('usr_id', 
										'usr_username', 
										'usr_password', 
										'usr_lname', 
										'usr_fname', 
										'usr_birthdate', 
										'usr_gender', 
										'usr_email', 
										'usr_image', 
										'usr_company', 
										'usr_website', 
										'usr_occupation', 
										'usr_mobile', 
										'usr_landline', 
										'usr_address1', 
										'usr_address2', 
										'usr_city', 
										'usr_country', 
										'usr_zip', 
										'usr_failed_login', 
										'usr_last_login', 
										'usr_status', 
										'usr_date_created', 
										'usr_verification',
										'usr_ip_address');
		// Call the parent constructor with the table name and fields as parameters.
		parent::__construct('user', $fields);
	}

	// Inherits the create, update, delete, get_one, and get_all methods of base_model.


	public function get_by_username($usr_username)
	{
		$this->db->where('usr_username', $usr_username);
		$query = $this->db->get($this->table); // Use $this->table to get the table name
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
		else
		{
			return false;
		}
	}

	public function get_by_email($usr_email)
	{
		$this->db->where('usr_email', $usr_email);
		$query = $this->db->get($this->table); // Use $this->table to get the table name
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
		else
		{
			return false;
		}
	}
	
	public function authenticate($usr_username, $usr_password)
	{
		$this->db->where('usr_username', $usr_username);
		$this->db->where('usr_password', md5($usr_password));
		$query = $this->db->get($this->table); 
		if($query->num_rows() > 0)
		{
			$query->usr_last_login = format_mysql_datetime();
			$query->usr_ip_address = $this->input->ip_address();
			$this->update($query);
			return $query->row();
		}
		else
		{
			$query->usr_failed_login++;
			$this->update($query);
			return false;
		}
	}

	public function search_keyword_admin($search = array(), $params = array())
	{
		if (count($search) > 0) {
			$this->db->like($search['field'], $search['keywords']); 
		}
		return parent::get_all($params);
	}

}
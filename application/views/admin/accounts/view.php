<div class="row">
	<div class="col-md-12 col-lg-6">
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">
					<i class="fa fa-lg fa-user"></i>
					User Profile
				</h3>
			</div>
			<div class="box-body no-padding">
				<table class="table table-hover table-view">
					<tr>
						<td colspan="2" class="text-center">
							<?php if($account->acc_image): ?>
							<img src="<?php echo base_url($account->acc_image); ?>" class="img-circle" alt="User Image">
							<?php else: ?>
							<!-- <img src="<?php echo $grav_url; ?>" class="img-circle" alt="User Image"> -->
							<img style="max-width:100px" src="<?php echo res_url('admin/images/default_avatar.png'); ?>" class="img-circle" alt="User Image">
							<?php endif; ?>
						</td>
					</tr>
					<tr>
						<th>Email</th>
						<td><?php echo $account->acc_username; ?></td>
					</tr>
					<tr>
						<th>Name</th>
						<td><?php echo $account->acc_first_name . ' ' . $account->acc_last_name; ?></td>
					</tr>
					<tr>
						<th>Account Type</th>
						<td><?php echo $account->acc_type; ?></td>
					</tr>
					<?php if($account->acc_type == 'employee'): ?>
					<tr>
						<th>Salary</th>
						<td><?php echo number_format($account->acc_salary) ?></td>
					</tr>
					<tr>
						<th>Required Time to Login</th>
						<td><?php echo format_time($account->acc_time_in) ?></td>
					</tr>
					<tr>
						<th># of Required Hours to Work</th>
						<td><?php echo ($account->acc_hours_to_work) ?></td>
					</tr>
					<?php endif; ?>
				</table>
			</div>
			<div class="box-footer">
				<div class="pull-right">
					<a href="<?php echo site_url('admin/accounts'); ?>" class="btn btn-primary"> <i class="fa fa-users"></i> View all accounts</a>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-12 col-lg-6">
		<form method="post" data-submit="submitForm">
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">
						<i class="fa fa-lg fa-lock"></i>
						Reset Password
					</h3>
				</div>
				<div class="box-body">				
					<div class="form-group">
						<input type="hidden" class="form-control" name="acc_username" />
					</div>
					<div class="form-group">
						<label for="acc_username" class="control-label">New Password</label>
						<?php echo $acc_password; ?>
						<input type="hidden" name="acc_password" value="<?php echo $acc_password; ?>" />
					</div>
					<div class="form-group">
						<span class="label label-warning">Please copy the password above.</span>
					</div>
					<div class="form-group">
					</div>
				</div>
				<div class="box-footer">
					<div class="pull-right">
						<button type="submit" name="form_mode" value='submit' class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Page_api_model extends Api_model
{

	public function __construct()
	{
		// List all fields of the table.
		// Primary key must be auto-increment and must be listed here first.
		$fields = array('pag_id', 'pag_title', 'pct_id', 'pag_slug', 'pag_content', 'pag_date_created', 'pag_date_modified', 'pag_date_published', 'pag_type', 'pag_status');
		// Call the parent constructor with the table name and fields as parameters.
		parent::__construct('page', $fields);
	}

	public function create($data, $field_list = array())
	{
		$this->set_message("Create Page");
		$this->form_validation->set_rules('pag_title', 'Title', 'trim|required|max_length[140]');
		$this->form_validation->set_rules('pct_id', 'Name', 'trim|integer|max_length[10]|min_length[1]');
		$this->form_validation->set_rules('pag_slug', 'Slug', 'trim|max_length[80]');
		$this->form_validation->set_rules('pag_content', 'Content', 'trim|required');
		$this->form_validation->set_rules('pag_date_published', 'Date Published', 'trim|datetime');
		$this->form_validation->set_rules('pag_type', 'Type', 'trim|required');
		$this->form_validation->set_rules('pag_status', 'Status', 'trim|required');
		return parent::create($data, $field_list = array());
	}

	public function update($data, $field_list = array())
	{
		$this->set_message("Update Page");
		$this->form_validation->set_rules('pag_id', 'Page Id', 'integer|required');
		$this->form_validation->set_rules('pag_title', 'Title', 'trim|required|max_length[140]');
		$this->form_validation->set_rules('pct_id', 'Name', 'trim|integer|max_length[10]|min_length[1]');
		$this->form_validation->set_rules('pag_slug', 'Slug', 'trim|max_length[80]');
		$this->form_validation->set_rules('pag_content', 'Content', 'trim|required');
		$this->form_validation->set_rules('pag_date_published', 'Date Published', 'trim|datetime');
		$this->form_validation->set_rules('pag_type', 'Type', 'trim|required');
		$this->form_validation->set_rules('pag_status', 'Status', 'trim|required');
		return parent::update($data, $field_list = array());
	}

	public function delete($id)
	{
		$this->set_message("Delete Page");
		return parent::delete($id);
	}

	public function get_one($id)
	{
		$this->set_message("Retrieve Single Page");
				
		$this->db->join('page_category', "page_category.pct_id = {$this->table}.pct_id");
		return parent::get_one($id);
	}

	public function get_all($params = array(), $order_by = array())
	{
		$this->set_message("List Pages");
				
		$this->db->join('page_category', "page_category.pct_id = {$this->table}.pct_id");
        return parent::get_all($params, $order_by);
	}
}
<?php
// Extend Base_model instead of CI_model
class Photo_model extends Base_model
{
	public function __construct()
	{
		// List all fields of the table.
		// Primary key must be auto-increment and must be listed here first.
		$fields = array('pho_id', 'phg_id', 'pho_src', 'pho_caption', 'pho_date_created', 'pho_created_by');
		// Call the parent constructor with the table name and fields as parameters.
		parent::__construct('photo', $fields);
	}

	// Inherits the create, update, delete, get_one, and get_all methods of base_model.
	public function get_one($id)
	{				
		$this->db->join('photo_gallery', "photo_gallery.phg_id = {$this->table}.phg_id");
		return parent::get_one($id);
	}

	public function get_all($params = array())
	{				
		$this->db->join('photo_gallery', "photo_gallery.phg_id = {$this->table}.phg_id");
		return parent::get_all($params);
	}
	
	public function get_order($params = array())
	{				
		$this->db->join('photo_gallery', "photo_gallery.phg_id = {$this->table}.phg_id");
		$this->db->order_by("pho_order", "desc"); 
		$this->db->limit(1);
		return parent::get_all($params);
	}
	
	public function get_latest($params = array())
	{				
		$this->db->join('photo_gallery', "photo_gallery.phg_id = {$this->table}.phg_id");
		$this->db->order_by("pho_id", "desc"); 
		$this->db->limit(1);
		return parent::get_all($params);
	}

	public function search_keyword_admin($search = array(), $params = array())
	{				
		$this->db->join('photo_gallery', "photo_gallery.phg_id = {$this->table}.phg_id");
		if (count($search) > 0) {
			$this->db->like($search['field'], $search['keywords']); 
		}
		return parent::get_all($params);
	}
}
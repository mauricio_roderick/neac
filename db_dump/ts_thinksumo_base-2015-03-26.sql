-- phpMyAdmin SQL Dump
-- version 4.0.4.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 26, 2015 at 05:57 AM
-- Server version: 5.6.11
-- PHP Version: 5.5.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ts_thinksumo_base`
--
-- CREATE DATABASE IF NOT EXISTS `ts_thinksumo_base` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
-- USE `ts_thinksumo_base`;

-- --------------------------------------------------------

--
-- Table structure for table `account`
--

CREATE TABLE IF NOT EXISTS `account` (
  `acc_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `acc_username` varchar(30) NOT NULL,
  `acc_password` varchar(32) NOT NULL,
  `acc_last_name` varchar(30) NOT NULL,
  `acc_first_name` varchar(60) NOT NULL,
  `acc_type` enum('admin','dev','user') NOT NULL DEFAULT 'user',
  `acc_failed_login` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `acc_status` enum('active','locked','deleted') NOT NULL DEFAULT 'active',
  PRIMARY KEY (`acc_id`),
  UNIQUE KEY `username` (`acc_username`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `account`
--

INSERT INTO `account` (`acc_id`, `acc_username`, `acc_password`, `acc_last_name`, `acc_first_name`, `acc_type`, `acc_failed_login`, `acc_status`) VALUES
(1, 'developer', '5e8edd851d2fdfbd7415232c67367cc3', 'Developer', 'ThinkSumo', 'dev', 0, 'active'),
(2, 'admin', 'b913b0f82adfa847d425eac4f404604a', 'Administrator', 'System', 'admin', 0, 'active');

-- --------------------------------------------------------

--
-- Table structure for table `article`
--

CREATE TABLE IF NOT EXISTS `article` (
  `art_id` int(11) NOT NULL AUTO_INCREMENT,
  `art_title` varchar(100) NOT NULL,
  `art_blurb` varchar(200) NOT NULL,
  `art_slug` varchar(100) NOT NULL,
  `art_content` text NOT NULL,
  `art_thumb` text NOT NULL,
  `art_image` text NOT NULL,
  `art_published` enum('published','draft') NOT NULL,
  `art_featured` enum('yes','no') NOT NULL,
  `art_date` date NOT NULL,
  `art_author` varchar(100) NOT NULL,
  `art_date_created` datetime NOT NULL,
  `art_created_by` varchar(30) NOT NULL,
  `art_date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `art_modified_by` varchar(30) NOT NULL,
  PRIMARY KEY (`art_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `article`
--

INSERT INTO `article` (`art_id`, `art_title`, `art_blurb`, `art_slug`, `art_content`, `art_thumb`, `art_image`, `art_published`, `art_featured`, `art_date`, `art_author`, `art_date_created`, `art_created_by`, `art_date_modified`, `art_modified_by`) VALUES
(1, 'aksdjlasd', 'aslkdj', 'asldkj', 'alsdkj', '', '', 'published', 'yes', '2014-10-13', 'asd', '2014-10-13 09:53:08', 'developer', '2014-10-13 09:53:08', '');

-- --------------------------------------------------------

--
-- Table structure for table `banner`
--

CREATE TABLE IF NOT EXISTS `banner` (
  `ban_id` int(11) NOT NULL AUTO_INCREMENT,
  `ban_title` varchar(100) NOT NULL,
  `ban_description` varchar(255) NOT NULL,
  `ban_link` varchar(512) NOT NULL,
  `ban_image` text NOT NULL,
  `ban_thumb` text NOT NULL,
  `ban_order` int(11) NOT NULL,
  `ban_published` enum('published','draft') NOT NULL,
  `ban_created_by` varchar(30) NOT NULL,
  `ban_date_created` datetime NOT NULL,
  `ban_date_modified` datetime NOT NULL,
  `ban_modified_by` varchar(100) NOT NULL,
  PRIMARY KEY (`ban_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `email`
--

CREATE TABLE IF NOT EXISTS `email` (
  `eml_id` int(11) NOT NULL AUTO_INCREMENT,
  `eml_mail_to` varchar(150) CHARACTER SET utf8 NOT NULL,
  `eml_cc` varchar(150) CHARACTER SET utf8 NOT NULL,
  `eml_bcc` varchar(150) CHARACTER SET utf8 NOT NULL,
  `eml_subject` varchar(150) CHARACTER SET utf8 NOT NULL,
  `eml_message` text CHARACTER SET utf8 NOT NULL,
  `eml_from` varchar(200) NOT NULL,
  `eml_from_name` varchar(200) NOT NULL,
  `eml_date_sent` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `eml_status` enum('sent','failed','resent') NOT NULL,
  `eml_debug` text NOT NULL,
  PRIMARY KEY (`eml_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `page`
--

CREATE TABLE IF NOT EXISTS `page` (
  `pag_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pag_title` varchar(140) NOT NULL,
  `pct_id` int(10) unsigned DEFAULT '0',
  `pag_slug` varchar(80) DEFAULT NULL,
  `pag_content` text NOT NULL,
  `pag_date_created` datetime NOT NULL,
  `pag_date_published` datetime DEFAULT NULL,
  `pag_type` enum('editable','static') NOT NULL DEFAULT 'editable',
  `pag_status` enum('published','draft') NOT NULL DEFAULT 'published',
  PRIMARY KEY (`pag_id`),
  UNIQUE KEY `slug` (`pag_slug`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `page`
--

INSERT INTO `page` (`pag_id`, `pag_title`, `pct_id`, `pag_slug`, `pag_content`, `pag_date_created`, `pag_date_published`, `pag_type`, `pag_status`) VALUES
(3, 'Page Test', 0, NULL, '<p>Mlkshk Brooklyn organic ugh authentic.  Ethical actually kitsch \nseitan, sustainable stumptown Williamsburg readymade normcore.  Wolf \nHelvetica meh, bespoke biodiesel 90''s occupy church-key Intelligentsia. \n Street art biodiesel kogi, High Life pop-up viral +1 Schlitz gentrify \nfarm-to-table scenester jean shorts cliche.  Vice forage mumblecore ugh \nbiodiesel, kitsch Intelligentsia master cleanse selfies pug.  Try-hard \nWilliamsburg keffiyeh, deep v DIY chambray Schlitz Thundercats ethical \nselfies drinking vinegar.  Distillery mlkshk selvage cornhole.</p><p>Stumptown\n organic hella put a bird on it ethical drinking vinegar.  Deep v \nsustainable PBR&B Odd Future flexitarian 8-bit, gluten-free Wes \nAnderson authentic photo booth.  Polaroid normcore farm-to-table \nstumptown 8-bit, irony roof party Carles PBR&B twee hashtag four \nloko.  Art party locavore cliche tousled skateboard gastropub +1 Odd \nFuture Bushwick.  Mlkshk crucifix leggings irony street art Brooklyn.  \nDeep v slow-carb messenger bag Helvetica asymmetrical farm-to-table.  \nMustache squid kitsch, Schlitz butcher Carles you probably haven''t heard\n of them crucifix brunch viral occupy.</p>', '2014-09-24 08:48:36', '2014-09-06 00:00:00', 'editable', 'published'),
(4, 'Page Test', 0, 'page-test', '<p>Mlkshk Brooklyn organic ugh authentic.  Ethical actually kitsch \nseitan, sustainable stumptown Williamsburg readymade normcore.  Wolf \nHelvetica meh, bespoke biodiesel 90''s occupy church-key Intelligentsia. \n Street art biodiesel kogi, High Life pop-up viral +1 Schlitz gentrify \nfarm-to-table scenester jean shorts cliche.  Vice forage mumblecore ugh \nbiodiesel, kitsch Intelligentsia master cleanse selfies pug.  Try-hard \nWilliamsburg keffiyeh, deep v DIY chambray Schlitz Thundercats ethical \nselfies drinking vinegar.  Distillery mlkshk selvage cornhole.</p><p>Stumptown\n organic hella put a bird on it ethical drinking vinegar.  Deep v \nsustainable PBR&B Odd Future flexitarian 8-bit, gluten-free Wes \nAnderson authentic photo booth.  Polaroid normcore farm-to-table \nstumptown 8-bit, irony roof party Carles PBR&B twee hashtag four \nloko.  Art party locavore cliche tousled skateboard gastropub +1 Odd \nFuture Bushwick.  Mlkshk crucifix leggings irony street art Brooklyn.  \nDeep v slow-carb messenger bag Helvetica asymmetrical farm-to-table.  \nMustache squid kitsch, Schlitz butcher Carles you probably haven''t heard\n of them crucifix brunch viral occupy.</p>', '2014-09-24 08:49:24', '2014-09-06 00:00:00', 'editable', 'published');

-- --------------------------------------------------------

--
-- Table structure for table `page_category`
--

CREATE TABLE IF NOT EXISTS `page_category` (
  `pct_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pct_name` varchar(50) NOT NULL,
  PRIMARY KEY (`pct_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `page_category`
--

INSERT INTO `page_category` (`pct_id`, `pct_name`) VALUES
(1, 'Category 1');

-- --------------------------------------------------------

--
-- Table structure for table `photo`
--

CREATE TABLE IF NOT EXISTS `photo` (
  `pho_id` int(11) NOT NULL AUTO_INCREMENT,
  `phg_id` int(11) DEFAULT NULL,
  `pho_src` varchar(5000) DEFAULT NULL,
  `pho_caption` varchar(100) DEFAULT NULL,
  `pho_date_created` datetime DEFAULT NULL,
  `pho_created_by` varchar(50) DEFAULT NULL,
  `pho_order` int(11) NOT NULL,
  PRIMARY KEY (`pho_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `photo_gallery`
--

CREATE TABLE IF NOT EXISTS `photo_gallery` (
  `phg_id` int(11) NOT NULL AUTO_INCREMENT,
  `phg_name` varchar(100) DEFAULT NULL,
  `phg_description` text,
  `phg_status` enum('draft','published') DEFAULT 'draft',
  `phg_date_created` datetime DEFAULT NULL,
  `phg_created_by` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`phg_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `photo_gallery`
--

INSERT INTO `photo_gallery` (`phg_id`, `phg_name`, `phg_description`, `phg_status`, `phg_date_created`, `phg_created_by`) VALUES
(1, 'Photo Gallery Test', 'hello po', 'draft', '2014-09-25 04:22:04', 'ThinkSumo Developer');

-- --------------------------------------------------------

--
-- Table structure for table `session`
--

CREATE TABLE IF NOT EXISTS `session` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(16) NOT NULL DEFAULT '0',
  `user_agent` varchar(50) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `session`
--

INSERT INTO `session` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('05db8dc97486eaac9a5042655fb939e2', '0.0.0.0', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/53', 1423038900, ''),
('062d75a3d6392117d50c15f5e1872a78', '0.0.0.0', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/53', 1423038902, ''),
('0b693fbb0d2de95fda3cfe6249bdd4a5', '0.0.0.0', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/53', 1423038938, ''),
('16f7c7c1957c162537d1e4978f2e2137', '0.0.0.0', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/53', 1423038794, ''),
('2051bde40659f44a6307934db61e708d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/53', 1414988316, 'a:6:{s:9:"user_data";s:0:"";s:12:"acc_username";s:9:"developer";s:13:"acc_last_name";s:9:"Developer";s:14:"acc_first_name";s:9:"ThinkSumo";s:8:"acc_type";s:3:"dev";s:8:"acc_name";s:19:"ThinkSumo Developer";}'),
('20a82575c7bffbc6848aaf451ad7f206', '0.0.0.0', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/53', 1414997699, 'a:6:{s:9:"user_data";s:0:"";s:12:"acc_username";s:9:"developer";s:13:"acc_last_name";s:9:"Developer";s:14:"acc_first_name";s:9:"ThinkSumo";s:8:"acc_type";s:3:"dev";s:8:"acc_name";s:19:"ThinkSumo Developer";}'),
('40f77fe29d16e0a46ca7277c46709236', '0.0.0.0', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/53', 1415680211, ''),
('41253b748e1193b1d08413284c7b233e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/53', 1415680171, 'a:2:{s:9:"user_data";s:0:"";s:21:"flash:old:current_url";s:52:"http://localhost/ts-think-sumo-base-v2/admin/profile";}'),
('443a44a21084ffd9864842c5efc47ca1', '0.0.0.0', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/53', 1423038902, ''),
('5e3f10bd886be29c7be350b8cbaa87a0', '0.0.0.0', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/53', 1423038900, ''),
('65f407f4ba37ff5eacc949b321bdd843', '0.0.0.0', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/53', 1423038691, ''),
('6ce453746f60ed6b4f1da4348d2ae3be', '0.0.0.0', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/53', 1415686690, ''),
('701fdeec143c9692e02381569724f995', '0.0.0.0', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/53', 1423038812, ''),
('8b7200e21273b8c5b600955837e75a7f', '0.0.0.0', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/53', 1423038794, ''),
('8fc8ac881ac454365bdedc1375d481c0', '0.0.0.0', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/53', 1423038880, ''),
('97f6c5f6495aecffda88c307f78e1531', '0.0.0.0', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/53', 1423038902, ''),
('983961a59e218172148e28e96865851b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/53', 1415603731, 'a:2:{s:9:"user_data";s:0:"";s:21:"flash:old:current_url";s:52:"http://localhost/ts-think-sumo-base-v2/admin/profile";}'),
('9a00cc0a02a664b4662dd693c94d6d86', '0.0.0.0', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/53', 1415603620, 'a:2:{s:9:"user_data";s:0:"";s:21:"flash:old:current_url";s:52:"http://localhost/ts-think-sumo-base-v2/admin/profile";}'),
('9db35221345dbd19f3866ea2067b815e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/53', 1426133240, 'a:1:{s:9:"user_data";s:0:"";}'),
('9e44da1d01f354b05269ccf5801211da', '0.0.0.0', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/53', 1415686684, 'a:2:{s:9:"user_data";s:0:"";s:21:"flash:old:current_url";s:52:"http://localhost/ts-think-sumo-base-v2/admin/profile";}'),
('b39d4053f99de149dae50784190dddee', '0.0.0.0', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/53', 1414996317, 'a:2:{s:9:"user_data";s:0:"";s:21:"flash:old:current_url";s:52:"http://localhost/ts-think-sumo-base-v2/admin/profile";}'),
('c3b9aef4044cef7343268dde2f14afaa', '0.0.0.0', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/53', 1415000949, 'a:2:{s:9:"user_data";s:0:"";s:21:"flash:old:current_url";s:52:"http://localhost/ts-think-sumo-base-v2/admin/profile";}'),
('c9adf85138352aee400622e835fb6ecf', '0.0.0.0', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/53', 1423038902, ''),
('cac16a629c3591f16c3bdcb6289382d3', '0.0.0.0', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/53', 1423038901, ''),
('d91431f5fd196d35e9620f8c8abce1e3', '0.0.0.0', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/53', 1423038901, ''),
('db78446ccacdf2475c823c3c1bf761f1', '0.0.0.0', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/53', 1423038880, ''),
('dd6e62d92630a9eab4e500c9ab12a92f', '0.0.0.0', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/53', 1423038902, ''),
('e2056eaec317a6db940d91fbe934e196', '0.0.0.0', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/53', 1423038938, ''),
('ebc85f2c5be1e1a78f395d441f60657b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/53', 1423038812, ''),
('f0b30c5a4a51e00e2fa3ca22779d3779', '0.0.0.0', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/53', 1423038902, ''),
('f12e3a443675148efdba51dcf47e1f40', '0.0.0.0', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/53', 1415606707, 'a:2:{s:9:"user_data";s:0:"";s:21:"flash:old:current_url";s:52:"http://localhost/ts-think-sumo-base-v2/admin/profile";}'),
('f27e28f0c14cd93cb83899ce0eead05d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/53', 1423038882, ''),
('fe76055b8d661ff9a4c90b8c064fa354', '0.0.0.0', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/53', 1423038882, '');

-- --------------------------------------------------------

--
-- Table structure for table `site_options`
--

CREATE TABLE IF NOT EXISTS `site_options` (
  `opt_id` int(11) NOT NULL AUTO_INCREMENT,
  `opt_name` varchar(256) NOT NULL,
  `opt_slug` varchar(100) NOT NULL,
  `opt_type` enum('text','image') NOT NULL DEFAULT 'text',
  `opt_value` text NOT NULL,
  PRIMARY KEY (`opt_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `site_options`
--

INSERT INTO `site_options` (`opt_id`, `opt_name`, `opt_slug`, `opt_type`, `opt_value`) VALUES
(1, 'Site Name', 'site-name', 'text', 'Think Sumo 2'),
(2, 'Facebook Title', 'facebook-title', 'text', 'Think Sumo'),
(3, 'Facebook Description', 'facebook-description', 'text', 'Think Sumo Site'),
(4, 'Facebook Image (1200px x 630px)', 'facebook-image', 'image', './uploads/site_options/facebook-image.png');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `usr_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `usr_username` varchar(30) NOT NULL,
  `usr_password` varchar(32) NOT NULL,
  `usr_lname` varchar(50) NOT NULL,
  `usr_fname` varchar(100) NOT NULL,
  `usr_birthdate` date DEFAULT NULL,
  `usr_gender` enum('male','female') DEFAULT NULL,
  `usr_email` varchar(100) DEFAULT NULL,
  `usr_image` text,
  `usr_company` varchar(100) DEFAULT NULL,
  `usr_website` varchar(100) DEFAULT NULL,
  `usr_occupation` varchar(100) DEFAULT NULL,
  `usr_mobile` varchar(50) DEFAULT NULL,
  `usr_landline` varchar(50) DEFAULT NULL,
  `usr_address1` varchar(150) DEFAULT NULL,
  `usr_address2` varchar(150) DEFAULT NULL,
  `usr_city` varchar(100) DEFAULT NULL,
  `usr_country` varchar(50) DEFAULT NULL,
  `usr_zip` varchar(10) DEFAULT NULL,
  `usr_failed_login` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `usr_last_login` datetime DEFAULT NULL,
  `usr_status` enum('active','locked','deleted') NOT NULL DEFAULT 'active',
  `usr_date_created` datetime NOT NULL,
  `usr_verification` varchar(32) DEFAULT NULL,
  `usr_ip_address` varchar(20) NOT NULL,
  PRIMARY KEY (`usr_id`),
  UNIQUE KEY `username` (`usr_username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_api_ids`
--

CREATE TABLE IF NOT EXISTS `user_api_ids` (
  `uai_id` int(11) NOT NULL AUTO_INCREMENT,
  `usr_id` int(11) NOT NULL,
  `uai_type` enum('facebook','twitter','instagram') NOT NULL,
  `uai_api_id` text NOT NULL,
  `uai_api_token` text NOT NULL,
  `uai_api_secret` text NOT NULL,
  PRIMARY KEY (`uai_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

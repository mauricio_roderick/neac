$(function() {
    var body = $(window);
    var height = (body.height() / 3) * 2;
    var width = height;
    var sumoContent = $('.sumo-bubble-content');
    var marginTop = height - sumoContent.height();

    $('#sumo-bubble').css({
        'height': height,
        'width': width,
    });

    sumoContent.css({
        'position': 'relative',
        'top': ( marginTop / 2 ) - 20
    });
});
<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Example
 *
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array.
 *
 * @package		CodeIgniter
 * @subpackage	Rest Server
 * @category	Controller
 * @author		Phil Sturgeon
 * @link		http://philsturgeon.co.uk/code/
*/

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/libraries/REST_Controller.php';

class Article_api extends REST_Controller
{

	public function __construct() 
	{
		parent::__construct();
		$this->load->model('article_model');
	}
	
	public function list_articles_get(){
		$page_size = 1;
		
		$page_no = $this->get('page_no');
		
		$articles = $this->article_model->get_paginated_article($page_no, $page_size);
		$result = array();
		
		foreach($articles->result() as $article) 
		{
			$result[] = $article;
		}
		
		$this->response($result, 200);
	}
}
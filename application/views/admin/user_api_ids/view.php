<div class="col-sm-3 col-sm-push-9 block">
	<div class="box">
		<div class="box-header">
			<h3 class="box-title">Menu</h3>
		</div>
		<div class="box-body">
			<div class="btn-group">
				<a href="<?php echo site_url('admin/user_api_ids/edit/' . $user_api_ids->uai_id); ?>" class="btn btn-primary btn-flat btn-sm">
					<i class="fa fa-edit"></i> Edit
				</a>
				<?php echo create_admin_back_btn($this->router->fetch_class()); ?>
			</div>
		</div>
	</div>
</div>
<div class="col-sm-9 col-sm-pull-3">
	<div class="box">
		<div class="box-body no-padding table-responsive">
			<table class="table table-bordered table-hover table-view">
				<tr>
					<th>User</th>
					<td><?php echo $user_api_ids->usr_username; ?></td>
				</tr>
				<tr>
					<th>Type</th>
					<td><?php echo $user_api_ids->uai_type; ?></td>
				</tr>
				<tr>
					<th>Api Id</th>
					<td><?php echo nl2br($user_api_ids->uai_api_id); ?></td>
				</tr>
				<tr>
					<th>Api Token</th>
					<td><?php echo nl2br($user_api_ids->uai_api_token); ?></td>
				</tr>
				<tr>
					<th>Api Secret</th>
					<td><?php echo nl2br($user_api_ids->uai_api_secret); ?></td>
				</tr>
			</table>
		</div>
	</div>
</div>
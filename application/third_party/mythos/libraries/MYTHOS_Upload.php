<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

require_once(SYSDIR . '/libraries/Upload.php');

/*****

This library handles basic file uploads

*****/
class MYTHOS_Upload extends CI_Upload
{
	private $CI;
	
	public function __construct() 
	{
		$this->CI =& get_instance();
	}
	
	/**
	* params
	* field - input type file name attribute
	* path - path where the file will be uploaded. Default is uploads folder in root
	* extensions - allowed extensions. Default is *
	* 
	*
	* returns the upload data
	*
	*/
	public function file($field = false, $path = false, $extensions = '*')
	{
		//Check if field is set
		if(!$field) {
			$error = array('error' => 'Specify File field');
			return $error;
		}
		
		if(!file_exists($_FILES[$field]['tmp_name']) || !is_uploaded_file($_FILES[$field]['tmp_name'])) {
			$error = array('error' => 'No file attached');
			return $error;
		}
		
		$upload_config = array();
		
		if(!$path)
		{
			$upload_config['upload_path'] = FCPATH . 'uploads/';
		}
		else
		{
			$upload_config['upload_path'] = rtrim($path, '/\\') . '/';
		}
		
		$upload_config['allowed_types'] = $extensions;
		$upload_config['max_size']	= '0';
		$upload_config['encrypt_name'] = true;
		$upload_config['overwrite'] = false;
		$this->CI->upload->initialize($upload_config);
		
		if(!$this->CI->upload->do_upload($field))
		{
			$error = array('error' => $this->CI->upload->display_errors());
			$d = $this->CI->upload->data();
			return $error;
		}
		else
		{
			return $this->CI->upload->data();
		}
	}
}
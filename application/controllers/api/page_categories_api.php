<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Example
 *
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array.
 *
 * @package		CodeIgniter
 * @subpackage	Rest Server
 * @category	Controller
 * @author		Phil Sturgeon
 * @link		http://philsturgeon.co.uk/code/
*/

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/libraries/REST_Controller.php';

class Page_categories_api extends REST_Controller
{

	public function __construct() 
	{
		parent::__construct();
		$this->load->model('page_category_model');
	}
	
	function view_get()
    {
		$pct_id = $this->get('pct_id');
		
		if($pct_id){
			$page_category = $this->page_category_model->get_one($pct_id);
			if(!$page_category){
				$this->response(array('error' => 'Page category not found.'), 200);
			}
			else{
				$this->response($page_category, 200);
			}
		}
		else{
			$this->response(array('error' => 'Enter a pct_id.'), 200);
		}
	}
	
	public function list_get()	
	{
		//expects a json encoded object
		$array = $this->get('array');
		
		//then decode the json encode file
		//$arry = json_decode($array, true)
		
		if($array) {
			$page_category = $this->page_category_model->get_all($array);
			if(!$page_category){
				$this->response(array('error' => 'Invalid parameters'), 200);
			}
			else{
				$this->response($page_category, 200);	
			}
		}
		else{
			$this->response(array('error' => 'Blank.'), 200);
		}
	}
	
	public function delete_post()
	{
		$pct = $this->extract->post();
		
			if($pct){
				if($pct['pct_id']){
					$page_category = $this->page_category_model->get_one($pct['pct_id']);
					if($page_category){
						$this->page_category_model->delete($pct_id);
					}
					else{
						$this->response(array('error' => 'Page category not found.'), 200);
					}
				}
				else{
					$this->response(array('error' => 'Enter a pct_id.'), 200);
				}
			}
			else{
				$this->response(array('error' => 'Enter a pct_id.'), 200);
			}
	}
	
	public function create_post()
	{
		$pct = $this->extract->post();
		
		if($pct){
			$new_page_category = $this->page_category_model->create($pct);
			
			if($new_page_category){
			
				$this->response(array('success' => 'Page category added.'), 200);
			}
			else{
				$this->response(array('error' => 'Page category not added.'), 200);
			}
		}
		else{
			$this->response(array('error' => 'Blank.'), 200);
		}
		
	}
	
	public function update_post()
	{
		$pct = $this->extract->post();
		
		if($pct){
			$upate_page_category = $this->page_category_model->update($pct);
			
			if($upate_page_category){
				$this->response(array('success' => 'Page category updated.'), 200);
			}
			else{
				$this->response(array('error' => 'Page category not updated'), 200);
			}
		}
		else{
			$this->response(array('error' => 'Blank.'), 200);
		}
	}
	
	/*
	$result = array();
		
		foreach($page_categories->result() as $page_category) 
		{
			$result[] = $page_category;
		}
		
		output_json($result);
	*/
}
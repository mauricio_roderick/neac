<?php
// Extend Base_model instead of CI_model
class Email_model extends Base_model
{
	public function __construct()
	{
		// List all fields of the table.
		// Primary key must be auto-increment and must be listed here first.
		$fields = array('eml_id', 'eml_mail_to', 'eml_cc', 'eml_bcc', 'eml_subject', 'eml_message', 'eml_date_sent','eml_from','eml_from_name','eml_status','eml_debug');
		// Call the parent constructor with the table name and fields as parameters.
		parent::__construct('email', $fields);
	}

	// Inherits the create, update, delete, get_one, and get_all methods of base_model.




	public function search_keyword_admin($search = array(), $params = array(),$order_by = array())
	{
		if (count($search) > 0) {
			$this->db->like($search['field'], $search['keywords']); 
		}
		return parent::get_all($params ,$order_by);
	}

}
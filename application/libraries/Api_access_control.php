<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Api_access_control
{
	private $CI;
	
	public function __construct() 
	{
		$this->CI =& get_instance();
		$this->CI->load->model('account_model');
	}

	public function validate()
	{
		$token = $this->get_auth_headers();
		$user = $this->authenticate($token);
		if($user!=false)
		{
			//set your user-related variables here
		}
		else
		{	
			echo json_encode(array('error'=>'invalid token'));
			die();
		}
	}

	private function get_auth_headers()
	{
		if($this->CI->input->get_request_header('Token')=="")
		{
			return false;;
		}
		else
		{
			return $this->CI->input->get_request_header('Token');	
		}
	}

	private function authenticate($token)
	{
		if($token=="")
		{
			return false;
		}
		else
		{
			return $this->CI->account_model->api_authenticate($token);
		}
	}
}

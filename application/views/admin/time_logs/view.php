<div class="row">
	<div class="col-md-3">
		<a href="<?php echo admin_url('time_logs/create') ?>" class="btn btn-primary btn-block margin-bottom">
			<i class="fa fa-plus-square"></i> Create
		</a>
			</div>

	<div class="col-md-9">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">
					Id: <?php echo $time_log->acc_id; ?> |
					In: <?php echo $time_log->tml_in; ?> |
					Out: <?php echo $time_log->tml_out; ?> |
					Set Time In: <?php echo $time_log->tml_set_time_in; ?> |
					Set Time Out: <?php echo $time_log->tml_set_time_out; ?> |
				</h3>
				<div class="box-tools pull-right">
                </div>
			</div>
			<div class="box-body">
		        <div class="mailbox-read-message">
                	<h3>In</h3>
                	<div><?php echo $time_log->tml_in ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>Out</h3>
                	<div><?php echo $time_log->tml_out ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>Set Time In</h3>
                	<div><?php echo $time_log->tml_set_time_in ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>Set Time Out</h3>
                	<div><?php echo $time_log->tml_set_time_out ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>Created</h3>
                	<div><?php echo $time_log->tml_created ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>Deleted</h3>
                	<div><?php echo $time_log->tml_deleted ?></div>
		        </div>
			</div>
			<div class="box-footer">
				<a class="btn btn-default" href="<?php echo admin_url('time logs/edit/'.$time_log->tml_id); ?>">
					<i class="fa fa-edit"></i> Edit
				</a>
			</div>
		</div>
	</div>
</div>
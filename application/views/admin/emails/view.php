<div class="col-sm-3 col-sm-push-9 block">
	<div class="box">
		<div class="box-header">
			<h3 class="box-title">Menu</h3>
		</div>
		<div class="box-body">
			<a href="<?php echo site_url('admin/emails/edit/' . $email->eml_id); ?>" class="btn btn-primary btn-flat btn-sm">Edit</a>
			<?php echo create_admin_back_btn($this->router->fetch_class()); ?>
		</div>
	</div>
</div>
<div class="col-sm-9 col-sm-pull-3">
	<div class="box">
		<div class="box-body no-padding">
			<div class="table-responsive">
				<table class="table table-striped table-hover">
					<tr>
						<th>Mail To</th>
						<td><?php echo $email->eml_mail_to; ?></td>
					</tr>
					<tr>
						<th>Cc</th>
						<td><?php echo $email->eml_cc; ?></td>
					</tr>
					<tr>
						<th>Bcc</th>
						<td><?php echo $email->eml_bcc; ?></td>
					</tr>
					<tr>
						<th>Subject</th>
						<td><?php echo $email->eml_subject; ?></td>
					</tr>
					<tr>
						<th>Message</th>
						<td><?php echo nl2br($email->eml_message); ?></td>
					</tr>
					<tr>
						<th>Date Sent</th>
						<td><?php echo format_datetime($email->eml_date_sent, "Y-m-d H:i:s"); ?></td>
					</tr>
				</table>
			</div>
		</div>
	</div>
</div>

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Page extends CI_Controller 
{

	public function __construct() 
	{
		parent::__construct();
		$this->load->model('page_model');
		$this->load->helper('format');
	}
	
	public function index($slug) 
	{
		
		$page = $this->page_model->get_published($slug);
		if($page !== false)
		{
			$this->template->title($page->pag_title);
			
			// Use format_image_path() to convert all relative image paths to absolute paths
			$content = $page->pag_content;
			$content = format_url_path($content);
			$content = format_image_path($content);
			$this->template->set('content', $content);
			
			$this->template->show('site');
		}
		else
		{
			show_404(site_url('page/index/' . $slug));
		}
	}
}

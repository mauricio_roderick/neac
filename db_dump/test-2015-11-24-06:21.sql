-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 24, 2015 at 06:20 AM
-- Server version: 5.5.46-0ubuntu0.14.04.2
-- PHP Version: 5.5.9-1ubuntu4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `test`
--

-- --------------------------------------------------------

--
-- Table structure for table `account`
--

CREATE TABLE IF NOT EXISTS `account` (
  `acc_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `acc_username` varchar(30) NOT NULL,
  `acc_password` varchar(32) NOT NULL,
  `acc_last_name` varchar(30) NOT NULL,
  `acc_first_name` varchar(60) NOT NULL,
  `acc_type` enum('admin','dev','user','employee') NOT NULL DEFAULT 'employee',
  `acc_failed_login` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `acc_status` enum('active','locked','deleted') NOT NULL DEFAULT 'active',
  PRIMARY KEY (`acc_id`),
  UNIQUE KEY `username` (`acc_username`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `account`
--

INSERT INTO `account` (`acc_id`, `acc_username`, `acc_password`, `acc_last_name`, `acc_first_name`, `acc_type`, `acc_failed_login`, `acc_status`) VALUES
(1, 'developer', '5e8edd851d2fdfbd7415232c67367cc3', 'Developer', 'ThinkSumo', 'dev', 0, 'active'),
(2, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'Administrator', 'System', 'admin', 0, 'active'),
(3, 'john.doe@yahoo.com', '9ff81091e4d6a3e35008a5dbc50f34f7', 'Doe', 'John', 'admin', 0, 'active'),
(4, 'john.doe2@yahoo.com', 'cf5b0a3ac7069112eaea8886a9906da8', 'Doe', 'John', 'admin', 0, 'active');

-- --------------------------------------------------------

--
-- Table structure for table `article`
--

CREATE TABLE IF NOT EXISTS `article` (
  `art_id` int(11) NOT NULL AUTO_INCREMENT,
  `art_title` varchar(100) NOT NULL,
  `art_blurb` varchar(200) NOT NULL,
  `art_slug` varchar(100) NOT NULL,
  `art_content` text NOT NULL,
  `art_thumb` text NOT NULL,
  `art_image` text NOT NULL,
  `art_published` enum('published','draft') NOT NULL,
  `art_featured` enum('yes','no') NOT NULL,
  `art_date` date NOT NULL,
  `art_author` varchar(100) NOT NULL,
  `art_date_created` datetime NOT NULL,
  `art_created_by` varchar(30) NOT NULL,
  `art_date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `art_modified_by` varchar(30) NOT NULL,
  PRIMARY KEY (`art_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `banner`
--

CREATE TABLE IF NOT EXISTS `banner` (
  `ban_id` int(11) NOT NULL AUTO_INCREMENT,
  `ban_title` varchar(100) NOT NULL,
  `ban_description` varchar(255) NOT NULL,
  `ban_link` varchar(512) NOT NULL,
  `ban_image` text NOT NULL,
  `ban_thumb` text NOT NULL,
  `ban_order` int(11) NOT NULL,
  `ban_published` enum('published','draft') NOT NULL,
  `ban_created_by` varchar(30) NOT NULL,
  `ban_date_created` datetime NOT NULL,
  `ban_date_modified` datetime NOT NULL,
  `ban_modified_by` varchar(100) NOT NULL,
  PRIMARY KEY (`ban_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `email`
--

CREATE TABLE IF NOT EXISTS `email` (
  `eml_id` int(11) NOT NULL AUTO_INCREMENT,
  `eml_mail_to` varchar(150) CHARACTER SET utf8 NOT NULL,
  `eml_cc` varchar(150) CHARACTER SET utf8 NOT NULL,
  `eml_bcc` varchar(150) CHARACTER SET utf8 NOT NULL,
  `eml_subject` varchar(150) CHARACTER SET utf8 NOT NULL,
  `eml_message` text CHARACTER SET utf8 NOT NULL,
  `eml_date_sent` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`eml_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `email`
--

INSERT INTO `email` (`eml_id`, `eml_mail_to`, `eml_cc`, `eml_bcc`, `eml_subject`, `eml_message`, `eml_date_sent`) VALUES
(1, 'maui.lontoc@sumofy.me', 'rupert.cabrera@sumofy.me, karlo.banzuela@sumofy.me', 'paolo.santos@sumofy.me', 'Ahihihi', '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /><style type="text/css">\n		\n			/* Client-specific Styles */\n			#outlook a{padding:0;} /* Force Outlook to provide a "view in browser" button. */\n			body{width:100% !important;} .ReadMsgBody{width:100%;} .ExternalClass{width:100%;} /* Force Hotmail to display emails at full width */\n			body{-webkit-text-size-adjust:none;} /* Prevent Webkit platforms from changing default text sizes. */\n\n			/* Reset Styles */\n			body{margin:0; padding:0;}\n			img{border:0; height:auto; line-height:100%; outline:none; text-decoration:none;}\n			table td{border-collapse:collapse;}\n			#backgroundTable{height:100% !important; margin:0; padding:0; width:100% !important;}\n\n			/* Template Styles */\n\n			/* /\\/\\/\\/\\/\\/\\/\\/\\/\\/\\ STANDARD STYLING: COMMON PAGE ELEMENTS /\\/\\/\\/\\/\\/\\/\\/\\/\\/\\ */\n\n			/**\n			* @tab Page\n			* @section background color\n			* @tip Set the background color for your email. You may want to choose one that matches your company''s branding.\n			* @theme page\n			*/\n			body, #backgroundTable{\n				/*@editable*/ background-color:#FAFAFA;\n			}\n\n			/**\n			* @tab Page\n			* @section email border\n			* @tip Set the border for your email.\n			*/\n			#templateContainer{\n				/*@editable*/ border: 1px solid #DDDDDD;\n			}\n\n			/**\n			* @tab Page\n			* @section heading 1\n			* @tip Set the styling for all first-level headings in your emails. These should be the largest of your headings.\n			* @style heading 1\n			*/\n			h1, .h1{\n				/*@editable*/ color:#202020;\n				display:block;\n				/*@editable*/ font-family:Arial;\n				/*@editable*/ font-size:34px;\n				/*@editable*/ font-weight:bold;\n				/*@editable*/ line-height:100%;\n				margin-top:0;\n				margin-right:0;\n				margin-bottom:10px;\n				margin-left:0;\n				/*@editable*/ text-align:left;\n			}\n\n			/**\n			* @tab Page\n			* @section heading 2\n			* @tip Set the styling for all second-level headings in your emails.\n			* @style heading 2\n			*/\n			h2, .h2{\n				/*@editable*/ color:#202020;\n				display:block;\n				/*@editable*/ font-family:Arial;\n				/*@editable*/ font-size:30px;\n				/*@editable*/ font-weight:bold;\n				/*@editable*/ line-height:100%;\n				margin-top:0;\n				margin-right:0;\n				margin-bottom:10px;\n				margin-left:0;\n				/*@editable*/ text-align:left;\n			}\n\n			/**\n			* @tab Page\n			* @section heading 3\n			* @tip Set the styling for all third-level headings in your emails.\n			* @style heading 3\n			*/\n			h3, .h3{\n				/*@editable*/ color:#202020;\n				display:block;\n				/*@editable*/ font-family:Arial;\n				/*@editable*/ font-size:26px;\n				/*@editable*/ font-weight:bold;\n				/*@editable*/ line-height:100%;\n				margin-top:0;\n				margin-right:0;\n				margin-bottom:10px;\n				margin-left:0;\n				/*@editable*/ text-align:left;\n			}\n\n			/**\n			* @tab Page\n			* @section heading 4\n			* @tip Set the styling for all fourth-level headings in your emails. These should be the smallest of your headings.\n			* @style heading 4\n			*/\n			h4, .h4{\n				/*@editable*/ color:#202020;\n				display:block;\n				/*@editable*/ font-family:Arial;\n				/*@editable*/ font-size:22px;\n				/*@editable*/ font-weight:bold;\n				/*@editable*/ line-height:100%;\n				margin-top:0;\n				margin-right:0;\n				margin-bottom:10px;\n				margin-left:0;\n				/*@editable*/ text-align:left;\n			}\n\n			/* /\\/\\/\\/\\/\\/\\/\\/\\/\\/\\ STANDARD STYLING: PREHEADER /\\/\\/\\/\\/\\/\\/\\/\\/\\/\\ */\n\n			/**\n			* @tab Header\n			* @section preheader style\n			* @tip Set the background color for your email''s preheader area.\n			* @theme page\n			*/\n			#templatePreheader{\n				/*@editable*/ background-color:#FAFAFA;\n			}\n\n			/**\n			* @tab Header\n			* @section preheader text\n			* @tip Set the styling for your email''s preheader text. Choose a size and color that is easy to read.\n			*/\n			.preheaderContent div{\n				/*@editable*/ color:#505050;\n				/*@editable*/ font-family:Arial;\n				/*@editable*/ font-size:10px;\n				/*@editable*/ line-height:100%;\n				/*@editable*/ text-align:left;\n			}\n\n			/**\n			* @tab Header\n			* @section preheader link\n			* @tip Set the styling for your email''s preheader links. Choose a color that helps them stand out from your text.\n			*/\n			.preheaderContent div a:link, .preheaderContent div a:visited, /* Yahoo! Mail Override */ .preheaderContent div a .yshortcuts /* Yahoo! Mail Override */{\n				/*@editable*/ color:#336699;\n				/*@editable*/ font-weight:normal;\n				/*@editable*/ text-decoration:underline;\n			}\n\n			/* /\\/\\/\\/\\/\\/\\/\\/\\/\\/\\ STANDARD STYLING: HEADER /\\/\\/\\/\\/\\/\\/\\/\\/\\/\\ */\n\n			/**\n			* @tab Header\n			* @section header style\n			* @tip Set the background color and border for your email''s header area.\n			* @theme header\n			*/\n			#templateHeader{\n				/*@editable*/ background-color:#FFFFFF;\n				/*@editable*/ border-bottom:0;\n			}\n\n			/**\n			* @tab Header\n			* @section header text\n			* @tip Set the styling for your email''s header text. Choose a size and color that is easy to read.\n			*/\n			.headerContent{\n				/*@editable*/ color:#202020;\n				/*@editable*/ font-family:Arial;\n				/*@editable*/ font-size:34px;\n				/*@editable*/ font-weight:bold;\n				/*@editable*/ line-height:100%;\n				/*@editable*/ padding:0;\n				/*@editable*/ text-align:center;\n				/*@editable*/ vertical-align:middle;\n			}\n\n			/**\n			* @tab Header\n			* @section header link\n			* @tip Set the styling for your email''s header links. Choose a color that helps them stand out from your text.\n			*/\n			.headerContent a:link, .headerContent a:visited, /* Yahoo! Mail Override */ .headerContent a .yshortcuts /* Yahoo! Mail Override */{\n				/*@editable*/ color:#336699;\n				/*@editable*/ font-weight:normal;\n				/*@editable*/ text-decoration:underline;\n			}\n\n			#headerImage{\n				height:auto;\n				max-width:600px !important;\n			}\n\n			/* /\\/\\/\\/\\/\\/\\/\\/\\/\\/\\ STANDARD STYLING: MAIN BODY /\\/\\/\\/\\/\\/\\/\\/\\/\\/\\ */\n\n			/**\n			* @tab Body\n			* @section body style\n			* @tip Set the background color for your email''s body area.\n			*/\n			#templateContainer, .bodyContent{\n				/*@editable*/ background-color:#FFFFFF;\n			}\n\n			/**\n			* @tab Body\n			* @section body text\n			* @tip Set the styling for your email''s main content text. Choose a size and color that is easy to read.\n			* @theme main\n			*/\n			.bodyContent div{\n				/*@editable*/ color:#505050;\n				/*@editable*/ font-family:Arial;\n				/*@editable*/ font-size:14px;\n				/*@editable*/ line-height:150%;\n				/*@editable*/ text-align:left;\n			}\n\n			/**\n			* @tab Body\n			* @section body link\n			* @tip Set the styling for your email''s main content links. Choose a color that helps them stand out from your text.\n			*/\n			.bodyContent div a:link, .bodyContent div a:visited, /* Yahoo! Mail Override */ .bodyContent div a .yshortcuts /* Yahoo! Mail Override */{\n				/*@editable*/ color:#336699;\n				/*@editable*/ font-weight:normal;\n				/*@editable*/ text-decoration:underline;\n			}\n\n			.bodyContent img{\n				display:inline;\n				height:auto;\n			}\n\n			/* /\\/\\/\\/\\/\\/\\/\\/\\/\\/\\ STANDARD STYLING: FOOTER /\\/\\/\\/\\/\\/\\/\\/\\/\\/\\ */\n\n			/**\n			* @tab Footer\n			* @section footer style\n			* @tip Set the background color and top border for your email''s footer area.\n			* @theme footer\n			*/\n			#templateFooter{\n				/*@editable*/ background-color:#FFFFFF;\n				/*@editable*/ border-top:0;\n			}\n\n			/**\n			* @tab Footer\n			* @section footer text\n			* @tip Set the styling for your email''s footer text. Choose a size and color that is easy to read.\n			* @theme footer\n			*/\n			.footerContent div{\n				/*@editable*/ color:#707070;\n				/*@editable*/ font-family:Arial;\n				/*@editable*/ font-size:12px;\n				/*@editable*/ line-height:125%;\n				/*@editable*/ text-align:left;\n			}\n\n			/**\n			* @tab Footer\n			* @section footer link\n			* @tip Set the styling for your email''s footer links. Choose a color that helps them stand out from your text.\n			*/\n			.footerContent div a:link, .footerContent div a:visited, /* Yahoo! Mail Override */ .footerContent div a .yshortcuts /* Yahoo! Mail Override */{\n				/*@editable*/ color:#336699;\n				/*@editable*/ font-weight:normal;\n				/*@editable*/ text-decoration:underline;\n			}\n\n			.footerContent img{\n				display:inline;\n			}\n\n			/**\n			* @tab Footer\n			* @section social bar style\n			* @tip Set the background color and border for your email''s footer social bar.\n			* @theme footer\n			*/\n			#social{\n				/*@editable*/ background-color:#FAFAFA;\n				/*@editable*/ border:0;\n			}\n\n			/**\n			* @tab Footer\n			* @section social bar style\n			* @tip Set the background color and border for your email''s footer social bar.\n			*/\n			#social div{\n				/*@editable*/ text-align:center;\n			}\n\n			/**\n			* @tab Footer\n			* @section utility bar style\n			* @tip Set the background color and border for your email''s footer utility bar.\n			* @theme footer\n			*/\n			#utility{\n				/*@editable*/ background-color:#FFFFFF;\n				/*@editable*/ border:0;\n			}\n\n			/**\n			* @tab Footer\n			* @section utility bar style\n			* @tip Set the background color and border for your email''s footer utility bar.\n			*/\n			#utility div{\n				/*@editable*/ text-align:center;\n			}\n\n			#monkeyRewards img{\n				max-width:190px;\n			}\n		</style></head><body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0"><center><table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="backgroundTable"><tr><td align="center" valign="top"><!-- // Begin Template Preheader \\\\ --><table border="0" cellpadding="10" cellspacing="0" width="600" id="templatePreheader"><tr><td valign="top" class="preheaderContent"><!-- // Begin Module: Standard Preheader \\ --><table border="0" cellpadding="10" cellspacing="0" width="100%"><tr><td valign="top"><div>\n                                                	 Use this area to offer a short teaser of your email''s content. Text here will show in the preview area of some email clients.\n                                                </div></td><!-- *|IFNOT:ARCHIVE_PAGE|* --><td valign="top" width="190"><div>\n                                                	Is this email not displaying correctly?<br /><a href="#" target="_blank">View it in your browser</a>.\n                                                </div></td><!-- *|END:IF|* --></tr></table><!-- // End Module: Standard Preheader \\ --></td></tr></table><!-- // End Template Preheader \\\\ --><table border="0" cellpadding="0" cellspacing="0" width="600" id="templateContainer"><tr><td align="center" valign="top"><!-- // Begin Template Header \\\\ --><table border="0" cellpadding="0" cellspacing="0" width="600" id="templateHeader"><tr><td class="headerContent"><!-- // Begin Module: Standard Header Image \\\\ --><img src="http://gallery.mailchimp.com/653153ae841fd11de66ad181a/images/placeholder_600.gif" style="max-width:600px;" id="headerImage campaign-icon"/><!-- // End Module: Standard Header Image \\\\ --></td></tr></table><!-- // End Template Header \\\\ --></td></tr><tr><td align="center" valign="top"><!-- // Begin Template Body \\\\ --><table border="0" cellpadding="0" cellspacing="0" width="600" id="templateBody"><tr><td valign="top" class="bodyContent"><!-- // Begin Module: Standard Content \\\\ --><table border="0" cellpadding="20" cellspacing="0" width="100%"><tr><td valign="top"><div>\n                                                                This is a freaking test.                                                            </div></td></tr></table><!-- // End Module: Standard Content \\\\ --></td></tr></table><!-- // End Template Body \\\\ --></td></tr><tr><td align="center" valign="top"><!-- // Begin Template Footer \\\\ --><table border="0" cellpadding="10" cellspacing="0" width="600" id="templateFooter"><tr><td valign="top" class="footerContent"><!-- // Begin Module: Standard Footer \\\\ --><table border="0" cellpadding="10" cellspacing="0" width="100%"><tr><td colspan="2" valign="middle" id="social"><div>\n                                                                &nbsp;<a href="#">follow on Twitter</a> | <a href="#">friend on Facebook</a> | <a href="#">forward to a friend</a>&nbsp;\n                                                            </div></td></tr><tr><td valign="top" width="350"><div><em>Copyright &copy; 2014, All rights reserved.</em><br /></div></td><td valign="top" width="190" id="monkeyRewards"><div><strong>Our mailing address is:</strong><br />\n																mail-tester@sumofy.me\n                                                            </div></td></tr><tr><td colspan="2" valign="middle" id="utility"><div>\n                                                                &nbsp;<a href="#">unsubscribe from this list</a> | <a href="#">update subscription preferences</a>&nbsp;\n                                                            </div></td></tr></table><!-- // End Module: Standard Footer \\\\ --></td></tr></table><!-- // End Template Footer \\\\ --></td></tr></table><br /></td></tr></table></center></body></html>', '2014-09-24 09:59:33'),
(2, 'maui.lontoc@sumofy.me', 'rupert.cabrera@sumofy.me, karlo.banzuela@sumofy.me', 'paolo.santos@sumofy.me', 'Ahihihi', '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /><style type="text/css">\n		\n			/* Client-specific Styles */\n			#outlook a{padding:0;} /* Force Outlook to provide a "view in browser" button. */\n			body{width:100% !important;} .ReadMsgBody{width:100%;} .ExternalClass{width:100%;} /* Force Hotmail to display emails at full width */\n			body{-webkit-text-size-adjust:none;} /* Prevent Webkit platforms from changing default text sizes. */\n\n			/* Reset Styles */\n			body{margin:0; padding:0;}\n			img{border:0; height:auto; line-height:100%; outline:none; text-decoration:none;}\n			table td{border-collapse:collapse;}\n			#backgroundTable{height:100% !important; margin:0; padding:0; width:100% !important;}\n\n			/* Template Styles */\n\n			/* /\\/\\/\\/\\/\\/\\/\\/\\/\\/\\ STANDARD STYLING: COMMON PAGE ELEMENTS /\\/\\/\\/\\/\\/\\/\\/\\/\\/\\ */\n\n			/**\n			* @tab Page\n			* @section background color\n			* @tip Set the background color for your email. You may want to choose one that matches your company''s branding.\n			* @theme page\n			*/\n			body, #backgroundTable{\n				/*@editable*/ background-color:#FAFAFA;\n			}\n\n			/**\n			* @tab Page\n			* @section email border\n			* @tip Set the border for your email.\n			*/\n			#templateContainer{\n				/*@editable*/ border: 1px solid #DDDDDD;\n			}\n\n			/**\n			* @tab Page\n			* @section heading 1\n			* @tip Set the styling for all first-level headings in your emails. These should be the largest of your headings.\n			* @style heading 1\n			*/\n			h1, .h1{\n				/*@editable*/ color:#202020;\n				display:block;\n				/*@editable*/ font-family:Arial;\n				/*@editable*/ font-size:34px;\n				/*@editable*/ font-weight:bold;\n				/*@editable*/ line-height:100%;\n				margin-top:0;\n				margin-right:0;\n				margin-bottom:10px;\n				margin-left:0;\n				/*@editable*/ text-align:left;\n			}\n\n			/**\n			* @tab Page\n			* @section heading 2\n			* @tip Set the styling for all second-level headings in your emails.\n			* @style heading 2\n			*/\n			h2, .h2{\n				/*@editable*/ color:#202020;\n				display:block;\n				/*@editable*/ font-family:Arial;\n				/*@editable*/ font-size:30px;\n				/*@editable*/ font-weight:bold;\n				/*@editable*/ line-height:100%;\n				margin-top:0;\n				margin-right:0;\n				margin-bottom:10px;\n				margin-left:0;\n				/*@editable*/ text-align:left;\n			}\n\n			/**\n			* @tab Page\n			* @section heading 3\n			* @tip Set the styling for all third-level headings in your emails.\n			* @style heading 3\n			*/\n			h3, .h3{\n				/*@editable*/ color:#202020;\n				display:block;\n				/*@editable*/ font-family:Arial;\n				/*@editable*/ font-size:26px;\n				/*@editable*/ font-weight:bold;\n				/*@editable*/ line-height:100%;\n				margin-top:0;\n				margin-right:0;\n				margin-bottom:10px;\n				margin-left:0;\n				/*@editable*/ text-align:left;\n			}\n\n			/**\n			* @tab Page\n			* @section heading 4\n			* @tip Set the styling for all fourth-level headings in your emails. These should be the smallest of your headings.\n			* @style heading 4\n			*/\n			h4, .h4{\n				/*@editable*/ color:#202020;\n				display:block;\n				/*@editable*/ font-family:Arial;\n				/*@editable*/ font-size:22px;\n				/*@editable*/ font-weight:bold;\n				/*@editable*/ line-height:100%;\n				margin-top:0;\n				margin-right:0;\n				margin-bottom:10px;\n				margin-left:0;\n				/*@editable*/ text-align:left;\n			}\n\n			/* /\\/\\/\\/\\/\\/\\/\\/\\/\\/\\ STANDARD STYLING: PREHEADER /\\/\\/\\/\\/\\/\\/\\/\\/\\/\\ */\n\n			/**\n			* @tab Header\n			* @section preheader style\n			* @tip Set the background color for your email''s preheader area.\n			* @theme page\n			*/\n			#templatePreheader{\n				/*@editable*/ background-color:#FAFAFA;\n			}\n\n			/**\n			* @tab Header\n			* @section preheader text\n			* @tip Set the styling for your email''s preheader text. Choose a size and color that is easy to read.\n			*/\n			.preheaderContent div{\n				/*@editable*/ color:#505050;\n				/*@editable*/ font-family:Arial;\n				/*@editable*/ font-size:10px;\n				/*@editable*/ line-height:100%;\n				/*@editable*/ text-align:left;\n			}\n\n			/**\n			* @tab Header\n			* @section preheader link\n			* @tip Set the styling for your email''s preheader links. Choose a color that helps them stand out from your text.\n			*/\n			.preheaderContent div a:link, .preheaderContent div a:visited, /* Yahoo! Mail Override */ .preheaderContent div a .yshortcuts /* Yahoo! Mail Override */{\n				/*@editable*/ color:#336699;\n				/*@editable*/ font-weight:normal;\n				/*@editable*/ text-decoration:underline;\n			}\n\n			/* /\\/\\/\\/\\/\\/\\/\\/\\/\\/\\ STANDARD STYLING: HEADER /\\/\\/\\/\\/\\/\\/\\/\\/\\/\\ */\n\n			/**\n			* @tab Header\n			* @section header style\n			* @tip Set the background color and border for your email''s header area.\n			* @theme header\n			*/\n			#templateHeader{\n				/*@editable*/ background-color:#FFFFFF;\n				/*@editable*/ border-bottom:0;\n			}\n\n			/**\n			* @tab Header\n			* @section header text\n			* @tip Set the styling for your email''s header text. Choose a size and color that is easy to read.\n			*/\n			.headerContent{\n				/*@editable*/ color:#202020;\n				/*@editable*/ font-family:Arial;\n				/*@editable*/ font-size:34px;\n				/*@editable*/ font-weight:bold;\n				/*@editable*/ line-height:100%;\n				/*@editable*/ padding:0;\n				/*@editable*/ text-align:center;\n				/*@editable*/ vertical-align:middle;\n			}\n\n			/**\n			* @tab Header\n			* @section header link\n			* @tip Set the styling for your email''s header links. Choose a color that helps them stand out from your text.\n			*/\n			.headerContent a:link, .headerContent a:visited, /* Yahoo! Mail Override */ .headerContent a .yshortcuts /* Yahoo! Mail Override */{\n				/*@editable*/ color:#336699;\n				/*@editable*/ font-weight:normal;\n				/*@editable*/ text-decoration:underline;\n			}\n\n			#headerImage{\n				height:auto;\n				max-width:600px !important;\n			}\n\n			/* /\\/\\/\\/\\/\\/\\/\\/\\/\\/\\ STANDARD STYLING: MAIN BODY /\\/\\/\\/\\/\\/\\/\\/\\/\\/\\ */\n\n			/**\n			* @tab Body\n			* @section body style\n			* @tip Set the background color for your email''s body area.\n			*/\n			#templateContainer, .bodyContent{\n				/*@editable*/ background-color:#FFFFFF;\n			}\n\n			/**\n			* @tab Body\n			* @section body text\n			* @tip Set the styling for your email''s main content text. Choose a size and color that is easy to read.\n			* @theme main\n			*/\n			.bodyContent div{\n				/*@editable*/ color:#505050;\n				/*@editable*/ font-family:Arial;\n				/*@editable*/ font-size:14px;\n				/*@editable*/ line-height:150%;\n				/*@editable*/ text-align:left;\n			}\n\n			/**\n			* @tab Body\n			* @section body link\n			* @tip Set the styling for your email''s main content links. Choose a color that helps them stand out from your text.\n			*/\n			.bodyContent div a:link, .bodyContent div a:visited, /* Yahoo! Mail Override */ .bodyContent div a .yshortcuts /* Yahoo! Mail Override */{\n				/*@editable*/ color:#336699;\n				/*@editable*/ font-weight:normal;\n				/*@editable*/ text-decoration:underline;\n			}\n\n			.bodyContent img{\n				display:inline;\n				height:auto;\n			}\n\n			/* /\\/\\/\\/\\/\\/\\/\\/\\/\\/\\ STANDARD STYLING: FOOTER /\\/\\/\\/\\/\\/\\/\\/\\/\\/\\ */\n\n			/**\n			* @tab Footer\n			* @section footer style\n			* @tip Set the background color and top border for your email''s footer area.\n			* @theme footer\n			*/\n			#templateFooter{\n				/*@editable*/ background-color:#FFFFFF;\n				/*@editable*/ border-top:0;\n			}\n\n			/**\n			* @tab Footer\n			* @section footer text\n			* @tip Set the styling for your email''s footer text. Choose a size and color that is easy to read.\n			* @theme footer\n			*/\n			.footerContent div{\n				/*@editable*/ color:#707070;\n				/*@editable*/ font-family:Arial;\n				/*@editable*/ font-size:12px;\n				/*@editable*/ line-height:125%;\n				/*@editable*/ text-align:left;\n			}\n\n			/**\n			* @tab Footer\n			* @section footer link\n			* @tip Set the styling for your email''s footer links. Choose a color that helps them stand out from your text.\n			*/\n			.footerContent div a:link, .footerContent div a:visited, /* Yahoo! Mail Override */ .footerContent div a .yshortcuts /* Yahoo! Mail Override */{\n				/*@editable*/ color:#336699;\n				/*@editable*/ font-weight:normal;\n				/*@editable*/ text-decoration:underline;\n			}\n\n			.footerContent img{\n				display:inline;\n			}\n\n			/**\n			* @tab Footer\n			* @section social bar style\n			* @tip Set the background color and border for your email''s footer social bar.\n			* @theme footer\n			*/\n			#social{\n				/*@editable*/ background-color:#FAFAFA;\n				/*@editable*/ border:0;\n			}\n\n			/**\n			* @tab Footer\n			* @section social bar style\n			* @tip Set the background color and border for your email''s footer social bar.\n			*/\n			#social div{\n				/*@editable*/ text-align:center;\n			}\n\n			/**\n			* @tab Footer\n			* @section utility bar style\n			* @tip Set the background color and border for your email''s footer utility bar.\n			* @theme footer\n			*/\n			#utility{\n				/*@editable*/ background-color:#FFFFFF;\n				/*@editable*/ border:0;\n			}\n\n			/**\n			* @tab Footer\n			* @section utility bar style\n			* @tip Set the background color and border for your email''s footer utility bar.\n			*/\n			#utility div{\n				/*@editable*/ text-align:center;\n			}\n\n			#monkeyRewards img{\n				max-width:190px;\n			}\n		</style></head><body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0"><center><table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="backgroundTable"><tr><td align="center" valign="top"><!-- // Begin Template Preheader \\\\ --><table border="0" cellpadding="10" cellspacing="0" width="600" id="templatePreheader"><tr><td valign="top" class="preheaderContent"><!-- // Begin Module: Standard Preheader \\ --><table border="0" cellpadding="10" cellspacing="0" width="100%"><tr><td valign="top"><div>\n                                                	 Use this area to offer a short teaser of your email''s content. Text here will show in the preview area of some email clients.\n                                                </div></td><!-- *|IFNOT:ARCHIVE_PAGE|* --><td valign="top" width="190"><div>\n                                                	Is this email not displaying correctly?<br /><a href="#" target="_blank">View it in your browser</a>.\n                                                </div></td><!-- *|END:IF|* --></tr></table><!-- // End Module: Standard Preheader \\ --></td></tr></table><!-- // End Template Preheader \\\\ --><table border="0" cellpadding="0" cellspacing="0" width="600" id="templateContainer"><tr><td align="center" valign="top"><!-- // Begin Template Header \\\\ --><table border="0" cellpadding="0" cellspacing="0" width="600" id="templateHeader"><tr><td class="headerContent"><!-- // Begin Module: Standard Header Image \\\\ --><img src="http://gallery.mailchimp.com/653153ae841fd11de66ad181a/images/placeholder_600.gif" style="max-width:600px;" id="headerImage campaign-icon"/><!-- // End Module: Standard Header Image \\\\ --></td></tr></table><!-- // End Template Header \\\\ --></td></tr><tr><td align="center" valign="top"><!-- // Begin Template Body \\\\ --><table border="0" cellpadding="0" cellspacing="0" width="600" id="templateBody"><tr><td valign="top" class="bodyContent"><!-- // Begin Module: Standard Content \\\\ --><table border="0" cellpadding="20" cellspacing="0" width="100%"><tr><td valign="top"><div>\n                                                                This is a freaking test.                                                            </div></td></tr></table><!-- // End Module: Standard Content \\\\ --></td></tr></table><!-- // End Template Body \\\\ --></td></tr><tr><td align="center" valign="top"><!-- // Begin Template Footer \\\\ --><table border="0" cellpadding="10" cellspacing="0" width="600" id="templateFooter"><tr><td valign="top" class="footerContent"><!-- // Begin Module: Standard Footer \\\\ --><table border="0" cellpadding="10" cellspacing="0" width="100%"><tr><td colspan="2" valign="middle" id="social"><div>\n                                                                &nbsp;<a href="#">follow on Twitter</a> | <a href="#">friend on Facebook</a> | <a href="#">forward to a friend</a>&nbsp;\n                                                            </div></td></tr><tr><td valign="top" width="350"><div><em>Copyright &copy; 2014, All rights reserved.</em><br /></div></td><td valign="top" width="190" id="monkeyRewards"><div><strong>Our mailing address is:</strong><br />\n																mail-tester@sumofy.me\n                                                            </div></td></tr><tr><td colspan="2" valign="middle" id="utility"><div>\n                                                                &nbsp;<a href="#">unsubscribe from this list</a> | <a href="#">update subscription preferences</a>&nbsp;\n                                                            </div></td></tr></table><!-- // End Module: Standard Footer \\\\ --></td></tr></table><!-- // End Template Footer \\\\ --></td></tr></table><br /></td></tr></table></center></body></html>', '2014-09-24 09:59:50');

-- --------------------------------------------------------

--
-- Table structure for table `page`
--

CREATE TABLE IF NOT EXISTS `page` (
  `pag_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pag_title` varchar(140) NOT NULL,
  `pct_id` int(10) unsigned DEFAULT '0',
  `pag_slug` varchar(80) DEFAULT NULL,
  `pag_content` text NOT NULL,
  `pag_date_created` datetime NOT NULL,
  `pag_date_published` datetime DEFAULT NULL,
  `pag_type` enum('editable','static') NOT NULL DEFAULT 'editable',
  `pag_status` enum('published','draft') NOT NULL DEFAULT 'published',
  PRIMARY KEY (`pag_id`),
  UNIQUE KEY `slug` (`pag_slug`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `page`
--

INSERT INTO `page` (`pag_id`, `pag_title`, `pct_id`, `pag_slug`, `pag_content`, `pag_date_created`, `pag_date_published`, `pag_type`, `pag_status`) VALUES
(2, 'Page Test', 0, NULL, '<p>Mlkshk Brooklyn organic ugh authentic.  Ethical actually kitsch \nseitan, sustainable stumptown Williamsburg readymade normcore.  Wolf \nHelvetica meh, bespoke biodiesel 90''s occupy church-key Intelligentsia. \n Street art biodiesel kogi, High Life pop-up viral +1 Schlitz gentrify \nfarm-to-table scenester jean shorts cliche.  Vice forage mumblecore ugh \nbiodiesel, kitsch Intelligentsia master cleanse selfies pug.  Try-hard \nWilliamsburg keffiyeh, deep v DIY chambray Schlitz Thundercats ethical \nselfies drinking vinegar.  Distillery mlkshk selvage cornhole.</p><p>Stumptown\n organic hella put a bird on it ethical drinking vinegar.  Deep v \nsustainable PBR&B Odd Future flexitarian 8-bit, gluten-free Wes \nAnderson authentic photo booth.  Polaroid normcore farm-to-table \nstumptown 8-bit, irony roof party Carles PBR&B twee hashtag four \nloko.  Art party locavore cliche tousled skateboard gastropub +1 Odd \nFuture Bushwick.  Mlkshk crucifix leggings irony street art Brooklyn.  \nDeep v slow-carb messenger bag Helvetica asymmetrical farm-to-table.  \nMustache squid kitsch, Schlitz butcher Carles you probably haven''t heard\n of them crucifix brunch viral occupy.</p>', '2014-09-24 08:47:54', '2014-09-06 00:00:00', 'editable', 'published'),
(3, 'Page Test', 0, NULL, '<p>Mlkshk Brooklyn organic ugh authentic.  Ethical actually kitsch \nseitan, sustainable stumptown Williamsburg readymade normcore.  Wolf \nHelvetica meh, bespoke biodiesel 90''s occupy church-key Intelligentsia. \n Street art biodiesel kogi, High Life pop-up viral +1 Schlitz gentrify \nfarm-to-table scenester jean shorts cliche.  Vice forage mumblecore ugh \nbiodiesel, kitsch Intelligentsia master cleanse selfies pug.  Try-hard \nWilliamsburg keffiyeh, deep v DIY chambray Schlitz Thundercats ethical \nselfies drinking vinegar.  Distillery mlkshk selvage cornhole.</p><p>Stumptown\n organic hella put a bird on it ethical drinking vinegar.  Deep v \nsustainable PBR&B Odd Future flexitarian 8-bit, gluten-free Wes \nAnderson authentic photo booth.  Polaroid normcore farm-to-table \nstumptown 8-bit, irony roof party Carles PBR&B twee hashtag four \nloko.  Art party locavore cliche tousled skateboard gastropub +1 Odd \nFuture Bushwick.  Mlkshk crucifix leggings irony street art Brooklyn.  \nDeep v slow-carb messenger bag Helvetica asymmetrical farm-to-table.  \nMustache squid kitsch, Schlitz butcher Carles you probably haven''t heard\n of them crucifix brunch viral occupy.</p>', '2014-09-24 08:48:36', '2014-09-06 00:00:00', 'editable', 'published'),
(4, 'Page Test', 0, 'page-test', '<p>Mlkshk Brooklyn organic ugh authentic.  Ethical actually kitsch \nseitan, sustainable stumptown Williamsburg readymade normcore.  Wolf \nHelvetica meh, bespoke biodiesel 90''s occupy church-key Intelligentsia. \n Street art biodiesel kogi, High Life pop-up viral +1 Schlitz gentrify \nfarm-to-table scenester jean shorts cliche.  Vice forage mumblecore ugh \nbiodiesel, kitsch Intelligentsia master cleanse selfies pug.  Try-hard \nWilliamsburg keffiyeh, deep v DIY chambray Schlitz Thundercats ethical \nselfies drinking vinegar.  Distillery mlkshk selvage cornhole.</p><p>Stumptown\n organic hella put a bird on it ethical drinking vinegar.  Deep v \nsustainable PBR&B Odd Future flexitarian 8-bit, gluten-free Wes \nAnderson authentic photo booth.  Polaroid normcore farm-to-table \nstumptown 8-bit, irony roof party Carles PBR&B twee hashtag four \nloko.  Art party locavore cliche tousled skateboard gastropub +1 Odd \nFuture Bushwick.  Mlkshk crucifix leggings irony street art Brooklyn.  \nDeep v slow-carb messenger bag Helvetica asymmetrical farm-to-table.  \nMustache squid kitsch, Schlitz butcher Carles you probably haven''t heard\n of them crucifix brunch viral occupy.</p>', '2014-09-24 08:49:24', '2014-09-06 00:00:00', 'editable', 'published');

-- --------------------------------------------------------

--
-- Table structure for table `page_category`
--

CREATE TABLE IF NOT EXISTS `page_category` (
  `pct_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pct_name` varchar(50) NOT NULL,
  PRIMARY KEY (`pct_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `page_category`
--

INSERT INTO `page_category` (`pct_id`, `pct_name`) VALUES
(1, 'Category 1');

-- --------------------------------------------------------

--
-- Table structure for table `photo`
--

CREATE TABLE IF NOT EXISTS `photo` (
  `pho_id` int(11) NOT NULL AUTO_INCREMENT,
  `phg_id` int(11) DEFAULT NULL,
  `pho_src` varchar(5000) DEFAULT NULL,
  `pho_caption` varchar(100) DEFAULT NULL,
  `pho_date_created` datetime DEFAULT NULL,
  `pho_created_by` varchar(50) DEFAULT NULL,
  `pho_order` int(11) NOT NULL,
  PRIMARY KEY (`pho_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `photo_gallery`
--

CREATE TABLE IF NOT EXISTS `photo_gallery` (
  `phg_id` int(11) NOT NULL AUTO_INCREMENT,
  `phg_name` varchar(100) DEFAULT NULL,
  `phg_description` text,
  `phg_status` enum('draft','published') DEFAULT 'draft',
  `phg_date_created` datetime DEFAULT NULL,
  `phg_created_by` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`phg_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `session`
--

CREATE TABLE IF NOT EXISTS `session` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(16) NOT NULL DEFAULT '0',
  `user_agent` varchar(50) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `session`
--

INSERT INTO `session` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('5b2efd84483f915191f39b610b6fa216', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1448311028, 'a:7:{s:9:"user_data";s:0:"";s:6:"acc_id";s:1:"2";s:12:"acc_username";s:5:"admin";s:13:"acc_last_name";s:13:"Administrator";s:14:"acc_first_name";s:6:"System";s:8:"acc_type";s:5:"admin";s:8:"acc_name";s:20:"System Administrator";}'),
('9cf19d0e612130a47be444694f98946c', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1448316906, 'a:8:{s:9:"user_data";s:0:"";s:25:"display_notification_type";b:1;s:6:"acc_id";s:1:"2";s:12:"acc_username";s:5:"admin";s:13:"acc_last_name";s:13:"Administrator";s:14:"acc_first_name";s:6:"System";s:8:"acc_type";s:5:"admin";s:8:"acc_name";s:20:"System Administrator";}');

-- --------------------------------------------------------

--
-- Table structure for table `site_options`
--

CREATE TABLE IF NOT EXISTS `site_options` (
  `opt_id` int(11) NOT NULL AUTO_INCREMENT,
  `opt_name` varchar(256) NOT NULL,
  `opt_slug` varchar(100) NOT NULL,
  `opt_type` enum('text','image') NOT NULL DEFAULT 'text',
  `opt_value` text NOT NULL,
  PRIMARY KEY (`opt_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `site_options`
--

INSERT INTO `site_options` (`opt_id`, `opt_name`, `opt_slug`, `opt_type`, `opt_value`) VALUES
(1, 'Site Name', 'site-name', 'text', 'Think Sumo 2'),
(2, 'Facebook Title', 'facebook-title', 'text', 'Think Sumo'),
(3, 'Facebook Description', 'facebook-description', 'text', 'Think Sumo Site'),
(4, 'Facebook Image', 'facebook-image', 'image', './uploads/site_options/facebook-image.png');

-- --------------------------------------------------------

--
-- Table structure for table `time_log`
--

CREATE TABLE IF NOT EXISTS `time_log` (
  `tml_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `acc_id` int(10) unsigned NOT NULL,
  `tml_in` datetime DEFAULT NULL,
  `tml_out` datetime DEFAULT NULL,
  `tml_set_time_in` datetime NOT NULL,
  `tml_set_time_out` datetime NOT NULL,
  `tml_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `tml_deleted` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`tml_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `time_log`
--

INSERT INTO `time_log` (`tml_id`, `acc_id`, `tml_in`, `tml_out`, `tml_set_time_in`, `tml_set_time_out`, `tml_created`, `tml_deleted`) VALUES
(1, 1, '2015-11-24 00:00:00', '2015-11-24 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-11-23 21:11:23', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `usr_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `usr_username` varchar(30) NOT NULL,
  `usr_password` varchar(32) NOT NULL,
  `usr_lname` varchar(50) NOT NULL,
  `usr_fname` varchar(100) NOT NULL,
  `usr_birthdate` date DEFAULT NULL,
  `usr_gender` enum('male','female') DEFAULT NULL,
  `usr_email` varchar(100) DEFAULT NULL,
  `usr_image` text,
  `usr_company` varchar(100) DEFAULT NULL,
  `usr_website` varchar(100) DEFAULT NULL,
  `usr_occupation` varchar(100) DEFAULT NULL,
  `usr_mobile` varchar(50) DEFAULT NULL,
  `usr_landline` varchar(50) DEFAULT NULL,
  `usr_address1` varchar(150) DEFAULT NULL,
  `usr_address2` varchar(150) DEFAULT NULL,
  `usr_city` varchar(100) DEFAULT NULL,
  `usr_country` varchar(50) DEFAULT NULL,
  `usr_zip` varchar(10) DEFAULT NULL,
  `usr_failed_login` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `usr_last_login` datetime DEFAULT NULL,
  `usr_status` enum('active','locked','deleted') NOT NULL DEFAULT 'active',
  `usr_date_created` datetime NOT NULL,
  `usr_verification` varchar(32) DEFAULT NULL,
  `usr_ip_address` varchar(20) NOT NULL,
  PRIMARY KEY (`usr_id`),
  UNIQUE KEY `username` (`usr_username`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_api_ids`
--

CREATE TABLE IF NOT EXISTS `user_api_ids` (
  `uai_id` int(11) NOT NULL AUTO_INCREMENT,
  `usr_id` int(11) NOT NULL,
  `uai_type` enum('facebook','twitter','instagram') NOT NULL,
  `uai_api_id` text NOT NULL,
  `uai_api_token` text NOT NULL,
  `uai_api_secret` text NOT NULL,
  PRIMARY KEY (`uai_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

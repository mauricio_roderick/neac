<div class="row">
	<div class="col-md-9">
		<form method="post"  data-submit="submitForm">
			<div class="box">
				<div class="box-body">
					<div class="form-group">
						<label for="acc_id" class="control-label">Account</label>
						<select name="acc_id" class="form-control">
					<?php foreach($acc_ids->result() as $acc_id): ?>
						<option value="<?php echo $acc_id->acc_id ?>"><?php echo $acc_id->acc_username ?></option>
					<?php endforeach; ?>
						</select>
					</div>
					<div class="form-group">
						<label for="tml_in" class="control-label">In</label>
						<input type="text" name="tml_in" class="form-control datetime" />
					</div>
					<div class="form-group">
						<label for="tml_out" class="control-label">Out</label>
						<input type="text" name="tml_out" class="form-control datetime" />
					</div>
					<div class="form-group">
						<label for="tml_set_time_in" class="control-label">Set Time In</label>
						<input type="text" name="tml_set_time_in" class="form-control datetime" />
					</div>
					<div class="form-group">
						<label for="tml_set_time_out" class="control-label">Set Time Out</label>
						<input type="text" name="tml_set_time_out" class="form-control datetime" />
					</div>
					<div class="form-group">
						<label for="tml_created" class="control-label">Created</label>
						<input type="text" name="tml_created" class="form-control" />
					</div>
					<div class="form-group">
						<label for="tml_deleted" class="control-label">Deleted</label>
						<input type="text" name="tml_deleted" class="form-control" />
					</div>
				</div>
				<div class="box-footer">
					<div class="pull-right">
						<button type="submit" name="form_submit" value='submit' class="btn btn-primary">
							<i class="fa fa-save"></i> Save
						</button>
					</div>
					<a class="btn btn-default" href="<?php echo back_href(); ?>">
						<i class="fa fa-times"></i> Cancel
					</a>
				</div>
			</div>
		</form>
	</div>
</div>
<script type="text/javascript">
$(function() {
	$('form').floodling('acc_id', '<?php echo addslashes($time_log->acc_id); ?>');
	$('form').floodling('tml_in', '<?php echo addslashes($time_log->tml_in); ?>');
	$('form').floodling('tml_out', '<?php echo addslashes($time_log->tml_out); ?>');
	$('form').floodling('tml_set_time_in', '<?php echo addslashes($time_log->tml_set_time_in); ?>');
	$('form').floodling('tml_set_time_out', '<?php echo addslashes($time_log->tml_set_time_out); ?>');
	$('form').floodling('tml_created', '<?php echo addslashes($time_log->tml_created); ?>');
	$('form').floodling('tml_deleted', '<?php echo addslashes($time_log->tml_deleted); ?>');
});
</script>

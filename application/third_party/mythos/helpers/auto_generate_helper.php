<?php
function generate_controller($class_name, $table, $fields, $title_singular, 
	$title_plural, $small_title_singular, $small_title_plural, $module,
	$auth_logged_in, $auth_account_types = '', $auth_users = '')
{
	$controller_template = <<<'EOT'
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class __CLASS_NAME__ extends CI_Controller 
{

	public function __construct() 
	{
		parent::__construct();
		// Call account_type, user, or logged_in methods from Access_control to set restrictions.
__ACCESS_CONTROL__
		// Call validate to check.
		$this->access_control->validate('__MODULE__');
		
		$this->load->model('__TABLE___model');
	}
	
	public function index() 
	{
		$template = array();
		$template['title'] = '__TITLE_PLURAL__';
		
		if($this->input->post('form_mode'))
		{
			$form_mode = $this->input->post('form_mode');
			
			if($form_mode == 'delete')
			{
				$__TABLE___ids = $this->input->post('__TABLE___ids');
				if($__TABLE___ids !== false)
				{
					foreach($__TABLE___ids as $__TABLE___id)
					{
						$__TABLE__ = $this->__TABLE___model->get_one($__TABLE___id);
						if($__TABLE__ !== false)
						{
							$this->__TABLE___model->delete($__TABLE___id);
						}
					}
					$this->template->notification('Selected __SMALL_TITLE_PLURAL__ were deleted.', 'success');
				}
			}
		}
		
		$page = array();
		$page['__CLASS_NAME_SMALL__'] = $this->__TABLE___model->pagination("__MODULE__/__CLASS_NAME_SMALL__/index/__PAGE__", 'get_all');
		$page['__CLASS_NAME_SMALL___pagination'] = $this->__TABLE___model->pagination_links();
		$template['content'] = $this->template->get_view('__CLASS_NAME_SMALL___index', $page, '__MODULE__');
		
		$this->template->render($template, '__MODULE__');
	}
	
	public function create() 
	{
		$template = array();
		$template['title'] = 'Create __TITLE_SINGULAR__';
		
		// Set validation rules
__VALIDATION_RULES__
		
		if($this->input->post('submit'))
		{
			// Call run method from Form_validation to check
			if($this->form_validation->run() !== false)
			{
				$__TABLE__ = $this->extract->post();
				$this->__TABLE___model->create($__TABLE__);
				// Set a notification using notification method from Template.
				// It is okay to redirect after and the notification will be displayed on the redirect page.
				$this->template->notification('New __SMALL_TITLE_SINGULAR__ created.', 'success');
				redirect('__MODULE_PATH__/__CLASS_NAME_SMALL__/');
			}
			else
			{
				// To display validation errors caught by the Form_validation, you should have the code below. 
				$this->template->notification(validation_errors(), 'error');
			}
		}
		
		$template['content'] = $this->template->get_view('__CLASS_NAME_SMALL___create', null, '__MODULE__');
		
		$this->template->render($template, '__MODULE__');
	}
	
	public function edit($__TABLE___id)
	{
		$template = array();
		$template['title'] = 'Edit __TITLE_SINGULAR__';
		
		// Set validation rules
__VALIDATION_RULES__
		
		if($this->input->post('submit'))
		{
			if($this->form_validation->run() !== false)
			{
				$__TABLE__ = $this->extract->post();
				$__TABLE__['__TABLE___id'] = $__TABLE___id;
				$rows_affected = $this->__TABLE___model->update($__TABLE__);
				
				$this->template->notification('__UCF_TITLE_SINGULAR__ updated.', 'success');
				redirect('__MODULE_PATH__/__CLASS_NAME_SMALL__/');
			}
			else
			{
				$this->template->notification(validation_errors());
			}
		}
		
		$page = array();
		$page['__TABLE__'] = $this->__TABLE___model->get_one($__TABLE___id);
		
		if($page['__TABLE__'] === false)
		{
			$this->template->notification('__UCF_TITLE_SINGULAR__ was not found.', 'error');
			redirect('__MODULE_PATH__/__CLASS_NAME_SMALL__/');
		}
		
		$template['content'] = $this->template->get_view('__CLASS_NAME_SMALL___edit', $page, '__MODULE__');
		
		$this->template->render($template, '__MODULE__');
	}
	
	public function view($__TABLE___id)
	{
		$template = array();
		$template['title'] = 'View __TITLE_SINGULAR__';
		
		$page = array();
		$page['__TABLE__'] = $this->__TABLE___model->get_one($__TABLE___id);
		
		if($page['__TABLE__'] === false)
		{
			$this->template->notification('__UCF_TITLE_SINGULAR__ was not found.', 'error');
			redirect('__MODULE_PATH__/__CLASS_NAME_SMALL__/');
		}
		
		$template['content'] = $this->template->get_view('__CLASS_NAME_SMALL___view', $page, '__MODULE__');
		
		$this->template->render($template, '__MODULE__');
	}
	
}
EOT;

	$access_control = '';
	if($auth_logged_in == 'yes' || $auth_account_types != '' || $auth_users != '')
	{
		$access_control .= <<<'EOT'
		$this->access_control->logged_in();
EOT;
		$access_control .= "\n";
		
		if($auth_account_types != '')
		{
			$auth_account_types = str_replace(' , ', ', ', $auth_account_types);
			$auth_account_types = str_replace(',', ', ', $auth_account_types);
			$auth_account_types = "'" . str_replace(', ', "', '", $auth_account_types) . "'";
			
			$access_control .= <<<EOT
		\$this->access_control->account_type({$auth_account_types});
EOT;
			$access_control .= "\n";
		}
		
		if($auth_users != '')
		{
			$auth_users = str_replace(' , ', ', ', $auth_users);
			$auth_users = str_replace(',', ', ', $auth_users);
			$auth_users = "'" . str_replace(', ', "', '", $auth_users) . "'";
			
			$access_control .= <<<EOT
		\$this->access_control->user({$auth_users});
EOT;
			$access_control .= "\n";
		}
		
		$access_control = rtrim($access_control);
	}
	
	$validation_rules = '';
	for($i = 1; $i < count($fields); $i++)
	{
		$field_name = $fields[$i];
		$field_title = humanize($field_name);
		$validation_rules .= <<<EOT
		\$this->form_validation->set_rules('{$field_name}', '{$field_title}', '');
EOT;
		$validation_rules .= "\n";
	}
	$validation_rules = rtrim($validation_rules);
	
	$params['__ACCESS_CONTROL__'] = $access_control;
	$params['__VALIDATION_RULES__'] = $validation_rules;
	$params['__CLASS_NAME__'] = $class_name;
	$params['__CLASS_NAME_SMALL__'] = strtolower($params['__CLASS_NAME__']);
	$params['__USER__'] = $user;
	$params['__TABLE__'] = $table;
	$params['__TITLE_SINGULAR__'] = $title_singular;
	$params['__TITLE_PLURAL__'] = $title_plural;
	$params['__SMALL_TITLE_SINGULAR__'] = $small_title_singular;
	$params['__SMALL_TITLE_PLURAL__'] = $small_title_plural;
	$params['__UCF_TITLE_SINGULAR__'] = ucfirst($params['__SMALL_TITLE_SINGULAR__']);
	$params['__MODULE__'] = $module;
	$params['__MODULE_PATH__'] = '/' . $params['__MODULE__'];

	foreach($params as $key => $value)
	{
		$controller_template = str_replace($key, $value, $controller_template);
	}
	
	if(!is_dir(APPPATH . 'controllers' . $params['__MODULE_PATH__']))
	{
		mkdir(APPPATH . 'controllers' . $params['__MODULE_PATH__']);
	}
	
	$file = fopen(APPPATH . 'controllers' . $params['__MODULE_PATH__'] . '/' . $params['__CLASS_NAME_SMALL__'] . '.php', 'w') or die("Can't open file");
	fwrite($file, $controller_template);
	fclose($file);
}

function generate_model($table, $fields)
{
	$model_template = <<<'EOT'
<?php
// Extend Base_model instead of CI_model
class __CLASS_NAME___model extends Base_model 
{	
	public function __construct() 
	{
		// List all fields of the table.
		// Primary key must be auto-increment and must be listed here first.
		$fields = array(__FIELDS__);
		// Call the parent constructor with the table name and fields as parameters.
		parent::__construct('__TABLE__', $fields);
	}
	
	// Inherits the create, update, delete, get_one, and get_all methods of base_model.
}
EOT;

	$class_name = ucfirst($table);
	
	$fields = "'" . implode("', '", $fields) . "'";
	
	$params = array();
	$params['__CLASS_NAME__'] = $class_name;
	$params['__TABLE__'] = $table;
	$params['__FIELDS__'] = $fields;
	
	foreach($params as $key => $value)
	{
		$model_template = str_replace($key, $value, $model_template);
	}
	
	$file = fopen(APPPATH . 'models/' . $table . '_model.php', 'w') or die("Can't open file");
	fwrite($file, $model_template);
	fclose($file);
}

function generate_view_index($class_name, $table, $fields, $small_title_plural, $module)
{
	$view_template = <<<'EOT'
<?php
if($__CLASS_NAME_SMALL__->num_rows())
{
	?>
	<form method="post">
		<table class="table-list">
			<thead>
				<tr>
					<th class="checkbox skip-sort center"><input type="checkbox" class="select-all" value="__TABLE___ids" /></th>
__FIELD_TITLE_ROWS__
					<th class="skip-sort"></th>
				</tr>
			</thead>
			<tbody>
			<?php
			foreach($__CLASS_NAME_SMALL__->result() as $__TABLE__)
			{
				?>
				<tr>
					<td class="center"><input type="checkbox" name="__TABLE___ids[]" value="<?php echo $__TABLE__->__TABLE___id; ?>" /></td>			
__FIELD_DATA_ROWS__
					<td class="center"><a href="<?php echo site_url('__MODULE__/__CLASS_NAME_SMALL__/edit/' . $__TABLE__->__TABLE___id); ?>">Edit</a></td>
				</tr>
				<?php
			}
			?>
			</tbody>
		</table>
		<?php echo $__CLASS_NAME_SMALL___pagination; ?>
		With selected: 
		<select name="form_mode" class="select-submit">
			<option value="">choose...</option>
			<option value="delete">Delete</option>
		</select>
	</form>
	<?php
}
else
{
	?>
	No page categories found.
	<?php
}
?>
EOT;
	
	$field_title_rows = '';
	$field_data_rows = '';
	for($i = 1; $i < count($fields) && $i <= 4; $i++)
	{
		$field_name = $fields[$i];
		$field_title = humanize($field_name);
		
		$field_title_rows .= <<<EOT
					<th>{$field_title}</th>
EOT;
		$field_title_rows .= "\n";

		if($i == 1) // if first element
		{
			$field_data_rows .= <<<EOT
					<td><a href="<?php echo site_url('__MODULE__/__CLASS_NAME_SMALL__/view/' . \$__TABLE__->__TABLE___id); ?>"><?php echo \$__TABLE__->{$field_name}; ?></a></td>
EOT;
		}
		else
		{
			$field_data_rows .= <<<EOT
					<td><?php echo \$__TABLE__->{$field_name}; ?></td>
EOT;
		}
		$field_data_rows .= "\n";
	}
	$field_title_rows = rtrim($field_title_rows);
	$field_data_rows = rtrim($field_data_rows);
	
	$params = array();
	$params['__FIELD_TITLE_ROWS__'] = $field_title_rows;
	$params['__FIELD_DATA_ROWS__'] = $field_data_rows;
	$params['__CLASS_NAME__'] = $class_name;
	$params['__CLASS_NAME_SMALL__'] = strtolower($params['__CLASS_NAME__']);
	$params['__TABLE__'] = $table;
	$params['__SMALL_TITLE_PLURAL__'] = $small_title_plural;
	$params['__MODULE__'] = $module;
	$params['__MODULE_PATH__'] = '/' . $params['__MODULE__'];
	
	foreach($params as $key => $value)
	{
		$view_template = str_replace($key, $value, $view_template);
	}
	
	if(!is_dir(APPPATH . 'views' . $params['__MODULE_PATH__']))
	{
		mkdir(APPPATH . 'views' . $params['__MODULE_PATH__']);
	}
	
	$file = fopen(APPPATH . 'views' . $params['__MODULE_PATH__'] . '/' . $params['__CLASS_NAME_SMALL__'] . '_index.php', 'w') or die("Can't open file");
	fwrite($file, $view_template);
	fclose($file);
}

function generate_view_create($class_name, $table, $fields, $module)
{
	$view_template = <<<'EOT'
<form method="post">
	<table class="table-form">
__FORM_ROWS__
		<tr>
			<th></th>
			<td>
				<input type="submit" name="submit" value="Submit" class="btn primary" />
				<input type="button" value="Back" onclick="redirect('<?php echo site_url('__MODULE__/__CLASS_NAME_SMALL__'); ?>');" class="btn" />
			</td>
		</tr>
	</table>
</form>
EOT;

	$form_rows = '';
	for($i = 1; $i < count($fields); $i++)
	{
		$field_name = $fields[$i];
		$field_title = humanize($field_name);
		
		$form_rows .= <<<EOT
		<tr>
			<th>{$field_title}</th>
			<td><input type="text" name="{$field_name}" value="" /></td>
		</tr>
EOT;
		$form_rows .= "\n";
	}
	$form_rows = rtrim($form_rows);
	
	$params = array();
	$params['__FORM_ROWS__'] = $form_rows;
	$params['__CLASS_NAME_SMALL__'] = strtolower($class_name);
	$params['__TABLE__'] = $table;
	$params['__MODULE__'] = $module;
	$params['__MODULE_PATH__'] = '/' . $params['__MODULE__'];
	
	foreach($params as $key => $value)
	{
		$view_template = str_replace($key, $value, $view_template);
	}
	
	if(!is_dir(APPPATH . 'views' . $params['__MODULE_PATH__']))
	{
		mkdir(APPPATH . 'views' . $params['__MODULE_PATH__']);
	}
	
	$file = fopen(APPPATH . 'views' . $params['__MODULE_PATH__'] . '/' . $params['__CLASS_NAME_SMALL__'] . '_create.php', 'w') or die("Can't open file");
	fwrite($file, $view_template);
	fclose($file);
}

function generate_view_edit($class_name, $table, $fields, $module)
{
	$view_template = <<<'EOT'
<form method="post">
	<table class="table-form">
__FORM_ROWS__
		<tr>
			<th></th>
			<td>
				<input type="submit" name="submit" value="Submit" class="btn primary" />
				<input type="button" value="Cancel" onclick="redirect('<?php echo site_url('__MODULE__/__CLASS_NAME_SMALL__/view/' . $__TABLE__->__TABLE___id); ?>');" class="btn" />
			</td>
		</tr>
	</table>
</form>
EOT;

	$form_rows = '';
	for($i = 1; $i < count($fields); $i++)
	{
		$field_name = $fields[$i];
		$field_title = humanize($field_name);
		
		$form_rows .= <<<EOT
		<tr>
			<th>{$field_title}</th>
			<td><input type="text" name="{$field_name}" value="<?php echo \$__TABLE__->{$field_name}; ?>" /></td>
		</tr>
EOT;
		$form_rows .= "\n";
	}
	$form_rows = rtrim($form_rows);
	
	$params = array();
	$params['__FORM_ROWS__'] = $form_rows;
	$params['__CLASS_NAME_SMALL__'] = strtolower($class_name);
	$params['__TABLE__'] = $table;
	$params['__MODULE__'] = $module;
	$params['__MODULE_PATH__'] = '/' . $params['__MODULE__'];
	
	foreach($params as $key => $value)
	{
		$view_template = str_replace($key, $value, $view_template);
	}
	
	if(!is_dir(APPPATH . 'views' . $params['__MODULE_PATH__']))
	{
		mkdir(APPPATH . 'views' . $params['__MODULE_PATH__']);
	}
	
	$file = fopen(APPPATH . 'views' . $params['__MODULE_PATH__'] . '/' . $params['__CLASS_NAME_SMALL__'] . '_edit.php', 'w') or die("Can't open file");
	fwrite($file, $view_template);
	fclose($file);
}

function generate_view_view($class_name, $table, $fields, $module)
{
	$view_template = <<<'EOT'

<table class="table-form">
__FORM_ROWS__
	<tr>
		<th></th>
		<td>
			<input type="button" value="Edit" onclick="redirect('<?php echo site_url('__MODULE__/__CLASS_NAME_SMALL__/edit/' . $__TABLE__->__TABLE___id); ?>');" class="btn primary" />
		</td>
	</tr>
</table>
EOT;

	$form_rows = '';
	for($i = 1; $i < count($fields); $i++)
	{
		$field_name = $fields[$i];
		$field_title = humanize($field_name);
		
		$form_rows .= <<<EOT
	<tr>
		<th>{$field_title}</th>
		<td><?php echo \$__TABLE__->{$field_name}; ?></td>
	</tr>
EOT;
		$form_rows .= "\n";
	}
	$form_rows = rtrim($form_rows);
	
	$params = array();
	$params['__FORM_ROWS__'] = $form_rows;
	$params['__CLASS_NAME_SMALL__'] = strtolower($class_name);
	$params['__TABLE__'] = $table;
	$params['__MODULE__'] = $module;
	$params['__MODULE_PATH__'] = '/' . $params['__MODULE__'];
	
	foreach($params as $key => $value)
	{
		$view_template = str_replace($key, $value, $view_template);
	}
	
	if(!is_dir(APPPATH . 'views' . $params['__MODULE_PATH__']))
	{
		mkdir(APPPATH . 'views' . $params['__MODULE_PATH__']);
	}
	
	$file = fopen(APPPATH . 'views' . $params['__MODULE_PATH__'] . '/' . $params['__CLASS_NAME_SMALL__'] . '_view.php', 'w') or die("Can't open file");
	fwrite($file, $view_template);
	fclose($file);
}
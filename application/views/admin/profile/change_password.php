<div class="row">
	<div class="col-md-12 col-lg-4">
		<form method="post" enctype="multipart/form-data" data-submit="submitForm">
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">
						<i class="fa fa-lock"></i>
						Change Password
					</h3>
				</div>
				<div class="box-body">
					<div class="form-group">
						<label for="old_password" class="control-label">Old Password</label>
						<input class="form-control input-sm" type="password" name="old_password" />
					</div>
					<div class="form-group">
						<label for="new_password" class="control-label">New Password</label>
						<input class="form-control input-sm" type="password" name="new_password" />
					</div>
					<div class="form-group">
						<label for="new_password2" class="control-label">Retype New Password</label>
						<input class="form-control input-sm" type="password" name="new_password2" />
					</div>
				</div>
				<div class="box-footer">
					<a class="btn btn-default" href="<?php echo back_href(); ?>"><i class="fa fa-times"></i> Cancel</a>
					<div class="pull-right">
						<button type="submit" name="form_submit" value='submit' class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
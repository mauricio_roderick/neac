<div class="col-sm-3 col-sm-push-9 block">
	<div class="box">
		<div class="box-header">
			<h3 class="box-title">Menu</h3>
		</div>
		<div class="box-body">
			<div class="btn-group">
				<a href="<?php echo admin_url('users/edit/' . $user->usr_id); ?>" class="btn btn-primary btn-flat btn-sm">
					<i class="fa fa-edit"></i> Edit
				</a>
				<?php echo create_admin_back_btn($this->router->fetch_class()); ?>
			</div>
		</div>
	</div>
</div>
<div class="col-sm-9 col-sm-pull-3">
	<div class="box">
		<div class="box-body no-padding table-responsive">
			<table class="table table-bordered table-hover table-view">
				<tr>
					<th>Username</th>
					<td><?php echo $user->usr_username; ?></td>
				</tr>
				<tr>
					<th>Password</th>
					<td><?php echo $user->usr_password; ?></td>
				</tr>
				<tr>
					<th>Lname</th>
					<td><?php echo $user->usr_lname; ?></td>
				</tr>
				<tr>
					<th>Fname</th>
					<td><?php echo $user->usr_fname; ?></td>
				</tr>
				<tr>
					<th>Birthdate</th>
					<td><?php echo format_datetime($user->usr_birthdate); ?></td>
				</tr>
				<tr>
					<th>Gender</th>
					<td><?php echo $user->usr_gender; ?></td>
				</tr>
				<tr>
					<th>Email</th>
					<td><?php echo $user->usr_email; ?></td>
				</tr>
				<tr>
					<th>Image</th>
					<td><?php echo nl2br($user->usr_image); ?></td>
				</tr>
				<tr>
					<th>Company</th>
					<td><?php echo $user->usr_company; ?></td>
				</tr>
				<tr>
					<th>Website</th>
					<td><?php echo $user->usr_website; ?></td>
				</tr>
				<tr>
					<th>Occupation</th>
					<td><?php echo $user->usr_occupation; ?></td>
				</tr>
				<tr>
					<th>Mobile</th>
					<td><?php echo $user->usr_mobile; ?></td>
				</tr>
				<tr>
					<th>Landline</th>
					<td><?php echo $user->usr_landline; ?></td>
				</tr>
				<tr>
					<th>Address1</th>
					<td><?php echo $user->usr_address1; ?></td>
				</tr>
				<tr>
					<th>Address2</th>
					<td><?php echo $user->usr_address2; ?></td>
				</tr>
				<tr>
					<th>City</th>
					<td><?php echo $user->usr_city; ?></td>
				</tr>
				<tr>
					<th>Country</th>
					<td><?php echo $user->usr_country; ?></td>
				</tr>
				<tr>
					<th>Zip</th>
					<td><?php echo $user->usr_zip; ?></td>
				</tr>
				<tr>
					<th>Failed Login</th>
					<td><?php echo number_format($user->usr_failed_login); ?></td>
				</tr>
				<tr>
					<th>Last Login</th>
					<td><?php echo format_datetime($user->usr_last_login); ?></td>
				</tr>
				<tr>
					<th>Status</th>
					<td><?php echo $user->usr_status; ?></td>
				</tr>
				<tr>
					<th>Date Created</th>
					<td><?php echo format_datetime($user->usr_date_created); ?></td>
				</tr>
				<tr>
					<th>Verification</th>
					<td><?php echo $user->usr_verification; ?></td>
				</tr>
			</table>
		</div>
	</div>
</div>
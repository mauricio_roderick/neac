<div class="row">
	<div class="col-md-3">
		<a href="<?php echo admin_url('banners/create') ?>" class="btn btn-primary btn-block margin-bottom">
			<i class="fa fa-plus-square"></i> Create
		</a>
		<div class="box box-solid">
			<div class="box-header with-border">
				<h3 class="box-title">Menu</h3>
			</div>
			<div class="box-body no-padding">
				<ul class="nav nav-pills nav-stacked">
					<li>
						<a href="<?php echo admin_url('banners') ?>"><i class="fa fa-circle-o"></i> All</a>
					</li>
					<li>
						<a href="<?php echo admin_url('banners/index/published') ?>"><i class="fa fa-circle-o text-blue"></i> Published </a>
					</li>
					<li>
						<a href="<?php echo admin_url('banners/index/draft') ?>"><i class="fa fa-circle-o text-yellow"></i> Draft</a>
					</li>
					<li>
						<a href="<?php echo admin_url('banners/index/archived') ?>"><i class="fa fa-circle-o text-red"></i> Archived</a>
					</li>
				</ul>
			</div><!-- /.box-body -->
		</div><!-- /. box -->
	</div>

	<div class="col-md-9">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title"><?php echo $banner->ban_title ?></h3>
				<div class="box-tools pull-right">
                    <span class="label label-default">Published: <?php echo $banner->ban_published; ?></span> 
                    <span class="label label-info">Link: <?php echo $banner->ban_link; ?></span> 
                </div>
			</div>
			<div class="box-body">
                <div class="mailbox-read-message text-center">
            		<?php if($banner->ban_image!=""){?>
					<div id="thumb">
						<img src="<?php echo base_url($banner->ban_image); ?>" />
					</div>
					<br />

					<a class="btn-btn-link" href="<?php echo base_url($banner->ban_image); ?>" target="_blank">Download Image</a>
					<?php } ?>
                </div>
				<div class="mailbox-read-message">
                    <h3><?php echo $banner->ban_description ?></h3>
                </div>
			</div>
			<div class="box-footer">
				<a class="btn btn-default" href="<?php echo admin_url('banners/edit/'.$banner->ban_id); ?>"><i class="fa fa-edit"></i> Edit</a>
			</div>
		</div>
	</div>
</div>
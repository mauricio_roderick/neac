<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Fangate extends CI_Controller 
{

	public function __construct() 
	{
		parent::__construct();
		$this->load->helper('format');
		$this->mythos->library('facebook');
	}
	
	public function index() 
	{
		$this->template->title('Fangate');

		$content = array();

		if($this->facebook->is_liked()){
			redirect('home');
		} else {
			$this->load->view('site/fangate_view', $content);
		}
	}
}

<?php
// Extend Base_model instead of CI_model
class Time_log_model extends Base_model
{
	public function __construct()
	{
		// List all fields of the table.
		// Primary key must be auto-increment and must be listed here first.
		$fields = array(
			'tml_id', 
			'acc_id', 
			'tml_in', 
			'tml_out', 
			'tml_set_time_in', 
			'tml_set_time_out', 
			'tml_created', 
			'tml_deleted'
		);

		$searchable_fields = array('acc_id');

		// Call the parent constructor with the table name and fields as parameters.


		parent::__construct('time_log', $fields, $searchable_fields, null);
	}

	// Inherits the create, update, delete, get_one, and get_all methods of base_model.
	public function create($data, $field_list = array())
	{

		return parent::create($data, $field_list);
	}

	public function update($data, $field_list = array())
	{

		return parent::update($data, $field_list);
	}

	public function get_one($id)
	{
		$this->db->join("account", "account.acc_id = time_log.acc_id");

		return parent::get_one($id);
	}

	public function get_all($params = array(), $order_by = array())
	{
		$this->db->join("account", "account.acc_id = time_log.acc_id");

		return parent::get_all($params, $order_by);
	}

	public function delete($id)
	{
		$params[$this->fields[0]] = $id;
		$params[""] = "";
		return parent::update($params);

	}
}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Facebook_connect
{
	private $CI;
	
	public function __construct() 
	{
		$this->CI =& get_instance();
	}

	//RETRIEVES AN ACCESS TOKEN FOR THE CURRENT USER
	public function get_access_token($permissions = ""){
		$fb_config = $this->CI->config->load('facebook', TRUE);
		
		$app_id = $this->CI->config->item('app_id', 'facebook');
		$app_secret = $this->CI->config->item('app_secret', 'facebook');		
		$my_url = current_url();
		
		$code = $this->CI->input->get('code');

		if(empty($code)) 
		{
			$state = md5(uniqid(rand(), TRUE));
			$this->CI->session->set_userdata('state', $state);
			
			$dialog_url = "https://www.facebook.com/dialog/oauth?client_id=" 
			. $app_id . "&redirect_uri=" . urlencode($my_url) . "&state="
			. $state;

			if($permissions != "") {
				$dialog_url = $dialog_url. "&scope=$permissions";
			}
			
			redirect($dialog_url);
		} 
		else 
		{
			$state2 = $this->CI->input->get('state');
			$state = $this->CI->session->userdata('state');
			
			if($state && ($state === $state2))
			{
				$token_url = "https://graph.facebook.com/oauth/access_token?"
				. "client_id=" . $app_id . "&redirect_uri=" . urlencode($my_url)
				. "&client_secret=" . $app_secret . "&code=" . $code;

				$response = file_get_contents($token_url);
				$params = null;

				parse_str($response, $params);

				return $params['access_token'];
			}
		    else 
			{
				return false;
		   }
		}
	}
	
	public function get_user($access_token){
		$graph_url = "https://graph.facebook.com/me?access_token=".$access_token;

		$user = json_decode(file_get_contents($graph_url));
		
		return $user;
	}
}

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Banners extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->access_control->logged_in();
		$this->access_control->account_type('dev', 'admin');
		$this->access_control->validate();

		$this->load->model('banner_model');
		$this->load->helper('nav');
	}

	public function index($ban_published = "all")
	{
		$this->template->title('Banners');

		if($this->input->post('form_mode'))
		{
			$form_mode = $this->input->post('form_mode');

			if($form_mode == 'delete')
			{
				$ban_ids = $this->input->post('ban_ids');
				if($ban_ids !== false)
				{
					foreach($ban_ids as $ban_id)
					{
						$banner = $this->banner_model->get_one($ban_id);
						if($banner !== false)
						{
							$this->banner_model->delete($ban_id);
						}
					}
					$this->template->notification('Selected banners were deleted.', 'success');
					redirect('admin/banners');
				}
			}
		}

		$page = array();
		$params = array();

		$get = "";

		if($this->input->get())
		{
			$get = "?".$_SERVER['QUERY_STRING'];
		}

		if($ban_published != 'all')
		{
			$params['ban_published'] = $ban_published;
		}
		else
		{
			$params['ban_published !='] = 'archived';
		}

		$page['banners'] = $this->banner_model->pagination("admin/banners/index/$ban_published/__PAGE__/$get", 'search', $this->input->get('search'), $params);
		$page['banners_pagination'] = $this->banner_model->pagination_links();
		$this->template->content('banners-index', $page);
		$this->template->content('banners-menu', null, 'admin', 'page-nav');
		$this->template->show();
	}

	public function create()
	{

		$this->template->title('Create Banner');


		// Use the set_rules from the Form_validation class for form validation.
		// Already combined with jQuery. No extra coding required for JS validation.
		// We get both JS and PHP validation which makes it both secure and user friendly.
		// NOTE: Set the rules before you check if $_POST is set so that the jQuery validation will work.
		$this->form_validation->set_rules('ban_title', 'Title', 'trim|required|max_length[100]');
		$this->form_validation->set_rules('ban_description', 'Description', 'trim|max_length[255]');
		$this->form_validation->set_rules('ban_image', 'Image', 'trim');
		$this->form_validation->set_rules('ban_link', 'Link', 'trim|max_length[500]');
		$this->form_validation->set_rules('ban_published', 'Published', 'trim|required');

		if($this->input->post('form_submit'))
		{
			$banner = $this->extract->post();

			// Call run method from Form_validation to check
			if($this->form_validation->run() !== false)
			{

				//$data = $this->upload->do_upload_resize("upload_pic", $this->img_width, $this->img_height, './uploads/banners/');
				$id = $this->banner_model->create($banner);

				// Set a notification using notification me	thod from Template.
				// It is okay to redirect after and the notification will be displayed on the redirect page.
				$this->template->notification('New banner created.', 'success');
				$id = $id['result']['insert_id'];
				redirect("/admin/banners/edit/$id");
			}
			else
			{
				// To display validation errors caught by the Form_validation, you should have the code below.
				$this->template->notification(validation_errors(), 'danger');
			}

			$this->template->autofill($banner);
		}

		$page = array();

		$this->template->content('banners-create', $page);
		$this->template->content('banners-menu', null, 'admin', 'page-nav');
		$this->template->show();
	}

	public function edit($ban_id)
	{
		$this->template->title('Edit Banner');


		$this->form_validation->set_rules('ban_title', 'Title', 'trim|required|max_length[100]');
		$this->form_validation->set_rules('ban_description', 'Description', 'trim|max_length[255]');
		$this->form_validation->set_rules('ban_image', 'Image', 'trim');
		$this->form_validation->set_rules('ban_link', 'Link', 'trim|max_length[500]');
		$this->form_validation->set_rules('ban_published', 'Published', 'trim|required');


		if($this->input->post('form_submit'))
		{
			$banner = $this->extract->post();

			// Call run method from Form_validation to check
			if($this->form_validation->run() !== false)
			{
				$banner['ban_id'] = $ban_id;

				//$data = $this->upload->do_upload_resize("upload_pic", $this->img_width, $this->img_height, './uploads/banners/');
				$id = $this->banner_model->update($banner);

				// Set a notification using notification me	thod from Template.
				// It is okay to redirect after and the notification will be displayed on the redirect page.
				$this->template->notification('Banner updated.', 'success');
				redirect("/admin/banners/edit/$ban_id");
			}
			else
			{
				// To display validation errors caught by the Form_validation, you should have the code below.
				$this->template->notification(validation_errors(), 'danger');
			}

			$this->template->autofill($banner);
		}

		$page = array();
		$page['banner'] = $this->banner_model->get_one($ban_id);

		if($page['banner'] === false)
		{
			$this->template->notification('Banner was not found.', 'danger');
			redirect('admin/banners');
		}

		$this->template->content('banners-edit', $page);
		$this->template->content('banners-menu', null, 'admin', 'page-nav');
		$this->template->show();
	}

	public function view($banner_id)
	{
		$this->template->title('View Banner');

		$page = array();
		$page['banner'] = $this->banner_model->get_one($banner_id);

		if($page['banner'] === false)
		{
			$this->template->notification('Banner was not found.', 'danger');
			redirect('admin/banners');
		}

		$this->template->content('banners-view', $page);
		$this->template->show();
	}

	public function reorder()
	{
		if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')) 
		{
			$post = $this->extract->post();


			if(isset($post['otherKey']))
			{
				$response = $this->banner_model->reorder($post['primKey'], $post['otherKey'], $post['order']);
			}
			else
			{
				$response = $this->banner_model->reorder($post['primKey'], null , $post['order']);
			}

			$data['response'] = $response;
			echo json_encode($data);
		}	
	}
}

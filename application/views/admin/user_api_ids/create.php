<form method="post">
	<div class="col-sm-3 col-sm-push-9 block">
		<div class="box">
			<div class="box-header">
				<h3 class="box-title">Menu</h3>
			</div>
			<div class="box-body">
				<?php echo create_admin_back_btn($this->router->fetch_class(), 'Cancel'); ?>
			</div>
		</div>
	</div>
	<div class="col-sm-9 col-sm-pull-3">
		<div class="box">
			<div class="box-body">
				<div class="form-group">
					<label for="usr_username" class="control-label">User</label>		
					<select class="form-control" name="usr_id">
					<?php
					foreach($usr_ids->result() as $usr_id) 
					{
						?>
						<option value="<?php echo $usr_id->usr_id; ?>"><?php echo $usr_id->usr_username; ?></option>
						<?php
					}
					?>
					</select>
				</div>
				<div class="form-group">
					<label for="uai_type" class="control-label">Type</label>
					<select class="form-control" name="uai_type">
						<option value="facebook">facebook</option>
						<option value="twitter">twitter</option>
						<option value="instagram">instagram</option>
					</select>
				</div>
				<div class="form-group">
					<label for="uai_api_id" class="control-label">Api Id</label>
					<textarea class="form-control" name="uai_api_id" rows="5" cols="80"></textarea>
				</div>
				<div class="form-group">
					<label for="uai_api_token" class="control-label">Api Token</label>
					<textarea class="form-control" name="uai_api_token" rows="5" cols="80"></textarea>
				</div>
				<div class="form-group">
					<label for="uai_api_secret" class="control-label">Api Secret</label>
					<textarea class="form-control" name="uai_api_secret" rows="5" cols="80"></textarea>
				</div>
				<div class="form-group">
					<input type="submit" name="submit" value="Submit" class="btn btn-primary" />
				</div>
			</div>
		</div>
	</div>
</form>
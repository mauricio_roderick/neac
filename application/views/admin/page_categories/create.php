<?php
// Normal form here. Form validation are taken care of by the controller.
// Make sure to name your form elements properly and uniquely.
?>
<form method="post">
	<div class="col-sm-3 col-sm-push-9 block">
		<div class="box">
			<div class="box-header">
				<h3 class="box-title">Menu</h3>
			</div>
			<div class="box-body">
				<?php echo create_admin_back_btn($this->router->fetch_class(), 'Cancel'); ?>
			</div>
		</div>
	</div>
	<div class="col-sm-9 col-sm-pull-3">
		<div class="box">
			<div class="box-body">
				<div class="form-group">
					<label for="pct_name" class="control-label">Category Name</label>
					<input class="form-control" type="text" name="pct_name" />
				</div>
				<div class="form-group">
					<input type="submit" name="submit" value="Submit" class="btn btn-primary btn-flat btn-small" />
				</div>
			</div>
		</div>
	</div>
</form>

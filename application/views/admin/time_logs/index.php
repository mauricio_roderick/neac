<div class="row">
	<!-- <div class="col-md-3">
		<a href="<?php echo admin_url('time_logs/create') ?>" class="btn btn-primary btn-block margin-bottom">
			<i class="fa fa-plus-square"></i> Create
		</a>
	</div> -->

	<div class="col-md-12">
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">Manage Time Logs</h3>
				<div class="box-tools pull-right">
					<div class="has-feedback">
						<form role="form" method="get">
							<input type="text" class="form-control input-sm" name="search" placeholder="Search" value="<?php echo $this->input->get('search') ?>" />
							<span class="glyphicon glyphicon-search form-control-feedback"></span>
						</form>
					</div>
				</div><!-- /.box-tools -->
			</div>
			<?php if($time_logs->num_rows()): ?>
			<div class="box-body table-responsive no-padding">
				<form role="form" method="post">
					<div class="mailbox-controls">
					<!-- Check all button -->
						<div class="btn btn-default btn-sm checkbox-toggle select-all" data-fields="tml_ids">
							<i class="fa fa-square-o"></i>
						</div>
						<div class="btn-group">
							<button class="btn btn-default btn-sm action-delete" name="form_mode" value="delete"> 
								<i class="fa fa-trash-o"></i> Delete All
							</button>
						</div><!-- /.btn-group --> 
						<div class="pull-right">
							<?php echo $time_logs_pagination; ?>
						</div><!-- /.pull-right -->
					</div>
					<table class="table table-striped table-hover">
						<thead>
							<tr>
								<th></th>
								<th>Account</th>
								<th>Actual Time In</th>
								<th>Actual Time Out</th>
								<th>Required Time In</th>
								<th>Required Time Out</th>
							</tr>
						</thead>
						<tbody>
						<?php foreach($time_logs->result() as $time_log): ?>
							<tr data-primkey="<?php echo $time_log->tml_id; ?>">
								<td style="width:50px" class="center">
									<input type="checkbox" name="tml_ids[]" value="<?php echo $time_log->tml_id; ?>" />
								</td>
								<td>
									<a href="<?php echo admin_url('time_logs/edit/'.$time_log->tml_id); ?>"><?php echo $time_log->acc_username?></a>
								</td>
								<td>
									<?php echo format_datetime($time_log->tml_in) ?>
								</td>
								<td>
									<?php echo format_datetime($time_log->tml_in) ?>
								</td>
								<td>
									<?php echo format_time($time_log->tml_set_time_in) ?>
								</td>
								<td>
									<?php echo format_time($time_log->tml_set_time_out) ?>
								</td>
							</tr>
						<?php endforeach; ?>
						</tbody>
					</table>
				</form>
			</div>
			<?php else: ?>
			<div class="box-body">
				<p class="text-center" style="padding: 10px;">
					No time logs found.
				</p>
			</div>
			<?php endif; ?>
		</div>
	</div>
</div>
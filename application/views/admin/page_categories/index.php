<form class="form-inline" role="form" method="post">
	<div class="box">
		<div class="box-header">
			<div class="box-tools clearfix">
				<div class="btn-group">
					<button class="btn btn-flat btn-default btn-sm" name="form_mode" value="delete">
						<i class="fa fa-trash-o"></i>
					</button>
				</div>
				<!-- <div class="pull-right">
					<div class="form-group">
						<label for="field" class="sr-only">Fields</label>
						<select name="field" id="" class="form-control input-sm">
							<option value="part_name"></option>
							<option value="part_name"></option>
						</select>
					</div>
					<div class="form-group input-group">
						<label for="keywords" class="sr-only">Keywords</label>
						<input type="text" class="form-control input-sm" name="keywords" placeholder="Filter" value="">
						<div class="input-group-btn">
							<button class="btn btn-flat btn-primary btn-sm" name="search" type="submit" value="search">
								<i class="fa fa-search"></i>
							</button>
						</div>
					</div>
				</div> -->
			</div>
		</div>
		<?php if ($page_categories->num_rows()): ?>
			<div class="box-body no-padding table-responsive">
				<table class="table table-striped table-hover">
					<thead>
						<tr>
							<th class="center skip-sort" style="width: 30px;"><input type="checkbox" class="select-all" value="pct_ids" /></th>
							<th>Category Name</th>
							<th style="width: 120px">Action</th>
						</tr>
					</thead>
					<tbody>
				<?php
				foreach($page_categories->result() as $page_category)
				{
					?>
					<tr>
						<td class="center"><input type="checkbox" name="pct_ids[]" value="<?php echo $page_category->pct_id; ?>" /></td>
						<td><?php echo $page_category->pct_name; ?></td>
						<td>
							<div class="btn-group">
								<button class="btn btn-primary btn-flat btn-disabled btn-table-action">Action</button>
								<button class="btn btn-primary btn-flat dropdown-toggle" data-toggle="dropdown">
									<span class="caret"></span>
									<span class="sr-only">Toggle Dropdown</span>
								</button>
								<ul class="dropdown-menu table-action" role="menu">
									<li>
										<a href="<?php echo admin_url('page_categories/edit/' . $page_category->pct_id); ?>"><i class="fa fa-edit"></i> Edit</a>
									</li>
								</ul>
							</div>
						</td>
					</tr>
					<?php
				}
				?>
					</tbody>
				</table>
			</div>
			<div class="box-footer clearfix">
				<?php echo $page_categories_pagination; ?>
			</div>
		<?php else: ?>
			<div class="box-body no-padding">
				<p class="text-center" style="padding: 10px;">
					No page categories found.
				</p>
			</div>
		<?php endif; ?>
	</div>
</form>

<div class="row">
	<div class="col-md-3">
		<a href="<?php echo admin_url('users/create') ?>" class="btn btn-primary btn-block margin-bottom">
			<i class="fa fa-plus-square"></i> Create
		</a>
		<div class="box box-solid">
			<div class="box-header with-border">
				<h3 class="box-title">Menu</h3>
			</div>
			<div class="box-body no-padding">
				<ul class="nav nav-pills nav-stacked">
					<li>
						<a href="<?php echo admin_url('users') ?>"><i class="fa fa-circle-o"></i> All</a>
					</li>
					<li>
						<a href="<?php echo admin_url('users/index/active') ?>"><i class="fa fa-circle-o text-blue"></i> Active </a>
					</li>
					<li>
						<a href="<?php echo admin_url('users/index/locked') ?>"><i class="fa fa-circle-o text-red"></i> Locked</a>
					</li>
				</ul>
			</div><!-- /.box-body -->
		</div><!-- /. box -->
	</div>

	<div class="col-md-9">
		<form method="post" enctype="multipart/form-data" data-submit="submitForm">
			<div class="box">
				<div class="box-body">
					<div class="form-group">
						<label for="usr_username" class="control-label">Username</label>
						<input class="form-control" type="text" name="usr_username" size="30" maxlength="30" value="" />
					</div>
					<div class="form-group">
						<label for="usr_lname" class="control-label">Last name</label>
						<input class="form-control" type="text" name="usr_lname" size="50" maxlength="50" value="" />
					</div>
					<div class="form-group">
						<label for="usr_fname" class="control-label">First name</label>
						<input class="form-control" type="text" name="usr_fname" size="80" maxlength="100" value="" />
					</div>
					<div class="form-group">
						<label for="usr_birthdate" class="control-label">Birthdate</label>
						<input class="form-control" type="text" name="usr_birthdate" class="datetime" value="" />
					</div>
					<div class="form-group">
						<label for="usr_gender" class="control-label">Gender</label>
						<select class="form-control" name="usr_gender">
							<option value="male">male</option>
							<option value="female">female</option>
						</select>
					</div>
					<div class="form-group">
						<label for="usr_email" class="control-label">Email</label>
						<input class="form-control" type="text" name="usr_email" size="80" maxlength="100" value="" />
					</div>
					<div class="form-group">
						<label for="usr_company" class="control-label">Company</label>
						<input class="form-control" type="text" name="usr_company" size="80" maxlength="100" value="" />
					</div>
					<div class="form-group">
						<label for="usr_website" class="control-label">Website</label>
						<input class="form-control" type="text" name="usr_website" size="80" maxlength="100" value="" />
					</div>
					<div class="form-group">
						<label for="usr_occupation" class="control-label">Occupation</label>
						<input class="form-control" type="text" name="usr_occupation" size="80" maxlength="100" value="" />
					</div>
					<div class="form-group">
						<label for="usr_mobile" class="control-label">Mobile</label>
						<input class="form-control" type="text" name="usr_mobile" size="50" maxlength="50" value="" />
					</div>
					<div class="form-group">
						<label for="usr_landline" class="control-label">Landline</label>
						<input class="form-control" type="text" name="usr_landline" size="50" maxlength="50" value="" />
					</div>
					<div class="form-group">
						<label for="usr_address1" class="control-label">Address1</label>
						<input class="form-control" type="text" name="usr_address1" size="80" maxlength="150" value="" />
					</div>
					<div class="form-group">
						<label for="usr_address2" class="control-label">Address2</label>
						<input class="form-control" type="text" name="usr_address2" size="80" maxlength="150" value="" />
					</div>
					<div class="form-group">
						<label for="usr_city" class="control-label">City</label>
						<input class="form-control" type="text" name="usr_city" size="80" maxlength="100" value="" />
					</div>
					<div class="form-group">
						<label for="usr_country" class="control-label">Country</label>
						<input class="form-control" type="text" name="usr_country" size="50" maxlength="50" value="" />
					</div>
					<div class="form-group">
						<label for="usr_zip" class="control-label">Zip</label>
						<input class="form-control" type="text" name="usr_zip" size="10" maxlength="10" value="" />
					</div>
					<div class="form-group">
						<label for="usr_status" class="control-label">Status</label>
						<select class="form-control" name="usr_status">
							<option value="active">active</option>
							<option value="locked">locked</option>
							<option value="deleted">deleted</option>
						</select>
					</div>
				</div>
				<div class="box-footer">
					<div class="pull-right">
						<button type="submit" name="form_submit" value='submit' class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
					</div>
					<a class="btn btn-default" href="<?php echo back_href(); ?>"><i class="fa fa-times"></i> Cancel</a>
				</div>
			</div>
		</form>
	</div>
</div>
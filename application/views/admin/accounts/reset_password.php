<div class="row">
	<div class="col-md-3">
		<a href="<?php echo admin_url('accounts/create') ?>" class="btn btn-primary btn-block margin-bottom">
			<i class="fa fa-plus-square"></i> Create
		</a>
		<div class="box box-solid">
			<div class="box-header with-border">
				<h3 class="box-title">Menu</h3>
			</div>
			<div class="box-body no-padding">
				<ul class="nav nav-pills nav-stacked">
					<li>
						<a href="<?php echo admin_url('accounts') ?>"><i class="fa fa-circle-o"></i> All</a>
					</li>
					<li>
						<a href="<?php echo admin_url('accounts/index/active') ?>"><i class="fa fa-circle-o text-blue"></i> Active </a>
					</li>
					<li>
						<a href="<?php echo admin_url('accounts/index/locked') ?>"><i class="fa fa-circle-o text-red"></i> Locked</a>
					</li>
				</ul>
			</div><!-- /.box-body -->
		</div><!-- /. box -->
	</div>
	<div class="col-md-9 col-lg-5">
		<div class="box box-default">
			<div class="box-header with-border">
				<h3 class="box-title">Manage accounts</h3>
				<div class="box-tools pull-right">
					<div class="has-feedback">
						<form role="form" method="get">
							<input type="text" class="form-control input-sm" name="search" placeholder="Search" />
							<span class="glyphicon glyphicon-search form-control-feedback"></span>
						</form>
					</div>
				</div><!-- /.box-tools -->
			</div>
			<?php if ($accounts->num_rows()): ?>
			<div class="box-body table-responsive no-padding">
				<form role="form" method="post">
					<div class="mailbox-controls">
					<!-- Check all button -->
						<div class="btn btn-default btn-sm checkbox-toggle select-all" data-fields="acc_ids"><i class="fa fa-square-o"></i></div>
						<div class="btn-group">
							<button class="btn btn-default btn-sm action-delete" name="form_mode" value="delete"> <i class="fa fa-trash-o"></i> Delete All</button>
						</div><!-- /.btn-group --> 
						<div class="pull-right">
							<?php echo $accounts_pagination; ?>
						</div><!-- /.pull-right -->
					</div>
					<table class="table table-striped table-hover">
						<tbody>
						<?php
						foreach($accounts->result() as $acc)
						{
						?>
							<tr>
								<td style="width:50px" class="center">
									<input type="checkbox" name="acc_ids[]" value="<?php echo $acc->acc_id; ?>" />
								</td>
								<td>
									<a href="<?php echo admin_url('accounts/view/'.$acc->acc_id) ?>">
										<i class="glyphicon glyphicon-user"></i>
										<?php echo $acc->acc_first_name; ?>
										<?php echo $acc->acc_last_name; ?>
										<br>
										<small><?php echo $acc->acc_username; ?></small>
									</a>
								</td>
								<td>
									
								</td>
								<td class='text-right' style="width:300px">
									<span class="label label-info"><?php echo $acc->acc_type; ?></span> 
									<span class="label label-default"><?php echo $acc->acc_status; ?></span> 
								</td>
							</tr>
						<?php
						}
						?>
						</tbody>
					</table>
				</form>
			</div>
			<?php else: ?>
			<div class="box-body">
				<p class="text-center" style="padding: 10px;">
					No accounts found.
				</p>
			</div>
			<?php endif; ?>
		</div>
	</div>
	<div class="col-md-12 col-lg-4">
		<form method="post" data-submit="submitForm">
			<div class="box box-primary">
				<div class="box-body">				
					<div class="form-group">
						<label for="acc_username" class="control-label">Account</label>
						<?php echo $account->acc_first_name . ' ' . $account->acc_last_name; ?> (<?php echo $account->acc_username ?>)
						<input type="hidden" class="form-control" name="acc_username" />
					</div>
					<div class="form-group">
						<label for="acc_username" class="control-label">New Password</label>
						<?php echo $acc_password; ?>
						<input type="hidden" name="acc_password" value="<?php echo $acc_password; ?>" />
					</div>
					<div class="form-group">
						<span class="label label-warning">Please copy the password above.</span>
					</div>
					<div class="form-group">
					</div>
				</div>
				<div class="box-footer">
					<div class="pull-right">
						<button type="submit" name="form_submit" value='submit' class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
					</div>
					<a class="btn btn-default" href="<?php echo back_href(); ?>"><i class="fa fa-times"></i> Cancel</a>
				</div>
			</div>
		</form>
	</div>
</div>

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Time_logs extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		
		$this->access_control->logged_in();
		$this->access_control->validate();

		$this->load->model('time_log_model');
		$this->load->model("account_model");


		$this->load->helper('nav');
	}

	public function index()
	{
		$this->template->title('Time Logs');

		if($this->input->post('form_mode'))
		{
			$form_mode = $this->input->post('form_mode');

			if($form_mode == 'delete')
			{
				$tml_ids = $this->input->post('tml_ids');
				if($tml_ids !== false)
				{
					foreach($tml_ids as $tml_id)
					{
						$time_log = $this->time_log_model->get_one($tml_id);
						if($time_log !== false)
						{
							$this->time_log_model->delete($tml_id);
						}
					}
					$this->template->notification('Selected time logs were deleted.', 'success');
					redirect('admin/time_logs');
				}
			}
		}

		$page = array();
		$params = $this->input->get();
		$get = "";

		if($this->input->get('search'))
		{
			$get = parse_str($_SERVER['QUERY_STRING'], $_GET);
		}

		$page["acc_ids"] = $this->account_model->get_all();

		$page['time_logs'] = $this->time_log_model->pagination("admin/time_logs/index/__PAGE__/$get", 'search', $this->input->get('search'), $params);
		$page['time_logs_count'] = $this->time_log_model->pagination->total_rows();
		$page['time_logs_pagination'] = $this->time_log_model->pagination_links();
		$this->template->content('time_logs-index', $page);
		$this->template->content('time_logs-menu', null, null, 'page-nav');
		$this->template->show();
	}

	public function create()
	{
		$this->template->title('Create Time Log');

		// Use the set_rules from the Form_validation class for form validation.
		// Already combined with jQuery. No extra coding required for JS validation.
		// We get both JS and PHP validation which makes it both secure and user friendly.
		// NOTE: Set the rules before you check if $_POST is set so that the jQuery validation will work.
		$this->form_validation->set_rules("acc_id", "Account", "trim|required|integer|max_length[10]|min_length[1]");
		$this->form_validation->set_rules("tml_in", "In", "trim|datetime");
		$this->form_validation->set_rules("tml_out", "Out", "trim|datetime");
		$this->form_validation->set_rules("tml_set_time_in", "Set Time In", "trim|required|datetime");
		$this->form_validation->set_rules("tml_set_time_out", "Set Time Out", "trim|required|datetime");
		$this->form_validation->set_rules("tml_created", "Created", "trim|required|is_natural");
		$this->form_validation->set_rules("tml_deleted", "Deleted", "trim|is_natural");

		if($this->input->post('form_submit'))
		{
			$time_log = $this->extract->post();

			// Call run method from Form_validation to check
			if($this->form_validation->run() !== false)
			{
				$fields = $this->form_validation->get_fields();
				$result = $this->time_log_model->create($time_log, $fields);
				$id = $result['result']['insert_id'];

				// Set a notification using notification method from Template.
				// It is okay to redirect after and the notification will be displayed on the redirect page.
				$this->template->notification('New time log created.', 'success');
				redirect("admin/time_logs/edit/$id");
			}
			else
			{
				// To display validation errors caught by the Form_validation, you should have the code below.
				$this->template->notification(validation_errors(), 'danger');
			}

			$this->template->autofill($time_log);
		}

		$page = array();

		$page["acc_ids"] = $this->account_model->get_all();

		$this->template->content('time_logs-menu', null, null, 'page-nav');
		$this->template->content('time_logs-create', $page);
		$this->template->show();
	}

	public function edit($tml_id)
	{
		$this->template->title('Edit Time Log');

		$this->form_validation->set_rules("acc_id", "Account", "trim|required|integer|max_length[10]|min_length[1]");
		$this->form_validation->set_rules("tml_in", "In", "trim|datetime");
		$this->form_validation->set_rules("tml_out", "Out", "trim|datetime");
		$this->form_validation->set_rules("tml_set_time_in", "Set Time In", "trim|required|datetime");
		$this->form_validation->set_rules("tml_set_time_out", "Set Time Out", "trim|required|datetime");
		$this->form_validation->set_rules("tml_created", "Created", "trim|required|is_natural");
		$this->form_validation->set_rules("tml_deleted", "Deleted", "trim|is_natural");

		if($this->input->post('form_submit'))
		{
			$time_log = $this->extract->post();
			if($this->form_validation->run() !== false)
			{
				$time_log['tml_id'] = $tml_id;
				$fields = $this->form_validation->get_fields();

				$rows_affected = $this->time_log_model->update($time_log, $fields);

				$this->template->notification('Time log updated.', 'success');
				redirect("admin/time_logs/edit/$tml_id");
			}
			else
			{
				$this->template->notification(validation_errors());
			}
			$this->template->autofill($time_log);
		}

		$page = array();
		$page['time_log'] = $this->time_log_model->get_one($tml_id);

		if($page['time_log'] === false)
		{
			$this->template->notification('Time log was not found.', 'danger');
			redirect('admin/time_logs');
		}

		$page["acc_ids"] = $this->account_model->get_all();
		$this->template->content('time_logs-menu', null, null, 'page-nav');
		$this->template->content('time_logs-edit', $page);
		$this->template->show();
	}

	public function view($tml_id)
	{
		$this->template->title('View Time Log');

		$page = array();
		$page['time_log'] = $this->time_log_model->get_one($tml_id);

		if($page['time_log'] === false)
		{
			$this->template->notification('Time log was not found.', 'danger');
			redirect('admin/time_logs');
		}
		
		$page["acc_ids"] = $this->account_model->get_all();

		$this->template->content('time_logs-menu', null, null, 'page-nav');
		$this->template->content('time_logs-view', $page);
		$this->template->show();
	}


}

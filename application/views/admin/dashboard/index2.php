<!-- Add Site Modules here... -->
<div class="page-header">
  	<h1 class="text-center">
  		<img src="<?php echo res_url('admin/images/site-logo.png') ?>" alt="Think Sumo Creative Media, Inc.">
  	</h1>
</div>
<div class="row">
	<div class="col-xs-12 text-center">
		<a class="btn btn-app" href="<?php echo admin_url('banners') ?>">
			<span class="badge bg-light-blue"><?php echo $banner_count ?></span>
			<i class="fa fa-picture-o"></i> Banners
		</a>
		
		<a class="btn btn-app" href="<?php echo admin_url('articles') ?>">
			<span class="badge bg-light-blue"><?php echo $article_count ?></span>
			<i class="fa fa-file"></i> Articles
		</a>
		
		<a class="btn btn-app" href="<?php echo admin_url('pages') ?>">
			<span class="badge bg-light-blue"><?php echo $page_count ?></span>
			<i class="fa fa-book"></i> Pages
		</a>

		<a class="btn btn-app" href="<?php echo admin_url('users') ?>">
			<span class="badge bg-light-blue"><?php echo $user_count ?></span>
			<i class="fa fa-user"></i> Users
		</a>
	</div>
</div>
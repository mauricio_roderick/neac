<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Example
 *
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array.
 *
 * @package		CodeIgniter
 * @subpackage	Rest Server
 * @category	Controller
 * @author		Phil Sturgeon
 * @link		http://philsturgeon.co.uk/code/
*/

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/libraries/REST_Controller.php';

class Account_api extends REST_Controller
{

	public function __construct() 
	{
		parent::__construct();
		$this->load->model('account_model');
	}
	
	function login_get()
    {
		
		$username = $this->get('acc_username');
		$password = $this->get('acc_password');
		
        if(!$username || !$password)
        {
        	$this->response(array('error' => 'Username or password is required'), 200);
        }

		$user = $this->account_model->authenticate($username, $password);
		
        if($user)
        {
            $this->response($user, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(array('error' => 'Username or password is incorrect'), 200);
        }
    }

    //sample user update post
    function updateAccount_post()
    {
		$account = $this->extract->post();

		$this->form_validation->set_rules('acc_first_name', 'First Name', 'trim|required|max_length[256]');
		$this->form_validation->set_rules('acc_last_name', 'Last Name', 'trim|required|max_length[256]');

		if($this->form_validation->run() !== false)
		{
			$rows_affected = $this->account_model->update($account, $this->form_validation->get_fields());
			if($rows_affected > 0) {
				 $this->response($rows_affected, 200);
			}
			else
			{
				$this->response(array('error' => 'Failed to update entry. <br/> ID is invalid or no data was changed..'), 200);
			}
		}
		else
		{
			$this->response(array('error' => validation_errors()), 200);
		}
    }	
}
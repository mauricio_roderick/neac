<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once 'third_party/facebook-php-sdk-master/facebook.php';

class MYTHOS_Facebook extends Facebook {

  private $facebook;

  //initialize the Facebook object
  public function __construct() 
  {
    $this->CI =& get_instance();

    $params = $this->CI->config->item('facebook', 'mythos');
    $app_id = $params['app_id'];
    $app_secret = $params['app_secret'];  
    $this->facebook = new Facebook(array(
      'appId'  => $app_id,
      'secret' => $app_secret
    ));
  }

  //get the facebook object
  public function get_fb(){
    return $this->facebook;
  }

  //get the active FB User
  public function get_user(){
    return $this->facebook->getUser();
  }

  //manually sets the access token
  public function set_access_token($token){
    return $this->facebook->setAccessToken($token);
  }

  //publish a photo in the facebook
  //requires publish permissions
  public function publish_photo($fb_id, $file_path, $caption) {
    $path = "$fb_id/photos";
    $method = "POST";
    $params = array('message' => $caption);
    $params['image'] = "@".$file_path;
    $this->facebook->setFileUploadSupport(TRUE);
    $ret = $this->facebook->api($path, $method, $params);

    $photo_id = $ret['id'];
    $token = $this->facebook->getAccessToken();
    $path = "https://graph.facebook.com/$photo_id?access_token=$token";
    $photo = file_get_contents($path);

    return json_decode($photo);
  }

  //gets the video thumbnail from the video id
  public function get_video_thumbnail($video_id){
    $fql2 = "SELECT vid, owner, title, description, thumbnail_link,
    embed_html, updated_time, created_time FROM video
    WHERE vid=" . $video_id;

        $param  =   array(
            'method'    => 'fql.query',
            'query'     => $fql2,
            'callback'  => ''
        );
        $fqlResult2   =   $this->facebook->api($param);     
        if (!isset($fqlResult2[0])) {
          return "";
        }   
        return $fqlResult2[0]['thumbnail_link'];
  }

  
  public function is_liked(){
    $signed_request = $this->facebook->getSignedRequest();
    $like_status = false;

    if(!empty($signed_request['page'])){
      $like_status = $signed_request["page"]["liked"];
      $this->CI->session->set_userdata('liked', 1);
    } else if($this->CI->session->userdata('liked')){
      $like_status = true;
    }
    return $like_status;
  }

}
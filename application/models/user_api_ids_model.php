<?php
// Extend Base_model instead of CI_model
class User_api_ids_model extends Base_model
{
	public function __construct()
	{
		// List all fields of the table.
		// Primary key must be auto-increment and must be listed here first.
		$fields = array('uai_id', 'uai_username', 'usr_id', 'uai_type', 'uai_api_id', 'uai_api_token', 'uai_api_secret', 'uai_image');
		// Call the parent constructor with the table name and fields as parameters.
		parent::__construct('user_api_ids', $fields);
	}

	// Inherits the create, update, delete, get_one, and get_all methods of base_model.
	public function get_one($id)
	{				
		$this->db->join('user', "user.usr_id = {$this->table}.usr_id");
		return parent::get_one($id);
	}

	public function get_all($params = array())
	{				
		$this->db->join('user', "user.usr_id = {$this->table}.usr_id");
		return parent::get_all($params);
	}

	public function get_one_based_on_id_type($uai_type, $id)
	{				
		$this->db->where('uai_type', $uai_type);
		$this->db->where('uai_api_id', $id);
		$this->db->join('user', "user.usr_id = {$this->table}.usr_id");
		$query = $this->db->get($this->table); 
		
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
		else
		{
			return false;
		}
	}

		// Inherits the create, update, delete, get_one, and get_all methods of base_model.
	public function get_one_based_on_secret($uai_type, $secret)
	{				
		$this->db->where('uai_type', $uai_type);
		$this->db->where('uai_api_secret', $secret);
		$this->db->join('user', "user.usr_id = {$this->table}.usr_id");
		$query = $this->db->get($this->table); 
		
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
		else
		{
			return false;
		}
	}

	public function get_one_based_on_usr_id_secret($usr_id, $secret)
	{				
		$this->db->where('usr_id', $usr_id);
		$this->db->where('uai_api_secret', $secret);
		$query = $this->db->get($this->table); 
		
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
		else
		{
			return false;
		}
	}

	public function get_one_based_on_uai_username($username) 
	{
		$this->db->where('uai_username', $username);
		$query = $this->db->get($this->table);
		if($query->num_rows() > 0) {
			return $query->row();
		}
		else {
			return false;
		}
	}
}
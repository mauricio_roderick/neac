<div class="row">
	<div class="col-md-12">
		<form class="box box-primary">
			<div class="box-header with-border">
				<!-- Check all button -->
					<div class="btn btn-default btn-sm checkbox-toggle select-all" data-fields="acc_ids"><i class="fa fa-square-o"></i></div>
					<div class="btn-group">
						<button class="btn btn-default btn-sm action-delete" name="form_mode" value="delete"> <i class="fa fa-trash-o"></i> Delete All</button>
					</div><!-- /.btn-group --> 
					<div class="pull-right">
						<?php echo $accounts_pagination; ?>
					</div><!-- /.pull-right -->
			</div>
			<?php if ($accounts->num_rows()): ?>
			<div class="box-body table-responsive no-padding">
					
					<table class="table table-striped table-hover">
						<thead>
							<tr>
								<th></th>
								<th>Username</th>
								<th>Account Type</th>
								<th>Status</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
						<?php
						foreach($accounts->result() as $account)
						{
						?>
							<tr>
								<td style="width:50px" class="center">
									<input type="checkbox" name="acc_ids[]" value="<?php echo $account->acc_id; ?>" />
								</td>
								<td>
									<a href="<?php echo admin_url('accounts/view/'.$account->acc_id) ?>">
										<?php echo $account->acc_first_name; ?>
										<?php echo $account->acc_last_name; ?>
										<br>
										<small><?php echo $account->acc_username; ?></small>
									</a>
								</td>
								<td>
									<?php echo ucfirst($account->acc_type) ?>
								</td>
								<td>
									<?php echo ucfirst($account->acc_status) ?>
								</td>
								<td>
									<a href="<?php echo admin_url('accounts/view/'. $account->acc_id) ?>" class="btn btn-xs btn-primary">View</a>
									<!-- <a href="<?php echo site_url('accounts/edit') ?>" class="btn btn-xs btn-primary">Edit</a> -->
								</td>
							</tr>
						<?php
						}
						?>
						</tbody>
					</table>
			</div>
			<?php else: ?>
			<div class="box-body">
				<p class="text-center" style="padding: 10px;">
					No accounts found.
				</p>
			</div>
			<?php endif; ?>
		</form>
	</div>
</div>
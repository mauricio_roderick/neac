<form method="post">
	<div class="col-sm-3 col-sm-push-9 block">
		<div class="box">
			<div class="box-header">
				<h3 class="box-title">Menu</h3>
			</div>
			<div class="box-body">
				<?php echo create_admin_back_btn($this->router->fetch_class(), 'Cancel'); ?>
			</div>
		</div>
	</div>
	<div class="col-sm-9 col-sm-pull-3">
		<div class="box">
			<div class="box-body">
				<div class="form-group">
					<label for="usr_username" class="control-label">Username</label>
					<input class="form-control" type="text" name="usr_username" size="30" maxlength="30" value="" />
				</div>
				<div class="form-group">
					<label for="usr_password" class="control-label">Password</label>
					<input class="form-control" type="text" name="usr_password" size="32" maxlength="32" value="" />
				</div>
				<div class="form-group">
					<label for="usr_lname" class="control-label">Lname</label>
					<input class="form-control" type="text" name="usr_lname" size="50" maxlength="50" value="" />
				</div>
				<div class="form-group">
					<label for="usr_fname" class="control-label">Fname</label>
					<input class="form-control" type="text" name="usr_fname" size="80" maxlength="100" value="" />
				</div>
				<div class="form-group">
					<label for="usr_birthdate" class="control-label">Birthdate</label>
					<input class="form-control" type="text" name="usr_birthdate" class="datetime" value="" />
				</div>
				<div class="form-group">
					<label for="usr_gender" class="control-label">Gender</label>
					<select class="form-control" name="usr_gender">
						<option value="male">male</option>
						<option value="female">female</option>
					</select>
				</div>
				<div class="form-group">
					<label for="usr_email" class="control-label">Email</label>
					<input class="form-control" type="text" name="usr_email" size="80" maxlength="100" value="" />
				</div>
				<div class="form-group">
					<label for="usr_image" class="control-label">Image</label>
					<textarea class="form-control" name="usr_image" rows="5" cols="80"></textarea></div>
				</div>
				<div class="form-group">
					<label for="usr_company" class="control-label">Company</label>
					<input class="form-control" type="text" name="usr_company" size="80" maxlength="100" value="" />
				</div>
				<div class="form-group">
					<label for="usr_website" class="control-label">Website</label>
					<input class="form-control" type="text" name="usr_website" size="80" maxlength="100" value="" />
				</div>
				<div class="form-group">
					<label for="usr_occupation" class="control-label">Occupation</label>
					<input class="form-control" type="text" name="usr_occupation" size="80" maxlength="100" value="" />
				</div>
				<div class="form-group">
					<label for="usr_mobile" class="control-label">Mobile</label>
					<input class="form-control" type="text" name="usr_mobile" size="50" maxlength="50" value="" />
				</div>
				<div class="form-group">
					<label for="usr_landline" class="control-label">Landline</label>
					<input class="form-control" type="text" name="usr_landline" size="50" maxlength="50" value="" />
				</div>
				<div class="form-group">
					<label for="usr_address1" class="control-label">Address1</label>
					<input class="form-control" type="text" name="usr_address1" size="80" maxlength="150" value="" />
				</div>
				<div class="form-group">
					<label for="usr_address2" class="control-label">Address2</label>
					<input class="form-control" type="text" name="usr_address2" size="80" maxlength="150" value="" />
				</div>
				<div class="form-group">
					<label for="usr_city" class="control-label">City</label>
					<input class="form-control" type="text" name="usr_city" size="80" maxlength="100" value="" />
				</div>
				<div class="form-group">
					<label for="usr_country" class="control-label">Country</label>
					<input class="form-control" type="text" name="usr_country" size="50" maxlength="50" value="" />
				</div>
				<div class="form-group">
					<label for="usr_zip" class="control-label">Zip</label>
					<input class="form-control" type="text" name="usr_zip" size="10" maxlength="10" value="" />
				</div>
				<div class="form-group">
					<label for="usr_failed_login" class="control-label">Failed Login</label>
					<input class="form-control" type="text" name="usr_failed_login" size="3" maxlength="3" value="" />
				</div>
				<div class="form-group">
					<label for="usr_last_login" class="control-label">Last Login</label>
					<input class="form-control" type="text" name="usr_last_login" class="datetime" value="" />
				</div>
				<div class="form-group">
					<label for="usr_status" class="control-label">Status</label>
					<select class="form-control" name="usr_status">
						<option value="active">active</option>
						<option value="locked">locked</option>
						<option value="deleted">deleted</option>
					</select>
				</div>
				<div class="form-group">
					<label for="usr_date_created" class="control-label">Date Created</label>
					<input class="form-control" type="text" name="usr_date_created" class="datetime" value="" />
				</div>
				<div class="form-group">
					<label for="usr_verification" class="control-label">Verification</label>
					<input class="form-control" type="text" name="usr_verification" size="32" maxlength="32" value="" />
				</div>
				<div class="form-group">
					<input type="submit" name="submit" value="Submit" class="btn btn-primary" />
				</div>
			</div>
		</div>
	</div>
</form>
<script type="text/javascript">
$(function() {		
	$('form').floodling('usr_username', "<?php echo addslashes($user->usr_username); ?>");		
	$('form').floodling('usr_password', "<?php echo addslashes($user->usr_password); ?>");		
	$('form').floodling('usr_lname', "<?php echo addslashes($user->usr_lname); ?>");		
	$('form').floodling('usr_fname', "<?php echo addslashes($user->usr_fname); ?>");		
	$('form').floodling('usr_birthdate', "<?php echo addslashes($user->usr_birthdate); ?>");		
	$('form').floodling('usr_gender', "<?php echo addslashes($user->usr_gender); ?>");		
	$('form').floodling('usr_email', "<?php echo addslashes($user->usr_email); ?>");		
	$('form').floodling('usr_image', "<?php echo addslashes($user->usr_image); ?>");		
	$('form').floodling('usr_company', "<?php echo addslashes($user->usr_company); ?>");		
	$('form').floodling('usr_website', "<?php echo addslashes($user->usr_website); ?>");		
	$('form').floodling('usr_occupation', "<?php echo addslashes($user->usr_occupation); ?>");		
	$('form').floodling('usr_mobile', "<?php echo addslashes($user->usr_mobile); ?>");		
	$('form').floodling('usr_landline', "<?php echo addslashes($user->usr_landline); ?>");		
	$('form').floodling('usr_address1', "<?php echo addslashes($user->usr_address1); ?>");		
	$('form').floodling('usr_address2', "<?php echo addslashes($user->usr_address2); ?>");		
	$('form').floodling('usr_city', "<?php echo addslashes($user->usr_city); ?>");		
	$('form').floodling('usr_country', "<?php echo addslashes($user->usr_country); ?>");		
	$('form').floodling('usr_zip', "<?php echo addslashes($user->usr_zip); ?>");		
	$('form').floodling('usr_failed_login', "<?php echo addslashes($user->usr_failed_login); ?>");		
	$('form').floodling('usr_last_login', "<?php echo addslashes($user->usr_last_login); ?>");		
	$('form').floodling('usr_status', "<?php echo addslashes($user->usr_status); ?>");		
	$('form').floodling('usr_date_created', "<?php echo addslashes($user->usr_date_created); ?>");		
	$('form').floodling('usr_verification', "<?php echo addslashes($user->usr_verification); ?>");
});
</script>

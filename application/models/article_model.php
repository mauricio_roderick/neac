<?php
// Extend Base_model instead of CI_model
class Article_model extends Base_model
{
	private $img_width = 100;
	private $img_height = 100;
	private $upload_path = 'uploads/articles/';

	public function __construct()
	{
		// List all fields of the table.
		// Primary key must be auto-increment and must be listed here first.
		$fields = array('art_id', 'art_title', 'art_blurb', 'art_slug', 'art_content', 'art_thumb', 'art_image', 'art_published', 'art_featured', 'art_date', 'art_author', 'art_date_created', 'art_created_by', 'art_date_modified', 'art_modified_by');
		// Call the parent constructor with the table name and fields as parameters.
		$this->mythos->library('upload');
		$this->mythos->library('image');
		parent::__construct('article', $fields);
	}

	// Inherits the create, update, delete, get_one, and get_all methods of base_model.
	public function create($params = array())
	{
		if(!empty($_FILES['art_image']["tmp_name"]))
		{
			$upload_data = $this->image->upload("art_image", $this->upload_path);

			if(!isset($upload_data['error']))
			{
				$params['art_image'] = $this->upload_path.$upload_data['file_name'];

				if($this->img_height && $this->img_width)
				{

					$params['art_thumb'] = $this->upload_path.$upload_data['raw_name'] . "_thumb" . $upload_data['file_ext'];

					$thumbs = array(
						array('width'=>$this->img_width, 'height'=>$this->img_height, 'filename'=>$upload_data['raw_name'] . "_thumb")	
					);

					$this->image->create_thumbs($params['art_image'], $thumbs, false);
				}	
			}
			else
			{
				return array('error'=>$upload_data['error']);
			}
		} 	
	
		$params['art_date_created'] = format_mysql_datetime();
		$params['art_created_by'] = $this->session->userdata('acc_id');
		$params['art_date_modified'] = format_mysql_datetime();
		$params['art_modified_by'] = $this->session->userdata('acc_id');

		return parent::create($params);
	}

	public function update($params = array())
	{		
		// Upload the image
		if(!empty($_FILES['art_image']["tmp_name"]))
		{
			$upload_data = $this->image->upload("art_image", $this->upload_path);

			if(!isset($upload_data['error']))
			{
				$params['art_image'] = $this->upload_path.$upload_data['file_name'];	

				if($this->img_height && $this->img_width)
				{

					$params['art_thumb'] = $this->upload_path.$upload_data['raw_name'] . "_thumb" . $upload_data['file_ext'];

					$thumbs = array(
						array('width'=>$this->img_width, 'height'=>$this->img_height, 'filename'=>$upload_data['raw_name'] . "_thumb")	
					);

					$this->image->create_thumbs($params['art_image'], $thumbs, false);
				}	
			}
			else
			{
				return array('error'=>$upload_data['error']);
			}
		} 	
	
		$params['art_date_modified'] = format_mysql_datetime();
		$params['art_modified_by'] = $this->session->userdata('acc_id');

		return parent::update($params);
	}

	public function delete($art_id)
	{
		$params = array();

		$params['art_id'] = $art_id;
		$params['art_published'] = 'archived';
		return parent::update($params);
	}

	public function get_paginated_article($page_no, $page_size)
	{
		$start = ($page_no * $page_size) - $page_size;
		$query = $this->db->get($this->table, $page_size, $start);
		return $query;
	}

}
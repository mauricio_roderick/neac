<?php
ob_start();
phpinfo();
$phpinfo = ob_get_contents();
ob_end_clean();

// Remove phpinfo's CSS
$phpinfo = str_replace('<style', '<!-- <style', $phpinfo);
$phpinfo = str_replace('</style>', '</style> -->', $phpinfo);
?>
<style>
.phpinfo table {
	width: 100%;
	table-layout: fixed;
}
.phpinfo table td {
	word-wrap: break-word;
}

.phpinfo table tr.h td {
	text-align: center;
}

</style>
<div class="col-sm-3 col-sm-push-9 block">
	<div class="box">
		<div class="box-header">
			<h3 class="box-title">Menu</h3>
		</div>
		<div class="box-body">
			<ul class="nav nav-pills nav-stacked">
				<li><a href="<?php echo site_url('admin/settings'); ?>">System Settings</a></li>
				<li class="active"><a href="<?php echo site_url('admin/settings/phpinfo'); ?>">PHP System Information</a></li>
			</ul>
		</div>
	</div>
</div>
<div class="col-sm-9 col-sm-pull-3">
	<div class="phpinfo box">
		<div class='box-body'>
			<?php echo $phpinfo; ?>
		</div>
	</div>
</div>



<div class="box">
	<?php if ($site_options->num_rows()): ?>
		<div class="box-body no-padding table-responsive">
			<table class="table table-hover">
				<thead>
					<tr>
						<th>Name</th>
						<th>Value</th>
						<th style="width: 60px;"></th>
					</tr>
				</thead>
				<tbody>
				<?php
				foreach($site_options->result() as $site_options)
				{
					?>
					<tr>
						<td><a href="<?php echo site_url('admin/site_options/view/' . $site_options->opt_id); ?>"><?php echo $site_options->opt_name; ?></a></td>
						<td>
							<?php if($site_options->opt_type == "image" ): ?>
							<img src="<?php echo base_url($site_options->opt_value); ?>" alt="" style="height:50px" class="lightbox"/>
							<?php else: ?>
						<?php echo $site_options->opt_value; ?>
						<?php endif; ?>
						</td>
						<td class="center"><a href="<?php echo site_url('admin/site_options/edit/' . $site_options->opt_id); ?>" class="btn btn-primary btn-flat btn-sm">Edit</a></td>
					</tr>
					<?php
				}
				?>
				</tbody>
			</table>
		</div>
		<div class="box-footer clearfix">
			<p>
				Out of site links must contain http://
			</p>
		</div>
	<?php else: ?>
		<div class="box-body no-padding">
			<p class="text-center" style="padding: 10px;">
				No site options found.
			</p>
		</div>
	<?php endif; ?>
</div>

<?php

function create_admin_back_btn($controller, $word = false)
{
  $CI =& get_instance();
  $button = '<a href="__URL__" class="btn btn-default btn-flat btn-sm"><i class="glyphicon glyphicon-arrow-left"></i> __TEXT__</a>';
  if( $CI->agent->is_referral() ) {
    $url = $CI->agent->referrer();
  } else {
    $url = admin_url($controller);
  }

  if( $word ) {
    $button = str_replace('__TEXT__', $word, $button);
  } else {
    $button = str_replace('__TEXT__', 'Back', $button);
  }

  return str_replace('__URL__', $url, $button);
}

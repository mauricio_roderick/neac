﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en">
<head>
	<title> <?php echo template('title'); ?> | <?php echo template('site-name') ?></title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?php echo template('facebook_meta_tags'); ?>

	<?php echo template('bootstrap'); ?>
	<link rel="stylesheet" type="text/css" href="<?php echo res_url('site/css/styles.css'); ?>" />
	<link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url('favicon.ico') ?>">

	<?php echo template('mythos'); ?>
	<?php echo template('head'); ?>
</head>
<body class="<?php echo uri_css_class(); ?>">
	<header>
		<nav class="navbar navbar-default navbar-inverse" role="navigation">
		  <div class="container-fluid">
		    <!-- Brand and toggle get grouped for better mobile display -->
		    <div class="navbar-header">
		      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>
		      <a class="navbar-brand" href="#">Think Sumo</a>
		    </div>

		    <!-- Collect the nav links, forms, and other content for toggling -->
		    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		      <ul class="nav navbar-nav">
		        <li class="active"><a href="#">Link</a></li>
		        <li><a href="#">Link</a></li>
		        <li class="dropdown">
		          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown <span class="caret"></span></a>
		          <ul class="dropdown-menu" role="menu">
		            <li><a href="#">Action</a></li>
		            <li><a href="#">Another action</a></li>
		            <li><a href="#">Something else here</a></li>
		            <li class="divider"></li>
		            <li><a href="#">Separated link</a></li>
		            <li class="divider"></li>
		            <li><a href="#">One more separated link</a></li>
		          </ul>
		        </li>
		      </ul>
		      <form class="navbar-form navbar-left" role="search">
		        <div class="form-group">
		          <input type="text" class="form-control" placeholder="Search">
		        </div>
		        <button type="submit" class="btn btn-default">Submit</button>
		      </form>
		      <ul class="nav navbar-nav navbar-right">
		        <li><a href="#">Link</a></li>
		        <li class="dropdown">
		          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown <span class="caret"></span></a>
		          <ul class="dropdown-menu" role="menu">
		            <li><a href="#">Action</a></li>
		            <li><a href="#">Another action</a></li>
		            <li><a href="#">Something else here</a></li>
		            <li class="divider"></li>
		            <li><a href="#">Separated link</a></li>
		          </ul>
		        </li>
		      </ul>
		    </div><!-- /.navbar-collapse -->
		  </div><!-- /.container-fluid -->
		</nav>
	</header>
<div class="container">
	<div class="row">
		<div class="span12">
			<img src="http://placehold.it/250x80" />
		</div>
	</div>
	<div class="row">
		<div class="span12">
			<ul class="nav nav-pills">
				<li><a href="#">Menu 1</a></li>
				<li><a href="#">Menu 2</a></li>
				<li><a href="#">Menu 3</a></li>
				<li><a href="#">Menu 4</a></li>
				<li><a href="#">Menu 5</a></li>
			</ul>
		</div>
	</div>
	<div class="row">
		<div class="offset4 span4">
			<button class="fb-connect btn btn-large btn-block btn-primary" type="button">Connect to FB</button>
		</div>
	</div>
	<div class="row">
		<div class="span12">
			<?php echo template('notification'); ?>
			<h1><?php echo template('title'); ?></h1>
			<?php echo template('content'); ?>
		</div>
	</div>
	<div class="row">
		<div class="span12 center">&copy; <?php echo date('Y'); ?> - Website Name</div>
	</div>
</div>
	<?php echo template('mythos', 'utils'); ?>
	<?php //echo template('fb_login'); ?>
	<?php echo template('bootstrap', 'js'); ?>
	<?php echo template('sharebutton'); ?>
	<?php echo template('autofill'); ?>
</body>
<script>
	$(function(){
		$('.fb-connect').on('click', function(e){
			e.preventDefault();
			fbLogin(connected);
		});
	});
</script>
</html>

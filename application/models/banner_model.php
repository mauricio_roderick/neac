<?php
// Extend Base_model instead of CI_model
class Banner_model extends Base_model
{
	private $img_width = 0;
	private $img_height = 0;

	private $upload_path = 'uploads/banners/';

	public function __construct()
	{
		// List all fields of the table.
		// Primary key must be auto-increment and must be listed here first.
		$fields = array('ban_id', 'ban_title', 'ban_description', 'ban_image', 'ban_thumb', 'ban_order', 'ban_link', 'ban_published', 'ban_created_by', 'ban_date_created', 'ban_date_modified', 'ban_modified_by');
		$searchable_fields = array('ban_title', 'ban_description', 'ban_link');
		// Call the parent constructor with the table name and fields as parameters.
		$this->mythos->library('upload');
		$this->mythos->library('image');
		parent::__construct('banner', $fields, $searchable_fields, 'ban_order');
	}

	// Inherits the create, update, delete, get_one, and get_all methods of base_model.


	public function create($params = array())
	{
		
		$upload_data = $this->image->upload("ban_image", $this->upload_path);

		if(!isset($upload_data['error']))
		{
			$params['ban_image'] = $this->upload_path.$upload_data['file_name'];		
		}
		else
		{
		
		}
	
		$params['ban_order'] = parent::get_all()->num_rows();

		$params['ban_date_created'] = format_mysql_datetime();
		$params['ban_created_by'] = $this->session->userdata('acc_id');
		$params['ban_date_modified'] = format_mysql_datetime();
		$params['ban_modified_by'] = $this->session->userdata('acc_id');

		return parent::create($params);
	}

	public function update($params = array())
	{		
		// Upload the image
		
		$upload_data = $this->image->upload("ban_image", $this->upload_path);

		if(!isset($upload_data['error']))
		{
			$params['ban_image'] = $this->upload_path.$upload_data['file_name'];		
		}
		else
		{
		
		}
		
		if(is_array($params))
		{
			$params['ban_date_modified'] = format_mysql_datetime();
			$params['ban_modified_by'] = $this->session->userdata('acc_id');
		}

		return parent::update($params);
	}

	public function get_all( $params = array(), $order_by = array('ban_order'=>'asc') )
	{
		return parent::get_all($params, $order_by);
	}

	public function search($keyword = "", $params = array(), $order_by = array('ban_order'=>'asc'))
	{
		return parent::search($keyword, $params, $order_by);
	}

	public function delete($ban_id)
	{
		$params = array();

		$params['ban_id'] = $ban_id;
		$params['ban_published'] = 'archived';
		return parent::update($params);
	}

	
}
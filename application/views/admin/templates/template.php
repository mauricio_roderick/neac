<!DOCTYPE html>
<html lang="en">
<head>
	<title><?php echo template('title'); ?> | Administration Panel</title>
	<meta charset="utf-8">

	<!-- CSS -->
	<?php echo template('font_awesome'); ?>
	<?php echo template('jqueryui'); ?>
	<?php echo template('redactor'); ?>
	<?php echo template('admin_lte'); ?>
	<?php echo template('datepicker'); ?>
	<link rel="stylesheet" type="text/css" href="<?php echo res_url('admin/css/styles.css'); ?>" />

	<!-- Javascripts (Jquery) -->
	<?php echo template('mythos'); ?>
	<?php echo template('mythos_utils'); ?>
	<?php echo template('head'); ?>
</head>
<body class="<?php echo uri_css_class(); ?> skin-blue">
	<script type="text/javascript">
		function toggleMenuCookie()
		{
			if(sessionStorage.sidebarCollapse == "true")
			{
				sessionStorage.sidebarCollapse = 'false';
			}
			else
			{
				sessionStorage.sidebarCollapse = 'true';
			}
		}

		if(sessionStorage.sidebarCollapse == "true")
		{
			document.body.className = document.body.className + " sidebar-collapse";
		}
	</script>
	<div class="wrapper">
		<!-- Start Header -->
		<section id="page-notification">
			<?php echo template('notification'); ?>
		</section>
		<header class="header main-header">
			<a href="<?php echo site_url('admin'); ?>" class="logo">
				<img src="<?php echo res_url('admin/images/logo.png') ?>">
			</a>
			<!-- Header Navbar: style can be found in header.less -->
			<nav class="navbar navbar-static-top" role="navigation">
				<!-- Sidebar toggle button-->
				<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button" onclick="toggleMenuCookie()">
					<span class="sr-only">Toggle navigation</span>
				</a>
				<div class="navbar-custom-menu">
					<ul class="nav navbar-nav">
						<?php if ($this->access_control->check_logged_in()): ?>
						<li class="dropdown user-menu">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<i class="glyphicon glyphicon-user"></i>
								<span><?php echo $this->session->userdata('acc_name'); ?><i class="caret"></i></span>
							</a>
							<ul class="dropdown-menu">
	              			<!-- Menu Body -->
								<li class="user-header bg-light-blue">
									<?php
										$email = $this->session->userdata('acc_username');
										if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
											$email = "info@sumofy.me";
									  	}
										$size = 90;
										$grav_url = "http://www.gravatar.com/avatar/" . md5( strtolower( trim( $email ) ) ) . "?d=&s=" . $size;
									?>

									<?php if($this->session->userdata('acc_image')): ?>
									<img src="<?php echo base_url($this->session->userdata('acc_image')); ?>" class="img-circle" alt="User Image">
									<?php else: ?>
									<!-- <img src="<?php echo $grav_url; ?>" class="img-circle" alt="User Image"> -->
									<img src="<?php echo res_url('admin/images/default_avatar.jpg'); ?>" class="img-circle" alt="User Image">
									<?php endif; ?>
									<p>
										<small><?php echo $this->session->userdata('acc_name'); ?></small>
									</p>
								</li>
								<li class="user-footer">
									<div class="pull-left">
										<a href="<?php echo admin_url('profile'); ?>" class="btn btn-default btn-flat">Profile</a>
									</div>
									<div class="pull-right">
										<a href="<?php echo admin_url('index/logout'); ?>" class="btn btn-default btn-flat">Sign out</a>
									</div>
								</li>
							</ul>
						</li>
						<?php endif; ?>
					</ul>
				</div>
			</nav>
		</header> <!-- End Header -->

		
		<!-- Left side column. contains the logo and sidebar -->
		<aside class="main-sidebar">
		<!-- sidebar: style can be found in sidebar.less -->
			<section class="sidebar">
			<!-- Sidebar user panel -->
				<div class="user-panel">
					<div class="pull-left image">
						<?php if($this->session->userdata('acc_image')): ?>
						<img src="<?php echo base_url($this->session->userdata('acc_image')); ?>" class="img-circle" alt="User Image">
						<?php else: ?>
						<!-- <img src="<?php echo $grav_url; ?>" class="img-circle" alt="User Image"> -->
						<img src="<?php echo res_url('admin/images/default_avatar.jpg'); ?>" class="img-circle" alt="User Image">
						<?php endif; ?>
					</div>
					<div class="pull-left info" style="width:160px;">
						<p><?php echo $this->session->userdata('acc_name'); ?></p>
						<a href="#"><i class="fa fa-circle text-success"></i> Online</a>
					</div>
				</div>
			  	<ul class="sidebar-menu">
			  		<li class="header">SITE MODULES</li>
					<!-- Menu for BOTH admin and dev accounts -->
					<?php if ($this->access_control->check_account_type('admin', 'dev')): ?>
					<!--
					<li class="treeview">
						<a href="#">
							<i class="fa fa-book"></i>
							<span>Page Menu</span>
							<i class="fa fa-angle-left pull-right"></i>
							<ul class="treeview-menu">
								<li><a href="<?php echo admin_url('pages'); ?>"><i class="fa fa-angle-double-right"></i> Pages</a></li>
								<li><a href="<?php echo admin_url('page_categories'); ?>"><i class="fa fa-angle-double-right"></i> Categories</a></li>
							</ul>
						</a>
					</li>
					-->
					<!-- <li>
						<a href="<?php echo admin_url('pages'); ?>">
							<i class="fa fa-book"></i>
							<span>Pages</span>
						</a>
					</li>
					<li>
						<a href="<?php echo admin_url('articles'); ?>">
							<i class="fa fa-file"></i>
							<span>Articles</span>
						</a>
					</li>
					<li>
						<a href="<?php echo admin_url('banners'); ?>">
							<i class="fa fa-image"></i>
							<span>Banners</span>
						</a>
					</li>
					<li>
						<a href="<?php echo admin_url('site_options'); ?>">
							<i class="fa fa-gear"></i><span>Site Options</span>
						</a>
					</li> -->
					<!-- <li class="treeview">
						<a href="#">
							<i class="fa fa-user"></i>
							<span>Accounts</span>
							<i class="fa fa-angle-left pull-right"></i>
							<ul class="treeview-menu">
								<li><a href="<?php echo admin_url('accounts'); ?>"><i class="fa fa-angle-double-right"></i> Admin</a></li>
								<li><a href="<?php echo admin_url('employee'); ?>"><i class="fa fa-angle-double-right"></i> Employee</a></li>
							</ul>
						</a>
					</li> -->
					<li>
						<a href="<?php echo admin_url('accounts'); ?>">
							<i class="fa fa-user"></i><span>Accounts</span>
						</a>
					</li>
					<li>
						<a href="<?php echo admin_url('time_logs'); ?>">
							<i class="fa fa-clock-o"></i><span>Time Log</span>
						</a>
					</li>
					<?php endif; ?>
					<!-- Menu for DEV accounts -->
					<?php if ($this->access_control->check_account_type('dev')): ?>
			  		<li class="header">DEVELOPER TOOLS</li>			  		
					<li>
						<a href="<?php echo admin_url('emails'); ?>"><i class="fa fa-angle-double-right"></i> Emails</a>
					</li>	
					<li>
						<a href="<?php echo admin_url('settings'); ?>"><i class="fa fa-angle-double-right"></i> Site Settings</a>
					</li>
					<?php endif; ?>


					<!-- for drop down -->
					<!-- <li class="treeview">
			            <a href="#">
			                <i class="fa fa-bar-chart-o"></i>
			                <span>Charts</span>
			                <i class="fa fa-angle-left pull-right"></i>
			            </a>
			            <ul class="treeview-menu">
			                <li><a href="pages/charts/morris.html"><i class="fa fa-angle-double-right"></i> Morris</a></li>
			                <li><a href="pages/charts/flot.html"><i class="fa fa-angle-double-right"></i> Flot</a></li>
			                <li><a href="pages/charts/inline.html"><i class="fa fa-angle-double-right"></i> Inline charts</a></li>
			            </ul>
			          </li> -->
				</ul>
			</section>
			<!-- /.sidebar -->
		</aside>

			<!-- Main Section -->
		<div class="content-wrapper">
			<section class="content-header clearfix">
		   		<?php echo template('page-nav'); ?>
			</section>
			<section class="content">
				<?php echo template('content'); ?>
			</section>
		</div>
	</div>
	<!-- End of Main Body -->
	<div class="modal fade" id="confirm-modal">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body center">
					<a class="close" data-dismiss="modal">&times;</a>
					<h3>Confirm Action</h3>
					<p>Are you sure you want to continue?</p>
				</div>
				<div class="modal-footer center">
					<a href="#" class="btn btn-primary btn-large">Yes</a>
					<a href="#" class="btn btn-large" data-dismiss="modal">No</a>
				</div>
			</div>
		</div>
	</div>
	<?php echo template('redactor', 'js'); ?>
	<?php echo template('admin_lte', 'js'); ?>
	<?php echo template('fancybox', 'js'); ?>
	<?php echo template('datepicker', 'js'); ?>
	<?php echo template('jqueryui', 'js'); ?>
	<?php echo template('autofill'); ?>
	<script type="text/javascript" src="<?php echo res_url('admin/js/document.ready.js'); ?>"></script>
</body>
</html>

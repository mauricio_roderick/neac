<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once(SYSDIR . '/libraries/Email.php');

class MYTHOS_Email extends CI_Email 
{
	private $CI;
	private $config;
	
	public function __construct() 
	{
		parent::__construct();
		$this->CI =& get_instance();
		$this->CI->load->library('email');
		$this->CI->load->model('email_model');
		$config = $this->CI->config->item('email', 'mythos');
		$this->config = $config;	
	}

	/**
	 * Valid Email Params:
	 * 	mail_to (string - comma separated email ads if multiple),
	 * 	subject (string), 
	 * 	cc (string - comma separated email ads if multiple),
	 * 	bcc (string - comma separated email ads if multiple), 
	 * 	content (string), 
	 * 	attachment (file path)
	 * 
	 */
	public function send_mail($params = array(), $template_folder = 'email', $template_view = 'template')
	{
		$email = array();
		$return = array();
		$template_params = array();
		$template_params['content'] = $params['content'];
		$html_message = $this->CI->template->get_view($template_view, $template_params, $template_folder, true);
		$debug_mode = $this->config['debug'];
		
		if (!empty($params['debug']))
		{
				$debug_mode = $params['debug'];
		}

		//check for debug mode
		if ($debug_mode == 'preview'){
				echo $html_message;
				die();
		}

		if(!empty($params['from_email'])) {
			$from_email = $params['from_email'];
		}
		else {
			$from_email = $this->config['from_email'];
		}
		
		if(!empty($params['from_email_name'])) {
			$from_email_name = $params['from_email_name'];
		}
		else {
			$from_email_name = $this->config['from_email_name'];
		}

		if(!empty($params['store_email'])) {
			$store_email = $params['store_email'];
		}
		else {
			$store_email = $this->config['store_email'];
		}

	
		$this->CI->email->initialize($this->config);
		$this->CI->email->subject($params['subject']);
		$this->CI->email->message($html_message);
		$this->CI->email->to($params['mail_to']);

		if(!empty($params['cc'])){
			$this->CI->email->cc($params['cc']);
		}

		if(!empty($params['bcc'])){
			$this->CI->email->bcc($params['bcc']);
		}

		if(!empty($params['attachment'])){
			$this->CI->email->attach($params['attachment']);
		}

		$this->CI->email->from($from_email, $from_email_name);


		if ($debug_mode == 'simulation') 
		{
			$email['eml_status'] = 'sent';
			$email['eml_datetime'] = format_datetime();
			$return['success'] = true;
			$return['message'] = 'Successfully sent the email.';

		} else {
			if( $this->CI->email->send() )
			{
				$email['eml_status'] = 'sent';
				$email['eml_datetime'] = format_datetime();
				$return['success'] = true;
				$return['message'] = 'Successfully sent the email.';
			}
			else
			{
				$email['eml_status'] = 'failed';
				$return['success'] = false;
				$return['message'] = 'Email sending has failed.';
			}
		}
		// Create Email for logging purposes
		$email['eml_mail_to'] = $params['mail_to'];
		$email['eml_cc']      = $params['cc'];
		$email['eml_bcc']      = $params['bcc'];
		$email['eml_from']      = $from_email;
		$email['eml_from_name']      = $from_email_name;
		$email['eml_subject'] = $params['subject'];
		$email['eml_message'] = $html_message;
		$email['eml_debug'] = $this->CI->email->print_debugger();
		if ($store_email)
		{
			$this->CI->email_model->create($email);
		}

		$return['result'] = $email['eml_debug'];

		//returns (success, message,result)
		return $return;
			
	}

	public function resend_mail($email_id){

		if ($data = $this->CI->email_model->get_one($email_id))
		{			
			$email['eml_id'] = $email_id;
			$email['eml_mail_to'] = $data->eml_mail_to;
			$email['eml_cc']      = $data->eml_cc;
			$email['eml_bcc']     = $data->eml_bcc;
			$email['eml_from']      = $data->eml_from;
			$email['eml_from_name']  = $data->eml_from_name;
			$email['eml_subject'] = $data->eml_subject;
			$email['eml_message'] = $data->eml_message;
			

			$this->CI->email->initialize($this->config);
			$this->CI->email->subject($email['eml_subject']);
			$this->CI->email->message($email['eml_message']);
			$this->CI->email->to($email['eml_mail_to']);

			if(!empty($params['cc'])){
				$this->CI->email->cc($params['cc']);
			}

			if(!empty($params['bcc'])){
				$this->CI->email->bcc($params['bcc']);
			}

			if(!empty($params['attachment'])){
				$this->CI->email->attach($params['attachment']);
			}

			$this->CI->email->from($email['eml_from'], $email['eml_from_name']);

			if( $this->CI->email->send() )
			{
				$email['eml_status'] = 'resent';
				$email['eml_datetime'] = format_datetime();
				$return['success'] = true;
				$return['message'] = 'Successfully sent the email.';
			}
			else
			{
				$email['eml_status'] = 'failed';
				$return['success'] = false;
				$return['message'] = 'Email sending has failed.';
			}

			$email['eml_debug'] = $this->CI->email->print_debugger();
			$this->CI->email_model->update($email);


		} else {
			$return['success'] = false;
			$return['message'] = 'There is no existing email with that ID';
		}

		//returns (success, message,result)
		return $return;
		
	}
}
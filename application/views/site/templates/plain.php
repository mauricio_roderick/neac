<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en">
<head>
	<title> <?php echo template('title'); ?> | <?php echo template('site-name') ?></title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?php echo template('facebook_meta_tags'); ?>

	<?php echo template('bootstrap'); ?>
	<link rel="stylesheet" type="text/css" href="<?php echo res_url('site/css/styles.css'); ?>" />
	<link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url('favicon.ico') ?>">

	<?php echo template('mythos'); ?>
	<?php echo template('head'); ?>
</head>
<body class="<?php echo uri_css_class(); ?>">
	<div class="container" style="margin-top:50px">
		<div class="row">
			<div class="col-xs-12">
				<?php echo template('notification'); ?>
			</div>
		</div>
		<?php echo template('content'); ?>
	</div>
	<?php echo template('bootstrap', 'js'); ?>
</body>
</html>
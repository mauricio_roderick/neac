<form class="form-inline" role="form" method="post">
	<div class="box">
		<div class="box-header">
			<div class="box-tools clearfix">
				<div class="btn-group">
					<button class="btn btn-flat btn-default btn-sm" name="form_mode" value="delete">
						<i class="fa fa-trash-o"></i>
					</button>
				</div>
				<div class="pull-right">
					<div class="form-group">
						<label for="field" class="sr-only">Fields</label>
						<select name="field" id="" class="form-control input-sm">
							<option value="part_name"></option>
							<option value="part_name"></option>
						</select>
					</div>
					<div class="form-group input-group">
						<label for="keywords" class="sr-only">Keywords</label>
						<input type="text" class="form-control input-sm" name="keywords" placeholder="Filter" value="">
						<div class="input-group-btn">
							<button class="btn btn-flat btn-primary btn-sm" name="search" type="submit" value="search">
								<i class="fa fa-search"></i>
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php if ($user_api_ids->num_rows()): ?>
			<div class="box-body no-padding table-responsive">
				<table class="table-list table-striped table-bordered table-hover">
					<thead>
						<tr>
							<th class="center skip-sort"><input type="checkbox" class="select-all" value="uai_ids" /></th>
						<th>User</th>
						<th>Type</th>
						<th>Api Id</th>
						<th>Api Token</th>
							<th style="width: 60px;"></th>
						</tr>
					</thead>
					<tbody>
					<?php
					foreach($user_api_ids->result() as $user_api_ids)
					{
						?>
						<tr>
							<td class="center"><input type="checkbox" name="uai_ids[]" value="<?php echo $user_api_ids->uai_id; ?>" /></td>
						<td><a href="<?php echo site_url('admin/user_api_ids/view/' . $user_api_ids->uai_id); ?>"><?php echo $user_api_ids->usr_username; ?></a></td>
						<td><?php echo $user_api_ids->uai_type; ?></td>
						<td><?php echo nl2br($user_api_ids->uai_api_id); ?></td>
						<td><?php echo nl2br($user_api_ids->uai_api_token); ?></td>
							<td class="center"><a href="<?php echo site_url('admin/user_api_ids/edit/' . $user_api_ids->uai_id); ?>" class="btn btn-primary">Edit</a></td>
						</tr>
						<?php
					}
					?>
					</tbody>
				</table>
			</div>
			<div class="box-footer clearfix">
				<?php echo $user_api_ids_pagination; ?>
			</div>
		<?php else: ?>
			<div class="box-body no-padding">
				<p class="text-center" style="10px;">
					No user api ids found.
				</p>
			</div>
		<?php endif; ?>
	</div>
</form>

<script type="text/javascript">
$(function() {	$('form').floodling('field', "<?php echo $field; ?>");
	$('form').floodling('keywords', "<?php echo $keywords; ?>");
});
</script>


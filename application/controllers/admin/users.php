<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->access_control->logged_in();
		$this->access_control->validate();

		$this->load->model('user_model');
		$this->load->helper('nav');
	}

	public function index()
	{
		$this->template->title('Users');

		if($this->input->post('form_mode'))
		{
			$form_mode = $this->input->post('form_mode');

			if($form_mode == 'delete')
			{
				$usr_ids = $this->input->post('usr_ids');
				if($usr_ids !== false)
				{
					foreach($usr_ids as $usr_id)
					{
						$user = $this->user_model->get_one($usr_id);
						if($user !== false)
						{
							$this->user_model->delete($usr_id);
						}
					}
					$this->template->notification('Selected users were deleted.', 'success');
				}
			}
		} elseif ($this->input->post('search')) {
			$search = $this->extract->post('search');
			$search_params['field'] = $search['field'];
			$search_params['keywords'] = $search['keywords'];
		}

		$page = array();
		$page['keywords'] = @$search['keywords'];
		$page['field'] = @$search['field'];
		$page['users'] = $this->user_model->pagination("admin/users/index/__PAGE__", 'search_keyword_admin', @$search_params);
		$page['users_pagination'] = $this->user_model->pagination_links();
		$this->template->content('users-index', $page);
		$this->template->content('menu-users', null, 'admin', 'page-nav');
		$this->template->show();
	}

	public function create()
	{
		$this->template->title('Create User');


		// Use the set_rules from the Form_validation class for form validation.
		// Already combined with jQuery. No extra coding required for JS validation.
		// We get both JS and PHP validation which makes it both secure and user friendly.
		// NOTE: Set the rules before you check if $_POST is set so that the jQuery validation will work.
		$this->form_validation->set_rules('usr_username', 'Username', 'trim|required|max_length[30]');
		$this->form_validation->set_rules('usr_password', 'Password', 'trim|required|max_length[32]');
		$this->form_validation->set_rules('usr_lname', 'Lname', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('usr_fname', 'Fname', 'trim|required|max_length[100]');
		$this->form_validation->set_rules('usr_birthdate', 'Birthdate', 'trim|datetime');
		$this->form_validation->set_rules('usr_gender', 'Gender', 'trim');
		$this->form_validation->set_rules('usr_email', 'Email', 'trim|max_length[100]');
		$this->form_validation->set_rules('usr_image', 'Image', 'trim');
		$this->form_validation->set_rules('usr_company', 'Company', 'trim|max_length[100]');
		$this->form_validation->set_rules('usr_website', 'Website', 'trim|max_length[100]');
		$this->form_validation->set_rules('usr_occupation', 'Occupation', 'trim|max_length[100]');
		$this->form_validation->set_rules('usr_mobile', 'Mobile', 'trim|max_length[50]');
		$this->form_validation->set_rules('usr_landline', 'Landline', 'trim|max_length[50]');
		$this->form_validation->set_rules('usr_address1', 'Address1', 'trim|max_length[150]');
		$this->form_validation->set_rules('usr_address2', 'Address2', 'trim|max_length[150]');
		$this->form_validation->set_rules('usr_city', 'City', 'trim|max_length[100]');
		$this->form_validation->set_rules('usr_country', 'Country', 'trim|max_length[50]');
		$this->form_validation->set_rules('usr_zip', 'Zip', 'trim|max_length[10]');
		$this->form_validation->set_rules('usr_failed_login', 'Failed Login', 'trim|required|integer|max_length[3]|min_length[1]');
		$this->form_validation->set_rules('usr_last_login', 'Last Login', 'trim|datetime');
		$this->form_validation->set_rules('usr_status', 'Status', 'trim|required');
		$this->form_validation->set_rules('usr_date_created', 'Date Created', 'trim|required|datetime');
		$this->form_validation->set_rules('usr_verification', 'Verification', 'trim|max_length[32]');

		if($this->input->post('submit'))
		{
			$user = $this->extract->post();

			// Call run method from Form_validation to check
			if($this->form_validation->run() !== false)
			{
				$fields = $this->form_validation->get_fields();
				$this->user_model->create($user, $fields);
				// Set a notification using notification method from Template.
				// It is okay to redirect after and the notification will be displayed on the redirect page.
				$this->template->notification('New user created.', 'success');
				redirect('admin/users');
			}
			else
			{
				// To display validation errors caught by the Form_validation, you should have the code below.
				$this->template->notification(validation_errors(), 'danger');
			}

			$this->template->autofill($user);
		}

		$page = array();
		
		$this->template->content('users-create', $page);
		$this->template->show();
	}

	public function edit($usr_id)
	{
		$this->template->title('Edit User');


		$this->form_validation->set_rules('usr_username', 'Username', 'trim|required|max_length[30]');
		$this->form_validation->set_rules('usr_password', 'Password', 'trim|required|max_length[32]');
		$this->form_validation->set_rules('usr_lname', 'Lname', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('usr_fname', 'Fname', 'trim|required|max_length[100]');
		$this->form_validation->set_rules('usr_birthdate', 'Birthdate', 'trim|datetime');
		$this->form_validation->set_rules('usr_gender', 'Gender', 'trim');
		$this->form_validation->set_rules('usr_email', 'Email', 'trim|max_length[100]');
		$this->form_validation->set_rules('usr_image', 'Image', 'trim');
		$this->form_validation->set_rules('usr_company', 'Company', 'trim|max_length[100]');
		$this->form_validation->set_rules('usr_website', 'Website', 'trim|max_length[100]');
		$this->form_validation->set_rules('usr_occupation', 'Occupation', 'trim|max_length[100]');
		$this->form_validation->set_rules('usr_mobile', 'Mobile', 'trim|max_length[50]');
		$this->form_validation->set_rules('usr_landline', 'Landline', 'trim|max_length[50]');
		$this->form_validation->set_rules('usr_address1', 'Address1', 'trim|max_length[150]');
		$this->form_validation->set_rules('usr_address2', 'Address2', 'trim|max_length[150]');
		$this->form_validation->set_rules('usr_city', 'City', 'trim|max_length[100]');
		$this->form_validation->set_rules('usr_country', 'Country', 'trim|max_length[50]');
		$this->form_validation->set_rules('usr_zip', 'Zip', 'trim|max_length[10]');
		$this->form_validation->set_rules('usr_failed_login', 'Failed Login', 'trim|required|integer|max_length[3]|min_length[1]');
		$this->form_validation->set_rules('usr_last_login', 'Last Login', 'trim|datetime');
		$this->form_validation->set_rules('usr_status', 'Status', 'trim|required');
		$this->form_validation->set_rules('usr_date_created', 'Date Created', 'trim|required|datetime');
		$this->form_validation->set_rules('usr_verification', 'Verification', 'trim|max_length[32]');

		if($this->input->post('submit'))
		{
			$user = $this->extract->post();
			if($this->form_validation->run() !== false)
			{
				$user['usr_id'] = $usr_id;
				$fields = $this->form_validation->get_fields();

				$rows_affected = $this->user_model->update($user, $fields);

				$this->template->notification('User updated.', 'success');
				redirect('admin/users');
			}
			else
			{
				$this->template->notification(validation_errors());
			}
			$this->template->autofill($user);
		}

		$page = array();
		$page['user'] = $this->user_model->get_one($usr_id);

		if($page['user'] === false)
		{
			$this->template->notification('User was not found.', 'danger');
			redirect('admin/users');
		}

		$this->template->content('users-edit', $page);
		$this->template->show();
	}

	public function view($user_id)
	{
		$this->template->title('View User');
		
		$page = array();
		$page['user'] = $this->user_model->get_one($user_id);

		if($page['user'] === false)
		{
			$this->template->notification('User was not found.', 'danger');
			redirect('admin/users');
		}
		
		$this->template->content('users-view', $page);
		$this->template->show();
	}
}
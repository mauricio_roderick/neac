<form method="post">
	<div class="col-sm-3 col-sm-push-9 block">
		<div class="box">
			<div class="box-header">
				<h3 class="box-title">Menu</h3>
			</div>
			<div class="box-body">
				<?php echo create_admin_back_btn($this->router->fetch_class(), 'Cancel'); ?>
			</div>
		</div>
	</div>
	<div class="col-sm-9 col-sm-pull-3">
		<div class="box">
			<div class="box-body">
				<div class="form-group">
					<label for="eml_mail_to" class="control-label">Mail To</label>
					<input class="form-control" type="text" name="eml_mail_to" size="80" maxlength="150" value="" />
				</div>
				<div class="form-group">
					<label for="eml_cc" class="control-label">Cc</label>
					<input class="form-control" type="text" name="eml_cc" size="80" maxlength="150" value="" />
				</div>
				<div class="form-group">
					<label for="eml_bcc" class="control-label">Bcc</label>
					<input class="form-control" type="text" name="eml_bcc" size="80" maxlength="150" value="" />
				</div>
				<div class="form-group">
					<label for="eml_subject" class="control-label">Subject</label>
					<input class="form-control" type="text" name="eml_subject" size="80" maxlength="150" value="" />
				</div>
				<div class="form-group">
					<label for="eml_message" class="control-label">Message</label>
					<textarea class="form-control" name="eml_message" rows="5" cols="80"></textarea>
				</div>
				<div class="form-group">
					<label for="eml_date_sent" class="control-label">Date Sent</label>
					<input class="form-control" type="text" name="eml_date_sent" size="" maxlength="" value="" />
				</div>
				<div class="form-group">
					<button class="btn btn-primary btn-flat" type="submit" name="submit">
						<i class="fa fa-save"></i> Submit
					</button>
				</div>
			</div>
		</div>
	</div>
</form>
<script type="text/javascript">
$(function() {
	$('form').floodling('eml_mail_to', "<?php echo addslashes($email->eml_mail_to); ?>");
	$('form').floodling('eml_cc', "<?php echo addslashes($email->eml_cc); ?>");
	$('form').floodling('eml_bcc', "<?php echo addslashes($email->eml_bcc); ?>");
	$('form').floodling('eml_subject', "<?php echo addslashes($email->eml_subject); ?>");
	$('form').floodling('eml_date_sent', "<?php echo addslashes($email->eml_date_sent); ?>");
});
</script>

<div class="row">
	<div class="col-md-3">
		<a href="<?php echo admin_url('banners/create') ?>" class="btn btn-primary btn-block margin-bottom">
			<i class="fa fa-plus-square"></i> Create
		</a>
		<div class="box box-solid">
			<div class="box-header with-border">
				<h3 class="box-title">Menu</h3>
			</div>
			<div class="box-body no-padding">
				<ul class="nav nav-pills nav-stacked">
					<li>
						<a href="<?php echo admin_url('banners') ?>"><i class="fa fa-circle-o"></i> All</a>
					</li>
					<li>
						<a href="<?php echo admin_url('banners/index/published') ?>"><i class="fa fa-circle-o text-blue"></i> Published </a>
					</li>
					<li>
						<a href="<?php echo admin_url('banners/index/draft') ?>"><i class="fa fa-circle-o text-yellow"></i> Draft</a>
					</li>
					<li>
						<a href="<?php echo admin_url('banners/index/archived') ?>"><i class="fa fa-circle-o text-red"></i> Archived</a>
					</li>
				</ul>
			</div><!-- /.box-body -->
		</div><!-- /. box -->
	</div>

	<div class="col-md-9">
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">Manage Banners</h3>
				<div class="box-tools pull-right">
					<div class="has-feedback">
						<form role="form" method="get">
							<input type="text" class="form-control input-sm" name="search" placeholder="Search" value="<?php echo $this->input->get('search') ?>" />
							<span class="glyphicon glyphicon-search form-control-feedback"></span>
						</form>
					</div>
				</div><!-- /.box-tools -->
			</div>
			<?php if ($banners->num_rows()): ?>
			<div class="box-body table-responsive no-padding">
				<form role="form" method="post">
					<div class="mailbox-controls">
					<!-- Check all button -->
						<div class="btn btn-default btn-sm checkbox-toggle select-all" data-fields="ban_ids"><i class="fa fa-square-o"></i></div>
						<div class="btn-group">
							<button class="btn btn-default btn-sm action-delete" name="form_mode" value="delete"> <i class="fa fa-trash-o"></i> Delete All</button>
						</div><!-- /.btn-group --> 
						<div class="pull-right">
							<?php echo $banners_pagination; ?>
						</div><!-- /.pull-right -->
					</div>
					<table class="table table-striped table-hover">
						<tbody class="sortable" data-target="<?php echo admin_url('banners/reorder') ?>">
						<?php
						foreach($banners->result() as $banner)
						{
						?>
							<tr data-primkey="<?php echo $banner->ban_id; ?>">
								<td style="width:50px" class="center"><input type="checkbox" name="ban_ids[]" value="<?php echo $banner->ban_id; ?>" /></td>
								<td style="width:120px">
									<img src='<?php echo base_url($banner->ban_image); ?>' style="height:50px;" />
								</td>
								<td>
									<a href="<?php echo admin_url('banners/edit/'.$banner->ban_id) ?>"><?php echo $banner->ban_title; ?></a>
								</td>
								<td class='text-right'>
									<span class="label label-default"><?php echo $banner->ban_published; ?></span> 
								</td>
								<td class='text-right' style="width:60px">
									<div class="btn btn-default btn-flat btn-xs order-up">
                         				<i class="fa fa-caret-up"></i>
                      				</div>
									<div class="btn btn-default btn-flat btn-xs order-down">
                         				<i class="fa fa-caret-down"></i>
                      				</div>
								</td>
							</tr>
						<?php
						}
						?>
						</tbody>
					</table>
				</form>
			</div>
			<?php else: ?>
			<div class="box-body">
				<p class="text-center" style="padding: 10px;">
					No banners found.
				</p>
			</div>
			<?php endif; ?>
		</div>
	</div>
</div>

<script type="text/javascript">
$(function() {
	// $('.sortable').sortable({
	// 	update: function( event, ui ) {

	// 	}
	// });
});
</script>
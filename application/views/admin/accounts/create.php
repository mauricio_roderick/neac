<div class="row">
	<div class="col-md-12">
		<form method="post" enctype="multipart/form-data" data-submit="submitForm">
			<div class="box">
				<div class="box-body">
					<div class="form-group">
						<label for="acc_username" class="control-label">Email</label>
						<input type="text" name="acc_username" class="form-control" maxlength="150" />
					</div>
					<div class="form-group">
						<label for="acc_password" class="control-label">Password</label>
						<input type="password" name="acc_password" class="form-control" maxlength="150" />
					</div>
					<div class="form-group">
						<label for="retype_password" class="control-label">Retype Password</label>
						<input type="password" name="retype_password" class="form-control" maxlength="150" />
					</div>
					<div class="form-group">
						<label for="acc_first_name" class="control-label">First Name</label>
						<input type="text" name="acc_first_name" class="form-control" maxlength="150" />
					</div>
					<div class="form-group">
						<label for="acc_last_name" class="control-label">Last Name</label>
						<input type="text" name="acc_last_name" class="form-control" maxlength="150" />
					</div>
					<div class="form-group">
						<div class="btn btn-default btn-file">
	                    	<i class="fa fa-file-image-o"></i> Image
	                    	<input type="file" name="acc_image">
	                    </div>
					</div>
					<div class="form-group">
						<label for="acc_last_name" class="control-label">Account Type</label>
						<?php
							$options =array();
							$options['admin'] = "Admin";
							$options['employee'] = "Employee";
							echo form_dropdown('acc_type',$options, $this->input->post('acc_type'), 'class="form-control"');
						?>
					</div>
					<div class="form-group employee_field">
						<label for="acc_last_name" class="control-label">Required Time to Login</label>
						<?php
							echo form_dropdown('acc_time_in',$time_opts, '', 'class="form-control"');
						?>
					</div>
					<div class="form-group employee_field">
						<label for="acc_last_name" class="control-label">Number of Hours to Work</label>
						<input type="text" name="acc_hours_to_work" placeholder="0.00" class="form-control" />
					</div>
				</div>
				<div class="box-footer">
					<div class="pull-right">
						<button type="submit" name="form_submit" value='submit' class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
					</div>
					<a class="btn btn-default" href="<?php echo back_href(); ?>"><i class="fa fa-times"></i> Cancel</a>
				</div>
			</div>
		</form>
	</div>
</div>

<script type="text/javascript">
$(document).ready(function(){
	function hideShowEmployeeFields() {
		var selected_opt = $('[name="acc_type"]').children('option:selected').val();
		
		if(selected_opt == 'employee') {
			$('.employee_field').show();
		} else {
			$('.employee_field').hide();
		}
	}

	$('[name="acc_type"]').change(function(){
		hideShowEmployeeFields();
	})

	hideShowEmployeeFields();
})
</script>